/*
 * Copyright © 2017 Michael von Glasow.
 * 
 * This file is part of Qz.
 *
 * Qz is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Qz is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Qz.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.vonglasow.michael.qz.tuning;

/* NOTE: 
 * This is a portable class which should be kept clean of any dependencies on platform-specific
 * classes (JRE or Android).
 */

/**
 * @brief Describes a station network with the AFI bit set.
 * 
 * If a station network has the AFI (Alternate Frequency Indicator) bit set, all frequencies
 * with the same PI code carry the same TMC service.
 * 
 * Instances of this class are immutable.
 */
public class AfiNetwork {
	public final int pi;
	public final int ltn;
	public final int sid;

	public AfiNetwork(int pi, int ltn, int sid) {
		this.pi = pi;
		this.ltn = ltn;
		this.sid = sid;
	}

	@Override
	public boolean equals(Object obj) {
		return ((obj instanceof AfiNetwork)
				&& (this.pi == ((AfiNetwork) obj).pi)
				&& (this.ltn == ((AfiNetwork) obj).ltn)
				&& (this.sid == ((AfiNetwork) obj).sid));
	}

	@Override
	public int hashCode() {
		return pi * 961 + ltn * 31 + sid;
	}
}