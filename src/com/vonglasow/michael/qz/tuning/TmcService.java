/*
 * Copyright © 2017 Michael von Glasow.
 * 
 * This file is part of Qz.
 *
 * Qz is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Qz is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Qz.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.vonglasow.michael.qz.tuning;

import java.util.Comparator;

/* NOTE: 
 * This is a portable class which should be kept clean of any dependencies on platform-specific
 * classes (JRE or Android).
 */

/**
 * @brief Describes a TMC service. 
 *
 * Instances of this class are immutable.
 */
public class TmcService {
	/**
	 * The default comparator.
	 */
	public static TmcServiceComparator COMPARATOR = new TmcServiceComparator();

	public final int cc;
	public final int ltn;
	public final int sid;
	public final String name;

	/**
	 * @brief Instantiates a TMC service without a service name.
	 * 
	 * @param cc The country code (between 0x1 and 0xF).
	 * @param ltn The Location Table Number
	 * @param sid The Service Identifier
	 */
	public TmcService(int cc, int ltn, int sid) {
		this(cc, ltn, sid, null);
	}

	/**
	 * @brief Instantiates a TMC service using another TMC service and a new name.
	 * 
	 * @param service The other service whose CC, LTN and SID will be reused
	 * @param name The name of the service, can be null
	 */
	public TmcService(TmcService service, String name) {
		this(service.cc, service.ltn, service.sid, name);
	}

	/**
	 * @brief Instantiates a TMC service.
	 * 
	 * @param cc The country code (between 0x1 and 0xF).
	 * @param ltn The Location Table Number
	 * @param sid The Service Identifier
	 * @param name The name of the service, can be null
	 */
	public TmcService(int cc, int ltn, int sid, String name) {
		this.cc = cc;
		this.ltn = ltn;
		this.sid = sid;
		this.name = name;
	}

	/**
	 * @brief A comparator for {@link TmcService} instances.
	 *
	 * Fields of {@code lhs} and {@code rhs} are compared in the following order: cc, ltn, sid. The result of the
	 * comparison is based on the first field which differs. If all three fields are equal, the two instances are
	 * considered equal.
	 */
	public static class TmcServiceComparator implements Comparator<TmcService> {

		@Override
		public int compare(TmcService lhs, TmcService rhs) {
			if (lhs.cc < rhs.cc)
				return -1;
			if (lhs.cc > rhs.cc)
				return 1;
			if (lhs.ltn < rhs.ltn)
				return -1;
			if (lhs.ltn > rhs.ltn)
				return 1;
			if (lhs.sid < rhs.sid)
				return -1;
			if (lhs.sid > rhs.sid)
				return 1;
			return 0;
		}

	}
}