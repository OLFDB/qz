/*
 * Copyright © 2017 Michael von Glasow.
 * 
 * This file is part of Qz.
 *
 * Qz is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Qz is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Qz.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.vonglasow.michael.qz.core;

import java.io.File;
import java.lang.ref.WeakReference;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SimpleTimeZone;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;

import com.vonglasow.michael.qz.tuning.AfiNetwork;
import com.vonglasow.michael.qz.tuning.OtherNetwork;
import com.vonglasow.michael.qz.tuning.TmcService;
import com.vonglasow.michael.qz.util.MessageDb;
import com.vonglasow.michael.qz.util.MessageListener;

import eu.jacquet80.rds.app.oda.AlertC.Message;
import eu.jacquet80.rds.app.oda.tmc.TMC;

// TODO table of ON mapped services (cc1, ltn1, sid1, cc2, ltn2, sid2), queryable by CC-LTN-SID (symmetric)
// TODO get PI-TN by PI-AF (not the other way round)

/* NOTE: 
 * This is a portable class which should be kept clean of any dependencies on platform-specific
 * classes (JRE or Android).
 */

/**
 * @brief Handles storage, retrieval and expiration of TMC messages.
 * 
 * This class is a weak singleton, which has the following implications:
 * <ul>
 * <li>This class is never instantiated directly. Rather, an instance is obtained by calling
 * {@link #getInstance()}.</li>
 * <li>At any given time, there can never be more than one instance of this class.</li>
 * <li>The instance may be garbage-collected after the last reference to it is deleted. In that
 * case, the next call to {@link #getInstance()} will create a new instance.</li>
 * </ul>
 */
public final class MessageCache {
	/**
	 * Identifier for AlertC (tuning information) schema version in version table
	 */
	private static final String DB_NAME_ALERT_C = "AlertC";

	/**
	 * Identifier for MessageCache schema version in version table
	 */
	private static final String DB_NAME_MESSAGE_CACHE = "MessageCache";

	/** 
	 * DB schema version for persistent storage of tuning information, incremented every time the schema changes.
	 * 
	 * You may wish to store this value in (or along with) your database and compare it on startup.
	 * If the versions do not match, reinitialize your local database. 
	 */
	private static final int DB_VERSION_ALERT_C = 2;

	/** Milliseconds per minute */
	private static final int MILLIS_PER_MINUTE = 60000;

	private static final String afiNetworkQueryStmt =
			"select pi, ltn, sid from AfiNetwork;";

	private static final String afiNetworkRemoveStmt =
			"delete from AfiNetwork where pi = ?;";

	private static final String afiNetworkUpdateStmt =
			"merge into AfiNetwork using (values(?, ?, ?)) as Vals(pi, ltn, sid) on AfiNetwork.pi = Vals.pi when matched then update set AfiNetwork.ltn = Vals.ltn, AfiNetwork.sid = Vals.sid when not matched then insert (pi, ltn, sid) values (Vals.pi, Vals.ltn, Vals.sid);";

	private static final String onMappedServiceInsertStmt =
			"insert into OnMappedService (cc1, ltn1, sid1, cc2, ltn2, sid2) values(?, ?, ?, ?, ?, ?);";

	private static final String onMappedServiceQueryStmt =
			"select cc1, ltn1, sid1, cc2, ltn2, sid2 from OnMappedService;";

	private static final String otherNetworkInsertStmt =
			"insert into OtherNetwork (tnPi, tnFreq, onPi, onFreq) values(?, ?, ?, ?);";

	private static final String otherNetworkQueryStmt =
			"select tnPi, tnFreq, onPi, onFreq from OtherNetwork;";

	private static final String serviceNameQueryStmt =
			"select cc, ltn, sid, name from ServiceName;";

	private static final String serviceNameUpdateStmt =
			"merge into ServiceName using (values(?, ?, ?, ?)) as Vals(cc, ltn, sid, name) on ServiceName.cc = Vals.cc and ServiceName.ltn = Vals.ltn and ServiceName.sid = Vals.sid when matched then update set ServiceName.name = Vals.name when not matched then insert (cc, ltn, sid, name) values (Vals.cc, Vals.ltn, Vals.sid, Vals.name);";

	/** 
	 * SQL statements for persistent storage of TMC tuning information.
	 * 
	 * Note that the version table must be created separately.
	 */
	private static final String[] tuningDbInitStmts = {
		// Drop existing tables and indices
		"drop index if exists AfiNetwork_ltn_sid_idx;",
		"drop index if exists OtherNetwork_onPi_idx;",
		"drop index if exists OtherNetwork_onPi_af_idx;",
		"drop index if exists OnMappedService_cc1_ltn1_sid1_idx;",
		"drop table if exists ServiceName;",
		"drop table if exists AfiNetwork;",
		"drop table if exists OtherNetwork;",
		"drop table if exists OnMappedService;",
		// TMC Service Name (CC-LTN-SID -> Name)
		"create table ServiceName(cc varchar(1), ltn integer, sid integer, name varchar(8), primary key(cc, ltn, sid));",
		// TMC Services for networks with AFI bit (PI -> LTN-SID)
		"create table AfiNetwork(pi varchar(4) primary key, ltn integer, sid integer);",
		"create index AfiNetwork_ltn_sid_idx ON AfiNetwork(ltn, sid);",
		// Other Network (incl. without AFI, PI(-TN) -> PI-AF)
		"create table OtherNetwork(tnPi varchar(4), tnFreq decimal(4,1), onPi varchar(4), onFreq decimal(4,1));",
		"create index OtherNetwork_onPi_idx ON OtherNetwork(onPi);",
		"create index OtherNetwork_onPi_onFreq_idx ON OtherNetwork(onPi, onFreq);",
		// Services mapped via Other Networks (CC-LTN-SID -> CC-LTN-SID)
		"create table OnMappedService(cc1 varchar(1), ltn1 integer, sid1 integer, cc2 varchar(1), ltn2 integer, sid2 integer, primary key(cc1, ltn1, sid1, cc2, ltn2, sid2));",
		"create index OnMappedService_cc1_ltn1_sid1_idx ON OnMappedService(cc1, ltn1, sid1);"
	};

	/** 
	 * SQL statement for version information.
	 * 
	 * Note that tables for tuning information must be created separately.
	 */
	private static final String versionDbInitStmt =
			"create table if not exists Version(name varchar(255) primary key, version integer);";

	/** 
	 * SQL statement to query version information.
	 */
	private static final String versionDbQueryStmt =
			"select name, version from Version;";

	/** 
	 * SQL statement to update version information.
	 */
	private static final String versionDbUpdateStmt =
			"merge into Version using (values(?, ?)) as Vals(name, version) on Version.name = Vals.name when matched then update set Version.version = Vals.version when not matched then insert values Vals.name, Vals.version;";

	/**
	 * The JDBC URL to the database.
	 */
	private static String dbUrl = "jdbc:hsqldb:mem:.";

	private static WeakReference<MessageCache> instance;

	private ConcurrentHashMap<Integer, AfiNetwork> afiNetworks = new ConcurrentHashMap<Integer, AfiNetwork>();

	private Connection dbConnection;
	private MessageDb db = null;

	/** The timer to remove expired messages */
	private Timer expirationTimer;

	/** Message IDs associated with a currently valid message */
	private Set<String> idsInUse;

	/**
	 * Listeners which get notified whenever new messages are added or removed
	 */
	private List<WeakReference<MessageListener>> listeners = Collections.synchronizedList(new ArrayList<WeakReference<MessageListener>>());

	/**
	 * In-memory copy of the message cache database
	 */
	private Map<MessageWrapper, Integer> messages = new ConcurrentHashMap<MessageWrapper, Integer>();

	/**
	 * In-memory copy of service mappings
	 */
	private Set<String> onMappedServices = Collections.synchronizedSet(new HashSet<String>());

	/**
	 * In-memory copy of the OtherNetwork table, searchable by PI(-TN)
	 */
	private ConcurrentHashMap<String, List<OtherNetwork>> otherNetworks = new ConcurrentHashMap<String, List<OtherNetwork>>();

	/**
	 * Duration of a scan cycle (i.e. time it takes a scan to return to the same station), in seconds
	 */
	private int scanCycleDuration = 0;

	/**
	 * In-memory copy of the ServiceName table, searchable by CC-LTN-SID
	 */
	private ConcurrentHashMap<String, String> serviceNames = new ConcurrentHashMap<String, String>();

	/**
	 * @brief Obtains an instance of the MessageCache.
	 * 
	 * By default, the MessageCache will operate on an in-memory database, which is lost when the
	 * MessageCache is deinstantiated. To make data persistent, call {@link #setDatabaseUrl(String)}
	 * or {@link #setDatabasePath(File)} before the first call to this method.
	 * 
	 * @return The instance
	 * 
	 * @throws SQLException If any of the database operations fails
	 * @throws ClassNotFoundException If the HSQLDB JDBC driver cannot be found
	 */
	public static synchronized MessageCache getInstance() throws ClassNotFoundException, SQLException {
		MessageCache strongInstance = null;

		if (instance != null) {
			strongInstance = instance.get();
			if (strongInstance != null)
				return strongInstance;
		}

		// need to (re-)instantiate
		strongInstance = new MessageCache();
		instance = new WeakReference<MessageCache>(strongInstance);
		return strongInstance;
	}

	/**
	 * @brief Convenience method for {@link #setDatabaseUrl(String)} with a HSQLDB file URL.
	 * 
	 * This method must be called before the first call to {@link #getInstance()}, else behavior
	 * is undefined.
	 * 
	 * @param path The path to the HSQLDB database.
	 */
	public static void setDatabasePath(File path) {
		setDatabaseUrl("jdbc:hsqldb:file:" + path.getAbsolutePath());
	}

	/**
	 * @brief Sets the database URL.
	 * 
	 * This method must be called before the first call to {@link #getInstance()}, else behavior
	 * is undefined.
	 * 
	 * Only HSQLDB is supported as a database. Other DBMSes may work but are unsupported.
	 * 
	 * @param url A JDBC URL to the new database.
	 */
	public static void setDatabaseUrl(String url) {
		dbUrl = url;
	}

	/**
	 * @brief Registers a new {@link MessageListener}
	 * 
	 * @param listener The new listener
	 */
	public void addListener(MessageListener listener) {
		boolean isInList = false;
		synchronized(listeners) {
			for (WeakReference<MessageListener> ref : listeners)
				if (ref.get() == listener) {
					isInList = true;
					break;
				}
			if (!isInList)
				listeners.add(new WeakReference<MessageListener>(listener));
		}
	}

	/**
	 * @brief Retrieves the LTN and SID for a station network which has the AFI bit set.
	 * 
	 * @param pi The Program Identification (PI) code
	 * 
	 * @return An AfiNetwork instance for the network, or null if no entry was found.
	 */
	public AfiNetwork getAfiNetwork(int pi) {
		return afiNetworks.get(pi);
	}

	/**
	 * @brief Retrieves all messages currently in the cache.
	 */
	public Collection<MessageWrapper> getMessages() {
		return messages.keySet();
	}

	/**
	 * @brief Retrieves Other Network mappings for a station.
	 * 
	 * @param pi The Program Identifier (PI) of the Tuned Network
	 * @param tn The Tuned Network (TN) frequency. If null, only mappings without a TN frequency
	 * will be returned.
	 * 
	 * @return A list of mappings, or null if none are found.
	 */
	// TODO this should be by ON PI and frequency
	public List<OtherNetwork> getOtherNetworks(int pi, Float tn) {
		String piStr = String.format("%04X", pi);
		String[] keys;
		List<OtherNetwork> ret = null;

		if (tn == null)
			keys = new String[] { piStr };
		else
			keys = new String[] { piStr, String.format("%s@%.1f", piStr, tn) };

		for (String key : keys) {
			List<OtherNetwork> entries = otherNetworks.get(key);
			if ((entries != null) && !entries.isEmpty()) {
				if (ret == null)
					ret = Collections.synchronizedList(new ArrayList<OtherNetwork>());
				ret.addAll(entries);
			}
		}

		return ret;
	}

	/**
	 * @brief Retrieves the service name for a given TMC service.
	 * 
	 * @param service The service. Only the CC, LTN and SID must be supplied.
	 * 
	 * @return The same service with the name filled in, or null if no name was found.
	 */
	public TmcService getServiceName(TmcService service) {
		String name = serviceNames.get(String.format("%1X.%d.%d", service.cc, service.ltn, service.sid));
		if (name == null)
			return null;
		return new TmcService(service, name);
	}

	/**
	 * @brief Whether an Other Networks mapping exists between the two services, allowing message updates between them.
	 * 
	 * @param service1
	 * @param service2
	 * 
	 * @return True if a mapping exists, false if not. If both services are equal, true is returned.
	 */
	public boolean hasOnMappingForService(TmcService service1, TmcService service2) {
		TmcService s1, s2;

		if (service1.equals(service2))
			return true;

		/* make sure services are ordered */
		if (TmcService.COMPARATOR.compare(service1, service2) <= 0) {
			s1 = service1;
			s2 = service2;
		} else {
			s1 = service2;
			s2 = service1;
		}

		String key = String.format("%1X.%d.%d-%1X.%d.%d", s1.cc, s1.ltn, s1.sid, s2.cc, s2.ltn, s2.sid);
		return onMappedServices.contains(key);
	}

	/**
	 * @brief Stores information for an AFI network.
	 * 
	 * @param network The network
	 * 
	 * @throws SQLException
	 */
	public void putAfiNetwork(AfiNetwork network) throws SQLException {
		if(!network.equals(afiNetworks.put(network.pi, network))) {
			PreparedStatement stmt = dbConnection.prepareStatement(afiNetworkUpdateStmt);
			stmt.setString(1, String.format("%4X", network.pi));
			stmt.setInt(2, network.ltn);
			stmt.setInt(3, network.sid);
			stmt.execute();
			stmt.close();
			dbConnection.commit();
		}
	}

	/**
	 * @brief Processes a TMC message.
	 * 
	 * Processing first checks the message for completeness; messages with incomplete information
	 * are discarded. Next, any messages which the current message overrides are removed and the
	 * new message is stored.
	 * 
	 * @param message The new message
	 * @throws SQLException 
	 */
	public void putMessage(Message message) throws SQLException {
		/* drop messages which cannot be fully resolved */
		if (!message.isFullyResolved())
			return;

		/* drop messages with event code 0 or other unknown codes */
		for (int evtCode : message.getEvents()) {
			if (TMC.getEvent(evtCode).updateClass == 0)
				return;
		}

		synchronized (messages) {
			boolean needsCommit = false;
			MessageWrapper replacedWrapper = null;

			/* find messages which are overridden by the new message */
			List<MessageWrapper> messagesToRemove = new LinkedList<MessageWrapper>();
			Set<String> replacedIds = new HashSet<String>();
			int updateCount = 0;
			for (MessageWrapper w : messages.keySet()) {
				if (message.overrides(w.message) && isServiceAllowedToUpdate(message, w.message)) {
					messagesToRemove.add(w);
					if (updateCount < w.message.getUpdateCount())
						updateCount = w.message.getUpdateCount();
					if (replacedWrapper == null)
						replacedWrapper = w;
					else
						replacedIds.add(w.id);
					if (w.replacedIds != null)
						replacedIds.addAll(w.replacedIds);
				}
			}

			message.setUpdateCount(updateCount + 1);
			
			MessageWrapper wrapper;
			if (replacedWrapper != null)
				wrapper = new MessageWrapper(message, replacedIds, replacedWrapper.id,
						replacedWrapper.received, replacedWrapper.receivedTz);
			else {
				wrapper = new MessageWrapper(message, replacedIds, idsInUse);
			}

			for (MessageWrapper w : messagesToRemove) {
				db.delete(messages.get(w), false);
				messages.remove(w);
				needsCommit = true;
			}

			int id = db.store(wrapper, false);
			messages.put(wrapper, id);
			idsInUse.add(wrapper.id);
			needsCommit = true;
			notifyListeners(wrapper, messagesToRemove);

			if (needsCommit)
				dbConnection.commit();
		}
	}

	/**
	 * @brief Stores a mapping between two TMC services, allowing them to update each other’s messages.
	 * 
	 * @param service1
	 * @param service2
	 * 
	 * @throws SQLException
	 */
	public void putOnMappedService(TmcService service1, TmcService service2) throws SQLException {
		TmcService s1, s2;

		/* make sure services are ordered */
		if (TmcService.COMPARATOR.compare(service1, service2) <= 0) {
			s1 = service1;
			s2 = service2;
		} else {
			s1 = service2;
			s2 = service1;
		}

		String key = String.format("%1X.%d.%d-%1X.%d.%d", s1.cc, s1.ltn, s1.sid, s2.cc, s2.ltn, s2.sid);
		if (onMappedServices.add(key)) {
			PreparedStatement stmt = dbConnection.prepareStatement(onMappedServiceInsertStmt);
			stmt.setString(1, String.format("%1X", s1.cc));
			stmt.setInt(2, s1.ltn);
			stmt.setInt(3, s1.sid);
			stmt.setString(4, String.format("%1X", s2.cc));
			stmt.setInt(5, s2.ltn);
			stmt.setInt(6, s2.sid);
			stmt.execute();
			stmt.close();
			dbConnection.commit();
		}
	}

	/**
	 * @brief Stores an Other Network (ON) mapping.
	 * 
	 * @param on The Other Network (ON) mapping.
	 * @throws SQLException 
	 */
	public void putOtherNetwork(OtherNetwork on) throws SQLException {
		// TODO key/value will not work here, as there may well be multiple mappings for the same PI
		String key;
		if (on.tnFreq == null)
			key = String.format("%4X", on.tnPi);
		else
			key = String.format("%4X@%.1f", on.tnPi, on.tnFreq);

		List<OtherNetwork> list = otherNetworks.get(key);
		if (list == null) {
			list = Collections.synchronizedList(new ArrayList<OtherNetwork>());
			otherNetworks.put(key, list);
		}

		if (!list.contains(on)) {
			list.add(on);
			PreparedStatement stmt = dbConnection.prepareStatement(otherNetworkInsertStmt);
			stmt.setString(1, String.format("%4X", on.tnPi));
			if (on.tnFreq == null)
				stmt.setNull(2, java.sql.Types.DECIMAL);
			else
				stmt.setFloat(2, on.tnFreq);
			stmt.setString(3, String.format("%4X", on.onPi));
			stmt.setFloat(4, on.onFreq);
			stmt.execute();
			stmt.close();
			dbConnection.commit();
		}
	}

	/**
	 * @brief Stores a service name.
	 * 
	 * @param service A TMC service. The name field of the service cannot be null.
	 * @throws SQLException 
	 */
	public void putServiceName(TmcService service) throws SQLException {
		if (service.name == null)
			throw new IllegalArgumentException("Service name cannot be null");
		if (!service.name.equals(serviceNames.put(String.format("%1X.%d.%d", service.cc, service.ltn, service.sid), service.name))) {
			PreparedStatement stmt = dbConnection.prepareStatement(serviceNameUpdateStmt);
			stmt.setString(1, String.format("%1X", service.cc));
			stmt.setInt(2, service.ltn);
			stmt.setInt(3, service.sid);
			stmt.setString(4, service.name);
			stmt.execute();
			stmt.close();
			dbConnection.commit();
		}
	}

	/**
	 * @brief Removes information for an AFI network from the list
	 * 
	 * @param pi The Program Identification (PI) code for the station network
	 * 
	 * @throws SQLException 
	 */
	public void removeAfiNetwork(int pi) throws SQLException {
		if (afiNetworks.remove(pi) != null) {
			PreparedStatement stmt = dbConnection.prepareStatement(afiNetworkRemoveStmt);
			stmt.setString(1, String.format("%4X", pi));
			stmt.execute();
			stmt.close();
			dbConnection.commit();
		}
	}

	/**
	 * @brief Unregisters a {@link MessageListener}
	 * 
	 * Unregistering a null listener, or a listener that is not currently registered, is a no-op.
	 * 
	 * This method will also remove from the list any entries pointing to listeners which have been
	 * recycled by the garbage collector.
	 * 
	 * @param listener The new listener
	 */
	public void removeListener(MessageListener listener) {
		List<WeakReference<MessageListener>> toRemove = new ArrayList<WeakReference<MessageListener>>();
		synchronized(listeners) {
			for (WeakReference<MessageListener> ref : listeners)
				if ((ref.get() == listener) || (ref.get() == null))
					toRemove.add(ref);
		}
		for (WeakReference<MessageListener> ref : toRemove)
			listeners.remove(ref);
	}

	/**
	 * @brief Updates the scan cycle duration.
	 * 
	 * The scan cycle duration is the time it takes a scan to return to the same station. Setting this value affects
	 * message expiration.
	 * 
	 * @param newDuration The new duration in seconds
	 */
	public void setScanCycleDuration(int newDuration) {
		scanCycleDuration = newDuration;
	}

	protected void finalize() throws Throwable {
		expirationTimer.cancel();
		super.finalize();
	}

	/**
	 * @throws ClassNotFoundException 
	 * @brief Instantiates a new MessageCache.
	 * 
	 * This constructor is not intended to be called directly. Instead, obtain an instance by
	 * calling {@link #getInstance()}.
	 * 
	 * @throws ClassNotFoundException if the HSQLDB JDBC driver is not found
	 * @throws SQLException if the database connection, or initialization, fails
	 */
	private MessageCache() throws ClassNotFoundException, SQLException {
		PreparedStatement stmt;
		int storedMsgCacheVersion = 0;
		int storedTuningVersion = 0;

		/* make sure the driver is loaded */
		Class.forName("org.hsqldb.jdbc.JDBCDriver");

		/* prepare DB connection and dbInfo */
		dbConnection = DriverManager.getConnection(dbUrl);
		dbConnection.setAutoCommit(false);
		db = new MessageDb(dbConnection);

		/* ensure we have a version table */
		stmt = dbConnection.prepareStatement(versionDbInitStmt);
		stmt.execute();
		dbConnection.commit();

		/* query the version table */
		stmt = dbConnection.prepareStatement(versionDbQueryStmt);
		ResultSet rset = stmt.executeQuery();
		while (rset.next()) {
			String name = rset.getString("name");
			if (DB_NAME_ALERT_C.equals(name))
				storedTuningVersion = rset.getInt("version");
			else if (DB_NAME_MESSAGE_CACHE.equals(name))
				storedMsgCacheVersion = rset.getInt("version");
		}
		rset.close();
		stmt.close();
		dbConnection.commit();

		/* initialize if necessary */
		if (storedTuningVersion < DB_VERSION_ALERT_C)
			initTuningDb();
		if (storedMsgCacheVersion < MessageDb.DB_VERSION_MESSAGE_CACHE)
			initMessageCacheDb();

		idsInUse = new HashSet<String>();

		/* read data back into memory */
		readAfiNetworks();
		readOnMappedServices();
		readOtherNetworks();
		readServiceNames();
		readMessages(idsInUse);

		/* expiration timer */
		expirationTimer = new Timer(true);
		expirationTimer.schedule(new ExpirationTimerTask(), 0, MILLIS_PER_MINUTE);
	}

	/**
	 * @brief Initializes a cache database for TMC messages.
	 * 
	 * Existing tables, indices and other objects used for TMC messages will be dropped and
	 * recreated. Therefore, this mechanism can also be used to implement schema changes. Note that
	 * this will purge any stored TMC messages.
	 * 
	 * @throws SQLException 
	 */
	private void initMessageCacheDb() throws SQLException {
		db.initDb();
		updateVersionDb(DB_NAME_MESSAGE_CACHE, MessageDb.DB_VERSION_MESSAGE_CACHE);
	}

	/**
	 * @brief Initializes a cache database for TMC tuning information.
	 * 
	 * Existing tables, indices and other objects used for TMC tuning information will be dropped
	 * and recreated. Therefore, this mechanism can also be used to implement schema changes. Note
	 * that this will purge any stored TMC tuning information.
	 * 
	 * @throws SQLException 
	 */
	private void initTuningDb() throws SQLException {
		for (String stmtRaw: tuningDbInitStmts) {
			PreparedStatement stmt = dbConnection.prepareStatement(stmtRaw);
			stmt.execute();
			stmt.close();
			dbConnection.commit();
		}
		updateVersionDb(DB_NAME_ALERT_C, DB_VERSION_ALERT_C);
	}

	/**
	 * @brief Whether the message has expired and should be purged from the cache.
	 * 
	 * A message is considered to have expired if all of the following are true:
	 * <ul>
	 * <li>Its persistence (as returned by {@link Message#getPersistence()}, i.e. the timestamp of the last update plus
	 * a duration specified in the message) has elapsed.</li>
	 * <li>Its start time, if set, has elapsed.</li>
	 * <li>Its stop time, if set, has elapsed.</li>
	 * <li>A period twice the duration of a scan cycle has elapsed since the message was last updated.</li>
	 * </ul>
	 * 
	 * Doubling the scan cycle duration is for improved resilience: The scan cycle is the time elapsed since the last
	 * time a scan stopped at the same frequency. If we were to rely simply on that duration, a message which was last
	 * received immediately after tuning into the station might expire before it is updated, even as we are listening
	 * to the same station again, if no other station carries the same service (or a compatible one which would serve
	 * the same message). Doubling expiration time takes that into account and also adds some resilience for other
	 * unforeseeable events.
	 * 
	 * @param w The {@link MessageWrapper} for the message to examine
	 * 
	 * @return True if the message has expired, false if not.
	 */
	private boolean isMessageExpired(MessageWrapper w) {
		Date now = new Date();
		Date then;
		Date expiration = w.message.getPersistence();
		Calendar cal = new GregorianCalendar(new SimpleTimeZone(0, ""));

		then = w.getStartTime();
		if ((then != null) && (expiration.before(then)))
			expiration = then;

		then = w.getStopTime();
		if ((then != null) && (expiration.before(then)))
			expiration = then;

		cal.clear();
		cal.setTime(w.message.getTimestamp());
		cal.setTimeZone(new SimpleTimeZone(0, ""));
		/* get a value from the calendar to normalize fields */
		cal.get(Calendar.SECOND);
		cal.setLenient(true);
		cal.add(Calendar.SECOND, scanCycleDuration * 2);
		then = cal.getTime();
		if ((then != null) && (expiration.before(then)))
			expiration = then;

		return expiration.before(now);
	}

	/**
	 * @brief Whether two messages originate from TMC services which may update each other’s messages.
	 * 
	 * Messages originating from the same service can always update each other as long as all other
	 * criteria (location, direction, update class and forecast duration) match. Updating between
	 * services is controlled by Variant 9 Tuning Information messages: the TMC service specified
	 * in the message may update messages from the TMC service of the broadcasting station, and
	 * vice versa. This relationship presumably applies to all stations carrying one of the two TMC
	 * services involved, despite the tuning message explicitly mentioning a station network.
	 * (The specs are somewhat vague on this but this seems like the logical conclusion.)
	 * 
	 * The designers of TMC assumed that a receiver would mostly remain within the same ecosystem of TMC services with
	 * ON mappings between them, retuning only when reception degrades and relying on tuning information wherever
	 * available. Scanning the entire FM band would only happen if a receiver lost reception and none of the cached
	 * tuning information pointed to a service available in the area. This makes message updates between services not
	 * aware of each other a rare occurrence.
	 * 
	 * Qz takes a different approach by aggressively scanning the entire FM band for TMC messages. If two competing TMC
	 * services exist in the same broadcast area, Qz wil pick up both, whereas a compliant receiver would only pick up
	 * one of the two. Therefore, Qz may easily receive messages for the same location from multiple services.
	 * 
	 * However, two different services may carry slightly different information: the extent may differ, as do the
	 * messages themselves, either because the services are operating on different information or simply because TMC
	 * often allows for different ways of encoding the same information. Allowing cross-service updates could then
	 * result in “flapping” messages, which alternate between one message and the other as Qz alternates between
	 * the two services. If the two services refer to each other via ON, they are more likely to carry identical
	 * messages. For this reason, Qz allows cross-service updates only between ON mapped services.
	 * 
	 * @param m1 The first message
	 * @param m2 The second message
	 * 
	 * @return True if updates between the two services are allowed, false if not.
	 */
	private boolean isServiceAllowedToUpdate(Message m1, Message m2) {
		/* check if both messages originate from the same service */
		if (((m1.cc == m2.cc)
				&& (m1.getLocationTableNumber() == m2.getLocationTableNumber())
				&& (m1.getSid() == m2.getSid())))
			return true;
		if (hasOnMappingForService(new TmcService(m1.cc, m1.getLocationTableNumber(), m1.getSid()),
				new TmcService(m2.cc, m2.getLocationTableNumber(), m2.getSid())))
			return true;
		/* if nothing else matches, return false */
		return false;
	}

	/**
	 * @brief Notifies all registered listeners about changes to the message cache.
	 * 
	 * @param added A newly added or updated message
	 * @param removed Superseded, canceled or expired messages
	 */
	private void notifyListeners(MessageWrapper added, Collection<MessageWrapper> removed) {
		synchronized(listeners) {
			for (WeakReference<MessageListener> ref : listeners) {
				MessageListener listener = ref.get();
				if (listener != null)
					listener.onUpdateReceived(added, removed);
			}
		}
	}

	/**
	 * @brief Reads the AfiNetworks table into memory.
	 * @throws SQLException 
	 */
	private void readAfiNetworks() throws SQLException {
		PreparedStatement stmt = dbConnection.prepareStatement(afiNetworkQueryStmt);
		ResultSet rset = stmt.executeQuery();
		while (rset.next())
			try {
				int pi = Integer.parseInt(rset.getString("pi").trim(), 16);
				int ltn = rset.getInt("ltn");
				int sid = rset.getInt("sid");
				afiNetworks.put(pi, new AfiNetwork(pi, ltn, sid));
			} catch (NumberFormatException e) {
				e.printStackTrace();
			}
		rset.close();
		stmt.close();
		dbConnection.commit();
	}

	/**
	 * @brief Reads the message database into main memory.
	 * @throws SQLException 
	 */
	private void readMessages(Set<String> idsInUse) throws SQLException {
		Map<MessageWrapper, Integer> restoredMessages = db.readMessages(true, idsInUse);
		messages.putAll(restoredMessages);
	}

	/**
	 * @brief Reads the OnMappedNetwork table into memory.
	 * @throws SQLException
	 */
	private void readOnMappedServices() throws SQLException {
		PreparedStatement stmt = dbConnection.prepareStatement(onMappedServiceQueryStmt);
		ResultSet rset = stmt.executeQuery();
		while (rset.next())
			try {
				int cc1 = Integer.parseInt(rset.getString("cc1").trim(), 16);
				int ltn1 = rset.getInt("ltn1");
				int sid1 = rset.getInt("sid1");
				int cc2 = Integer.parseInt(rset.getString("cc2").trim(), 16);
				int ltn2 = rset.getInt("ltn2");
				int sid2 = rset.getInt("sid2");
				onMappedServices.add(String.format("%1X.%d.%d-%1X.%d.%d", cc1, ltn1, sid1, cc2, ltn2, sid2));
			} catch (NumberFormatException e) {
				e.printStackTrace();
			}
		rset.close();
		stmt.close();
		dbConnection.commit();
	}

	/**
	 * @brief Reads the OtherNetwork table into memory.
	 * @throws SQLException
	 */
	private void readOtherNetworks() throws SQLException {
		PreparedStatement stmt = dbConnection.prepareStatement(otherNetworkQueryStmt);
		ResultSet rset = stmt.executeQuery();
		while (rset.next())
			try {
				int tnPi = Integer.parseInt(rset.getString("tnPi").trim(), 16);
				Float tnFreq = rset.getFloat("tnFreq");
				if (tnFreq <= OtherNetwork.MIN_FREQ)
					tnFreq = null;
				int onPi = Integer.parseInt(rset.getString("onPi").trim(), 16);
				float onFreq = rset.getFloat("onFreq");

				OtherNetwork on = new OtherNetwork(tnPi, tnFreq, onPi, onFreq);

				String key;
				if (on.tnFreq == null)
					key = String.format("%4X", on.tnPi);
				else
					key = String.format("%4X@%.1f", on.tnPi, on.tnFreq);

				List<OtherNetwork> list = otherNetworks.get(key);
				if (list == null) {
					list = Collections.synchronizedList(new ArrayList<OtherNetwork>());
					otherNetworks.put(key, list);
				}

				if (!list.contains(on)) {
					list.add(on);
				}
			} catch (NumberFormatException e) {
				e.printStackTrace();
			}
		rset.close();
		stmt.close();
		dbConnection.commit();
	}

	/**
	 * @brief Reads the ServiceName table into memory.
	 * @throws SQLException 
	 */
	private void readServiceNames() throws SQLException {
		PreparedStatement stmt = dbConnection.prepareStatement(serviceNameQueryStmt);
		ResultSet rset = stmt.executeQuery();
		while (rset.next())
			try {
				int cc = Integer.parseInt(rset.getString("cc").trim(), 16);
				int ltn = rset.getInt("ltn");
				int sid = rset.getInt("sid");
				String name = rset.getString("name");
				serviceNames.put(String.format("%1X.%d.%d", cc, ltn, sid), name);
			} catch (NumberFormatException e) {
				e.printStackTrace();
			}
		rset.close();
		stmt.close();
		dbConnection.commit();
	}

	/**
	 * @brief Updates version information in the version table.
	 * 
	 * @param name The name, must be one of the {@code DB_NAME_*} constants
	 * @param version The version
	 * 
	 * @throws SQLException 
	 */
	private void updateVersionDb(String name, int version) throws SQLException {
		PreparedStatement stmt = dbConnection.prepareStatement(versionDbUpdateStmt);
		stmt.setString(1, name);
		stmt.setInt(2, version);
		stmt.execute();
		stmt.close();
		dbConnection.commit();
	}

	private class ExpirationTimerTask extends TimerTask {
		public void run() {
			/* purge expired messages */
			synchronized (messages) {
				boolean needsCommit = false;

				/* find expired messages */
				List<MessageWrapper> messagesToRemove = new LinkedList<MessageWrapper>();
				for (MessageWrapper w : messages.keySet()) {
					if (isMessageExpired(w))
						messagesToRemove.add(w);
				}

				for (MessageWrapper w : messagesToRemove) {
					try {
						db.delete(messages.get(w), false);
					} catch (SQLException e) {
						e.printStackTrace();
					}
					messages.remove(w);
					idsInUse.remove(w.id);
					if (w.replacedIds != null)
						idsInUse.removeAll(w.replacedIds);
					needsCommit = true;
				}

				notifyListeners(null, messagesToRemove);

				if (needsCommit)
					try {
						dbConnection.commit();
					} catch (SQLException e) {
						e.printStackTrace();
					}
			}
		}
	}
}
