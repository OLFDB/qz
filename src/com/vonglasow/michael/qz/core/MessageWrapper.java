package com.vonglasow.michael.qz.core;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.SimpleTimeZone;
import java.util.TimeZone;
import java.util.regex.Pattern;

import eu.jacquet80.rds.app.oda.AlertC;
import eu.jacquet80.rds.app.oda.AlertC.InformationBlock;
import eu.jacquet80.rds.app.oda.AlertC.Message;
import eu.jacquet80.rds.app.oda.tmc.Segment;
import eu.jacquet80.rds.app.oda.tmc.SupplementaryInfo;
import eu.jacquet80.rds.app.oda.tmc.TMC;
import eu.jacquet80.rds.app.oda.tmc.TMCEvent.EventUrgency;
import eu.jacquet80.rds.app.oda.tmc.TMCLocation;
import eu.jacquet80.rds.app.oda.tmc.TMCLocation.LocationClass;
import eu.jacquet80.rds.app.oda.tmc.TMCPoint;

/**
 * @brief A wrapper around a {@link Message} which can be stored in a {@link MessageCache}. 
 *
 * This class adds Qz-specific members to a {@link Message}.
 */
public class MessageWrapper {

	/**
	 * Road class types.
	 */
	public static enum RoadClass {
		MOTORWAY,
		TRUNK,
		PRIMARY,
		SECONDARY,
		TERTIARY,
		OTHER,
		NONE,
	}

	/** Pattern for parsing data files */
	static Pattern colonPattern = Pattern.compile(";");

	/** Pattern for parsing quantifier lists */
	static Pattern commaPattern = Pattern.compile(",");

	/** The encapsulated message. */
	public final Message message;
	
	/** A unique identifier. If a message is replaced with a new one, the identifier is retained. */
	public final String id;

	/** IDs of other messages replaced by this message */
	public final Set<String> replacedIds;

	/** When the message was first received. */
	public final Date received;

	/** Time zone for {@link #received} **/
	public final TimeZone receivedTz;

	/** TMC to TraFF event mapping */
	private static Map<Integer, List<TraffEventMapping>> EVENT_MAPPINGS = new HashMap<Integer, List<TraffEventMapping>>();
	static {
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(MessageWrapper.class.getResourceAsStream("event.dat")));
			// first line contains column headers, skip
			br.readLine();
			String line;
			while ((line = br.readLine()) != null)
				try {
					TraffEventMapping mapping = new TraffEventMapping(line);
					if (!EVENT_MAPPINGS.containsKey(mapping.tmcId))
						EVENT_MAPPINGS.put(mapping.tmcId, new ArrayList<TraffEventMapping>());
					EVENT_MAPPINGS.get(mapping.tmcId).add(mapping);
				} catch (IllegalArgumentException e) {
					// NOP (invalid lines are skipped)
				}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/** TMC to TraFF supplementary information mapping */
	private static Map<Integer, TraffSiMapping> SI_MAPPINGS = new HashMap<Integer, TraffSiMapping>();
	static {
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(MessageWrapper.class.getResourceAsStream("si.dat")));
			// first line contains column headers, skip
			br.readLine();
			String line;
			while ((line = br.readLine()) != null)
				try {
					TraffSiMapping mapping = new TraffSiMapping(line);
					SI_MAPPINGS.put(mapping.tmcId, mapping);
				} catch (IllegalArgumentException e) {
					// NOP (invalid lines are skipped)
				}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/** TMC to TraFF quantifier mapping */
	private static String[] TMC_Q_TYPES = {
			"int",         //  0, integer
			"int",         //  1, large integer
			"dimension",   //  2, visibility
			"percent",     //  3, probability
			"speed",       //  4, speed (other than moving traffic)
			"duration",    //  5, duration
			"temperature", //  6, temperature
			"time",        //  7, time
			"weight",      //  8, weight
			"dimension",   //  9, dimension
			"dimension",   // 10, precipitation
			"frequency",   // 11, frequency in MHz
			"frequency"    // 12, frequency in kHz
	};

	/**
	 * @brief Creates a new message wrapper using data from an existing message.
	 * 
	 * This constructor is intended for use with messages which replace an existing message. The new message wrapper is
	 * assigned the specified identifier, while the identifiers of all other messages in {@code messagesToRemove} are
	 * stored in the {@ref #replacedIds} member.
	 * 
	 * @param message The new message
	 * @param replacedIds IDs of messages replaced by {@code message}, not including {@code id}
	 * @param id The identifier for the new message, inherited from one of the replaced messages
	 * @param received The timestamp when the first replaced message was received
	 * @param receivedTz The time zone for {@code received}
	 */
	public MessageWrapper(Message message, Set<String> replacedIds, String id, Date received, TimeZone receivedTz) {
		this.message = message;

		if ((replacedIds != null) && !replacedIds.isEmpty())
			this.replacedIds = replacedIds;
		else
			this.replacedIds = null;

		this.id = id;
		this.received = received;
		this.receivedTz = receivedTz;
	}

	/**
	 * @brief Creates a new message wrapper for a new message.
	 * 
	 * This constructor is intended for use with messages that do not replace any existing messages, or replace
	 * multiple messages without a clear single “ancestor”.
	 * 
	 * The new message wrapper is assigned a new identifier that is guaranteed to be unique. In its simplest form, an
	 * identifier is of the form {@code tmc:a.1.0:a.1.32908.n.3,5} ({@code tmc} prefix, sending service ID, fully
	 * qualified location, direction and update classes). If that identifier is currently in use, it is extended with
	 * a dot and a number which is incremented until the resulting identifier is unique (e.g.
	 * {@code tmc:a.1.0:a.1.32908.n.3,5.2}).
	 * 
	 * Identifiers of the messages in {@code messagesToRemove} are stored in the {@ref #replacedIds} member.
	 * 
	 * @param message The new message
	 * @param replacedIds IDs of messages replaced by {@code message}, not including {@code id}
	 * @param idsInUse Identifiers already used by currently active messages
	 */
	public MessageWrapper(Message message, Set<String> replacedIds, Set<String> idsInUse) {
		this.message = message;
		this.received = message.getTimestamp();
		this.receivedTz = message.timeZone;
		
		/* build a fresh ID of the form tmc:a.1.0:a.1.32908.n.3,5.42 */
		Boolean isFirstUpdateClass = true;
		StringBuilder idBuilder = new StringBuilder("tmc:");
		idBuilder.append(String.format("%01x.%d.%d:", message.cc, message.getLocationTableNumber(), message.getSid()));
		if (message.interroad)
			idBuilder.append(String.format("%01x.%d.%d.", message.fcc, message.getForeignLocationTableNumber(), message.lcid));
		else
			idBuilder.append(String.format("%01x.%d.%d.", message.cc, message.getLocationTableNumber(), message.lcid));
		idBuilder.append(message.direction != 0 ? "n" : "p");
		long updateClasses = 0;
		for (int evtCode : message.getEvents())
			updateClasses |= 1L << (TMC.getEvent(evtCode).updateClass - 1);
		for (int i = 1; i <= 39; i++)
			if ((updateClasses & (1L << (i - 1))) != 0) {
				if (isFirstUpdateClass) {
					idBuilder.append(".").append(i);
					isFirstUpdateClass = false;
				} else
					idBuilder.append(",").append(i);
			}
		String tmpId = idBuilder.toString();
		int i = 0;
		while (idsInUse.contains(tmpId)) {
			i++;
			tmpId = String.format("%s.%d", idBuilder.toString(), i);
		}
		this.id = tmpId;

		if ((replacedIds != null) && !replacedIds.isEmpty())
			this.replacedIds = replacedIds;
		else
			this.replacedIds = null;
	}

	/**
	 * @brief Accepts a {@link WrappedMessageVisitor}.
	 * 
	 * Ths method invokes the visitor’s {@link WrappedMessageVisitor#visit(Message)} method on the current instance and
	 * calls the {@link Message#accept(MessageVisitor)} of the message, in the order specified by {@code parentFirst}.
	 * 
	 * Note that, for the order of traversal by visitors, the message wrapper is treated as if it were a child of the
	 * message, not its parent.
	 * 
	 * @param visitor The visitor
	 * @param parentFirst If true, the parent will be visited before its first child. If false,
	 * the parent will be visited after its last child.
	 */
	public void accept(WrappedMessageVisitor visitor, boolean parentFirst) {
		if (!parentFirst)
			visitor.visit(this);
		message.accept(visitor, parentFirst);
		if (parentFirst)
			visitor.visit(this);
	}

	/**
	 * @brief Returns the road class for the location of the message.
	 * 
	 * TMC has no dedicated type for trunk roads: Some countries may classify motorway-grade trunk roads as motorways
	 * for TMC purposes. Other countries may classify them as order 1 roads ({@code RoadClass#PRIMARY}). In this case,
	 * the next-lower road level may either be classified as the same level, or as {@code RoadClass#SECONDARY}.
	 * 
	 * For this reason, {@code RoadClass#TRUNK} will never be returned for some countries.
	 * 
	 * Also, TMC knows only two types of ring roads: motorway and non-motorway. For ring roads of the latter type,
	 * {@code RoadClass#OTHER} will be returned.
	 * 
	 * @param message
	 * 
	 * @return The road class, or {@code RoadClass#NONE} if the location does not refer to a road
	 */
	public static RoadClass getRoadClass(Message message) {
		TMCLocation location = message.location;
		boolean hasTrunkRoads = MessageWrapper.hasTrunkRoads(message.fcc, message.getForeignLocationTableNumber());

		while (location != null) {
			if (LocationClass.LINE.equals(location.category)) {
				if (location.tcd == 1) {
					switch (location.stcd) {
					case 1:
						return RoadClass.MOTORWAY;
					case 2:
						return hasTrunkRoads ? RoadClass.TRUNK : RoadClass.PRIMARY;
					case 3:
						return hasTrunkRoads ? RoadClass.PRIMARY : RoadClass.SECONDARY;
					case 4:
						return hasTrunkRoads ? RoadClass.SECONDARY: RoadClass.TERTIARY;
					default:
						return RoadClass.OTHER;
					}
				} else if ((location.tcd == 2) && (location.stcd == 1)) {
					return RoadClass.MOTORWAY;
				}
			}

			if (location instanceof TMCPoint) {
				if (((TMCPoint) location).road != null)
					location = ((TMCPoint) location).road;
				else
					location = ((TMCPoint) location).segment;
			} else if (location instanceof Segment) {
				if (((Segment) location).road != null)
					location = ((Segment) location).road;
				else
					location = ((Segment) location).segment;
			} else
				return RoadClass.NONE;
		}

		return RoadClass.NONE;
	}

	/**
	 * @brief Whether the location table has a category for trunk roads.
	 * 
	 * @param cc The country code
	 * @param ltn The location table number
	 * 
	 * @return
	 */
	public static boolean hasTrunkRoads(int cc, int ltn) {
		// TODO complete this table
		switch(cc << 8 | ltn) {
		// TODO Belgium
		// TODO Luxembourg
		// TODO Netherlands
		// TODO Sweden
		// TODO Slovakia
		// case 0x219: /* 2/25: Czech Republic */
		case 0x409: /* 4/9: Switzerland */
			return false;
		case 0x501: /* 5/1: Italy */
			return true;
		case 0x611: /* 6/17: Finland */
			return false;
		case 0x909: /* 9/9: Denmark */
			return false;
		case 0x923: /* 9/35: Slovenia */
			return false;
		case 0xa01: /* A/1: Austria */
			return true;
			// case 0xc07: /* C/7: UK */
		case 0xd01: /* D/1: Germany */
			return false;
			// case 0xe11: /* E/17: Spain */
		case 0xf20: /* F/32: France */
			return false;
		case 0xf31: /* F/49: Norway */
			return false;
		}
		return true;
	}

	/**
	 * @brief Returns the ramps value of the message.
	 * 
	 * @return The ramps value.
	 */
	public TraffRamps getRamps() {
		TraffRamps result = null;
		for (InformationBlock ib : message.getInformationBlocks())
			for (AlertC.Event ev : ib.getEvents()) {
				List<TraffEventMapping> mappings = EVENT_MAPPINGS.get(ev.tmcEvent.code);
				if ((mappings == null) || (mappings.isEmpty()))
					continue;
				for (TraffEventMapping mapping : mappings) {
					result = mergeRamps(result, mapping.ramps);
					if ((mapping.siClass != null) && (mapping.si != null))
						for (String siType : mapping.si) {
							if ("PLACE".equalsIgnoreCase(mapping.siClass) && "S_PLACE_RAMP".equalsIgnoreCase(siType))
								result = mergeRamps(result, TraffRamps.TRAFF_RAMPS_GENERIC);
						}
				}
			}
		if (result == null)
			result = TraffRamps.TRAFF_RAMPS_NONE;
		return result;
	}

	/**
	 * @brief Returns the road class for the location of the message.
	 * 
	 * See {@code #getRoadClass(Message)} for details.
	 * 
	 * @return The road class, or {@code RoadClass#NONE} if the location does not refer to a road
	 */
	public RoadClass getRoadClass() {
		return getRoadClass(message);
	}

	/**
	 * @brief Returns the start time for a message.
	 * 
	 * See {@link #resolveTime(int)} for details on how this is calculated.
	 * 
	 * @return The start time, or null if not set or invalid
	 */
	public Date getStartTime() {
		return resolveTime(message, message.startTime);
	}

	/**
	 * @brief Returns the stop time for a message.
	 * 
	 * See {@link #resolveTime(int)} for details on how this is calculated.
	 * 
	 * @return The stop time, or null if not set or invalid
	 */
	public Date getStopTime() {
		return resolveTime(message, message.stopTime);
	}

	/**
	 * @brief Converts internal start/stop time into a timestamp.
	 * 
	 * Timestamps are interpreted relative to the last update of the message.
	 * 
	 * Times given in hours and minutes are always on the day on which the message was received, and can thus be in the
	 * past. Times given as days and hours after midnight are inherently in the future. All other times are interpreted
	 * as the next matching occurrence in the future. For example, a reference of “day 15” refers to the current month
	 * if received on the 10th, but to the next month if received on the 20th.
	 * 
	 * Middle and end of month references are interpreted as the 15th and last day of the month, respectively. It is
	 * expected that TMC services replace such references with a more precise one as the date approaches.
	 * 
	 * Times given with granularities of a day or half a month are interpreted as 00:00 UTC on that day.
	 * 
	 * All time calculations are based on UTC: Days are assumed to begin at 00:00 UTC, and timestamps with hour or
	 * minute granularity are interpreted as UTC.
	 * 
	 * @param relTime TMC start or stop time
	 * 
	 * @return The absolute timestamp, or null
	 */
	public static Date resolveTime(Message message, int relTime) {
		Date baseTime = message.getTimestamp();
		Calendar cal = new GregorianCalendar(new SimpleTimeZone(0, ""));

		cal.clear();
		cal.setTime(baseTime);
		cal.setTimeZone(new SimpleTimeZone(0, ""));
		/* get a value from the calendar to normalize fields */
		cal.get(Calendar.SECOND);
		cal.setLenient(true);

		if (relTime >= 0 && relTime <= 95) {
			/* today hh:mm, in multiples of 15 minutes (this can be in the past) */
			cal.set(Calendar.HOUR_OF_DAY, relTime / 4);
			cal.set(Calendar.MINUTE, (relTime % 4) * 15);
			cal.set(Calendar.SECOND, 0);
			cal.set(Calendar.MILLISECOND, 0);
		} else if (relTime >= 96 && relTime <= 200) {
			/* days and hours after midnight (24:00 today), in full hours */
			cal.add(Calendar.DAY_OF_MONTH, (relTime - 96) / 24);
			cal.set(Calendar.HOUR_OF_DAY, 24 + (relTime - 96) % 24);
			cal.set(Calendar.MINUTE, 0);
			cal.set(Calendar.SECOND, 0);
			cal.set(Calendar.MILLISECOND, 0);
		} else if (relTime >= 201 && relTime <= 231) {
			/* day of month (first occurrence in the future, i.e. current or next month) */
			int day = relTime - 200;
			if (cal.get(Calendar.DAY_OF_MONTH) >= day)
				cal.add(Calendar.MONTH, 1);
			cal.set(Calendar.DAY_OF_MONTH, day);
			cal.set(Calendar.HOUR_OF_DAY, 0);
			cal.set(Calendar.MINUTE, 0);
			cal.set(Calendar.SECOND, 0);
			cal.set(Calendar.MILLISECOND, 0);
		} else if (relTime >= 232 && relTime <= 255) {
			/* middle or end of month, in multiples of half months
			 * (current or next year, whichever is in the future and occurs first)
			 */
			/* month, zero-based (0 = January) */
			int month = (relTime - 231) / 2 - 1;
			/* half of month (0 = mid, 1 = end of) */
			int half = (relTime - 231) % 2;
			if ((cal.get(Calendar.MONTH) > month) || ((cal.get(Calendar.MONTH) == month) &&
					((cal.get(Calendar.DAY_OF_MONTH) >= 15) || (half == 1))))
				cal.add(Calendar.YEAR, 1);
			/* for end of month, set date to the 0th of the following month */
			cal.set(Calendar.MONTH, month + half);
			cal.set(Calendar.DAY_OF_MONTH, half == 0 ? 15 : 0);
			cal.set(Calendar.HOUR_OF_DAY, 0);
			cal.set(Calendar.MINUTE, 0);
			cal.set(Calendar.SECOND, 0);
			cal.set(Calendar.MILLISECOND, 0);
		} else {
			/* invalid time value */
			return null;
		}
		return cal.getTime();
	}

	/**
	 * @brief Converts a message to TraFF format.
	 * 
	 * Since not all TMC features are supported in TraFF, some messages cannot be converted. These include:
	 * <ul>
	 * <li>Messages whose primary location is not a point location</li>
	 * <li>Messages that do not contain any events supported in TraFF</li>
	 * </ul> 
	 * 
	 * @return The message in TraFF XML format, or null if the message cannot be converted
	 */
	public String toXml() {
		if ((message.location == null) || !(message.location instanceof TMCPoint))
			return null;

		StringBuilder builder = new StringBuilder();
		DateFormat df;
		df = createIsoDateFormat(receivedTz);

		builder.append(String.format("  <message id=\"%s\" receive_time=\"%s\"", id, df.format(received)));
		df.setTimeZone(TimeZone.getTimeZone(message.timeZone.getID()));
		builder.append(String.format(" update_time=\"%s\"", df.format(message.getTimestamp())));
		if (message.isCancellation()) {
			builder.append(" cancellation=\"true\"");
		} else {
			appendTime(builder, "start_time", getStartTime());
			appendTime(builder, "end_time", getStopTime());
			builder.append(String.format(" expiration_time=\"%s\"", df.format(message.getPersistence())));
			/* FIXME make method visible
			if (message.isForecastMessage())
				builder.append(" forecast=\"true\"");
			 */
			if (message.urgency == EventUrgency.URGENT)
				builder.append(" urgency=\"URGENT\"");
			else if (message.urgency == EventUrgency.XURGENT)
				builder.append(" urgency=\"X_URGENT\"");
		}
		builder.append(">\n");

		if ((this.replacedIds != null) && !this.replacedIds.isEmpty()) {
			builder.append("    <merge>\n");
			for (String replacedId : replacedIds)
				builder.append(String.format("      <replaces id=\"%s\"/>\n", replacedId));
			builder.append("    </merge>\n");
		}

		if (message.isCancellation()) {
			builder.append("  </message>\n");
			return builder.toString();
		}

		/* location */
		builder.append("    <location fuzziness=\"LOW_RES\"");
		// TODO location.destination
		if (message.isBidirectional)
			builder.append(" directionality=\"BOTH_DIRECTIONS\"");
		else
			builder.append(" directionality=\"ONE_DIRECTION\"");

		builder.append(" ramps=\"").append(getStringFromRamps(getRamps())).append("\"");

		switch (getRoadClass()) {
		case MOTORWAY:
			builder.append(" road_class=\"MOTORWAY\"");
			break;
		case TRUNK:
			builder.append(" road_class=\"TRUNK\"");
			break;
		case PRIMARY:
			builder.append(" road_class=\"PRIMARY\"");
			break;
		case SECONDARY:
			builder.append(" road_class=\"SECONDARY\"");
			break;
		case TERTIARY:
			builder.append(" road_class=\"TERTIARY\"");
			break;
		case OTHER:
			builder.append(" road_class=\"OTHER\"");
			break;
		default:
			/* NOP */
			break;
		}
		String roadRef = message.getRoadNumber();
		if (roadRef != null)
			builder.append(String.format(" road_ref=\"%s\"", roadRef));
		if ((message.location != null) && (message.location.roadName != null))
			builder.append(String.format(" road_name=\"%s\"", message.location.roadName.name));
		builder.append(">\n");

		TMCPoint from = null, to = null, at = null, via = null, not_via = null, aux = null;

		boolean isOnRingRoad = false;

		/* figure out if we’re on a ring road (L2.* location class) */
		if ((((TMCPoint) message.location).road != null)
				&& (((TMCPoint) message.location).road.category == LocationClass.LINE)
				&& (((TMCPoint) message.location).road.tcd == 2))
			isOnRingRoad = true;

		if (message.extent > 0) {
			/* nonzero extent, we have a distinct from and to point */
			from = (TMCPoint) message.getSecondaryLocation();
			to = (TMCPoint) message.location;
			/* if the location refers to a ring road, add via (half the extent) or not_via (if extent is 1) */
			if (isOnRingRoad) {
				if (message.extent > 1)
					via = to.getOffset(message.extent / 2, message.direction);
				else
					not_via = from.getOffset(1, message.direction);
			}
		} else {
			at = (TMCPoint) message.location;
			/* add from or to (or both for ring roads) */
			aux = at.getOffset(1, message.direction);
			if (!aux.equals(at))
				from = aux;
			if (aux.equals(at) || isOnRingRoad) {
				aux = at.getOffset(1, message.direction == 0 ? 1 : 0);
				if (!aux.equals(at))
					to = aux;
			}
		}

		appendLocation(builder, from, "from");
		appendLocation(builder, to, "to");
		appendLocation(builder, at, "at");
		appendLocation(builder, via, "via");
		appendLocation(builder, not_via, "not_via");

		builder.append("    </location>\n");

		/* events */
		boolean hasEvents = false;
		builder.append("    <events");
		builder.append(">\n");
		for (InformationBlock ib : message.getInformationBlocks())
			for (AlertC.Event ev : ib.getEvents())
				hasEvents |= appendEvent(builder, ib, ev, message.getTimestamp());
		if (!hasEvents)
			return null;
		builder.append("    </events>\n");

		builder.append("  </message>\n");
		return builder.toString();
	}

	/**
	 * @brief Appends an event to the TraFF {@link StringBuilder} for a message.
	 * 
	 * If no TraFF mapping for the event exists, nothing is appended and false is returned.
	 * 
	 * @param builder The string builder
	 * @param ib The information block which contains {@code event}
	 * @param event The event to append
	 * @param timestamp The time at which the last update to the message was received
	 * @return True if data was appended, false if not
	 */
	private static boolean appendEvent(StringBuilder builder, InformationBlock ib, AlertC.Event event, Date timestamp) {
		List<TraffEventMapping> mappings = EVENT_MAPPINGS.get(event.tmcEvent.code);
		if ((mappings == null) || (mappings.isEmpty()))
			return false;
		for (TraffEventMapping mapping : mappings) {
			builder.append(String.format("      <event class=\"%s\" type=\"%s\"", mapping.traffClass, mapping.traffEvent));

			/* length */
			if (ib.length >= 0)
				builder.append(String.format(" length=\"%d\"", ib.length * 1000));
			else if (mapping.length >= 0)
				builder.append(String.format(" length=\"%d\"", mapping.length));

			/* 
			 * speed
			 * Speed may be specified in multiple ways:
			 * - Implied in the event: `mapping.speed` holds the integer value
			 * - Via a regular quantifier: `mapping.speed` is `$Q`, speed is determined from quantifiers
			 * - Via a speed field in the information block
			 * Where multiple speeds are given, the lowest applies.
			 */
			int speed = -1;
			if ("$Q".equals(mapping.speed)) {
				if ((event.tmcEvent.quantifierType == 4) && (event.quantifier != -1))
					speed = (int) getQuantifierFromEvent(event);
			} else if (mapping.speed != null)
				speed = Integer.parseInt(mapping.speed);
			if (speed == -1)
				speed = ib.speed;
			else if (ib.speed != -1)
				speed = Math.min(speed, ib.speed);
			if (speed >= 0)
				builder.append(String.format(" speed=\"%d\"", speed));

			/* quantifiers */
			if (mapping.qType != null && mapping.quantifier != null) {
				ArrayList<String> qList;
				String qType = null;
				float q = -1;
				if ((event.tmcEvent.quantifierType >= 0) && (event.tmcEvent.quantifierType < TMC_Q_TYPES.length))
					qType = TMC_Q_TYPES[event.tmcEvent.quantifierType];
				if ("ints".equals(mapping.qType) && "int".equals(qType))
					qType = "ints";
				if (mapping.qType.equals(qType))
					q = getQuantifierFromEvent(event);
				if ((q != -1) && mapping.traffEvent.endsWith("ABOVE_ELEVATION"))
					q *= 100;
				qList = new ArrayList<String>(Arrays.asList(commaPattern.split(mapping.quantifier)));
				for (int i = 0; i < qList.size(); i++)
					qList.set(i, qList.get(i).trim());
				if (q == -1)
					while (qList.contains("$Q"))
						qList.remove("$Q");
				else
					for (int i = 0; i < qList.size(); i++)
						if ("$Q".equals(qList.get(i))) {
							if ("duration".equals(mapping.qType))
								qList.set(i, String.format("%02d:%02d:00", (int)(q / 60), (int)(q % 60)));
							else if ("time".equals(qType)) {
								int hour = (int) q / 60;
								int min = (int) q % 60;
								DateFormat df = createIsoDateFormat(TimeZone.getDefault());
								Calendar cal = new GregorianCalendar(new SimpleTimeZone(0, ""));

								cal.clear();
								cal.setTime(timestamp);
								cal.setTimeZone(new SimpleTimeZone(0, ""));
								/* get a value from the calendar to normalize fields */
								cal.get(Calendar.SECOND);
								cal.setLenient(true);
								/* times past current time are assumed to refer to the next day */
								if ((cal.get(Calendar.HOUR_OF_DAY) > hour)
										|| ((cal.get(Calendar.HOUR_OF_DAY) == hour) && ((cal.get(Calendar.MINUTE) > min)
												|| ((cal.get(Calendar.MINUTE) == min) && (cal.get(Calendar.SECOND) > 0)))))
									cal.roll(Calendar.DATE, 1);
								cal.set(Calendar.HOUR_OF_DAY, hour);
								cal.set(Calendar.MINUTE, min);
								cal.set(Calendar.SECOND, 0);
								cal.set(Calendar.MILLISECOND, 0);
								qList.set(i, df.format(cal.getTime()));
							}
							else
								qList.set(i, (q == (int) q) ? Integer.toString((int) q) : Float.toString(q));
						}
				if (!qList.isEmpty()) {
					builder.append(String.format(" q_%s=\"", mapping.qType));
					for (int i = 0; i < qList.size(); i++) {
						builder.append(qList.get(i));
						if (i < (qList.size() - 1))
							builder.append(", ");
					}
					builder.append("\"");
				}
			}

			builder.append(">\n");
			/* SI inferred from event */
			if ((mapping.siClass != null) && (mapping.si != null))
				for (String siType : mapping.si)
					appendSi(builder, mapping.siClass, siType, null, null);

			/* explicit SI */
			for (SupplementaryInfo si : event.getSupplementaryInfo()) {
				TraffSiMapping siMapping = SI_MAPPINGS.get(si.code);
				if ((siMapping == null) || ((siMapping.traffClass != null) && (siMapping.traffEvent != null)))
					/* skip items without mapping; TMC SIs translating to TraFF events will be handled later */
					continue;
				if ((siMapping.siClass != null) && (siMapping.siType != null)) {
					appendSi(builder, siMapping.siClass, siMapping.siType, siMapping.qType, siMapping.quantifier);
				}
			}
			builder.append("      </event>\n");
		}

		/* TMC SIs which translate to TraFF events */
		for (SupplementaryInfo si : event.getSupplementaryInfo()) {
			TraffSiMapping siMapping = SI_MAPPINGS.get(si.code);
			if ((siMapping == null) || (siMapping.traffClass == null) || (siMapping.traffEvent == null))
				continue;
			builder.append(String.format("      <event class=\"%s\" type=\"%s\"", siMapping.traffClass, siMapping.traffEvent));
			if (ib.speed >= 0)
				builder.append(String.format(" speed=\"%d\"", ib.speed));
			builder.append(">\n");
			if ((siMapping.siClass != null) && (siMapping.siType != null)) {
				appendSi(builder, siMapping.siClass, siMapping.siType, siMapping.qType, siMapping.quantifier);
			}
		}
		return true;
	}

	/**
	 * @brief Appends a location to the TraFF {@link StringBuilder} for a message.
	 * 
	 * If the location is null, nothing is appended.
	 * 
	 * @param builder The string builder
	 * @param location The location to append
	 * @param tag The tag to use for the location
	 */
	private static void appendLocation(StringBuilder builder, TMCPoint location, String tag) {
		if (location != null) {
			builder.append("      <").append(tag);
			if ((location.name1 != null) && (location.name1.name != null))
				builder.append(" junction_name=\"").append(location.name1.name).append("\"");
			if (location.junctionNumber != null)
				builder.append(" junction_ref=\"").append(location.junctionNumber).append("\"");
			builder.append(">");
			builder.append(String.format(Locale.US, "%+f %+f", location.yCoord, location.xCoord));
			builder.append("</").append(tag).append(">\n");
		}
	}

	/**
	 * @brief Appends a supplementary information item to the TraFF {@link StringBuilder} for a message.
	 * 
	 * The {@code builder} must have an open {@code <event>} element when calling this method, else the generated XML
	 * will violate the TraFF specification and parsers may not process the supplementary information element
	 * correctly.
	 * 
	 * If a quantifier is supplied, the respective attributes will be added to the element. For this, both
	 * {@code qType} and {@code quantifier} must be non-null.
	 * 
	 * @param builder The string builder
	 * @param siClass The supplementary information class
	 * @param siType The supplementary information type
	 * @param qType A quantifier type, can be null
	 * @param quantifier A quantifier, can be null
	 */
	private static void appendSi(StringBuilder builder, String siClass, String siType, String qType, String quantifier) {
		builder.append(String.format("        <supplementary_info class=\"%s\" type=\"%s\"", siClass, siType));
		if (qType != null && quantifier != null)
			builder.append(String.format(" q_%s=\"%s\"", qType, quantifier));
		builder.append("/>\n");
	}

	/**
	 * @brief Appends a time attribute to an element.
	 * 
	 * The {@code builder} must be in the opening tag of an element when calling this method, else the generated output
	 * may not be valid XML.
	 * 
	 * If {@code time} is null, nothing is appended.
	 * 
	 * @param builder The string builder
	 * @param attr The attribute name
	 * @param time The timestamp to insert
	 */
	private static void appendTime(StringBuilder builder, String attr, Date time) {
		if (time == null)
			return;
		String formatStr = String.format(" %s=\"%%s\"", attr);
		DateFormat df = createIsoDateFormat(TimeZone.getDefault());

		builder.append(String.format(formatStr, df.format(time)));
	}

	/**
	 * @brief Creates a date format which outputs timestamps in ISO format
	 * 
	 * @param tz The time zone
	 * 
	 * @return The date format
	 */
	private static DateFormat createIsoDateFormat(TimeZone tz) {
		DateFormat df;
		if (android.os.Build.VERSION.SDK_INT >= 24)
			df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX", Locale.ROOT);
		else
			df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.ROOT);
		df.setTimeZone(tz);
		return df;
	}

	/**
	 * @brief Retrieves and decodes a quantifier from an event.
	 * 
	 * This converts from TMC-specific indices into the default units for the corresponding TraFF type. The following
	 * rules apply:
	 * 
	 * Integers and percentages are returned in literal form.
	 * 
	 * Durations are given in minutes (non-default unit).
	 * 
	 * Speed is given in km/h.
	 * 
	 * Time is given in minutes since midnight (UTC).
	 * 
	 * Dimensions are given in meters.
	 * 
	 * Frequencies are given in MHz.
	 * 
	 * Temperatures are given in degrees Celsius.
	 * 
	 * Weight is given in metric tons.
	 * 
	 * @param event The event
	 * 
	 * @return The quantifier, or -1 if the event does not specify a valid quantifier
	 */
	private static float getQuantifierFromEvent(AlertC.Event event) {
		if (event.quantifier == -1)
			return -1;

		float q = event.quantifier;
		// q == 0 represents the highest possible value, i.e. 2^5 (for types 0-5) or 2^8 (for types 6-12)
		if (q == 0) {
			q = event.tmcEvent.quantifierType <= 5 ? 32 : 256;
		}
		switch (event.tmcEvent.quantifierType) {
		case 0:
			if (q > 28)
				q = (q - 29) * 2 + 30;
			break;

		case 1:
			if (q > 14)
				q = (q - 12) * 50;
			else if (q > 4)
				q = (q - 4) * 10;
			break;

		case 2:
			/* visibility */
			q *= 10;
			break;

		case 3:
			/* probability */
			q = q - 1 * 5;
			break;

		case 4:
			/* speed limit */
			q *= 5;
			break;

		case 5:
			/* duration */
			if (q <= 10)
				q *= 5;
			else if (q <=22)
				q = (q - 10) * 60;
			else
				q = (q - 20) * 360;
			break;

		case 6:
			/* temperature */
			q -= 51;
			break;

		case 7:
			/* time */
			q *= 10;
			break;

		case 8:
			/* weight */
		case 9:
			/* dimension */
			q = (float) (q <= 100 ? q / 10. : .5 * (q - 80));
			break;

		case 10:
			/* precipitation */
			break;

		case 11:
			/* frequency in MHz */
			q = 87500 + q * 100;
			break;

		case 12:
			/* frequency in kHz */
			if (q < 16)
				q = (float) ((q * 9 + 144) / 1000.);
			else
				q = (float) ((q * 9 + 387) / 1000.);
			break;

		default:
			return -1;
		}

		return q;
	}

	/**
	 * @brief Parses a string into a {@link TraffRamps} instance.
	 * 
	 * @param ramps The string to parse
	 * 
	 * @return The traffic ramps instance, or null if {@code ramps} does not correspond to a valid instance
	 */
	private static TraffRamps getRampsFromString(String ramps) {
		if ("ALL_RAMPS".equalsIgnoreCase(ramps))
			return TraffRamps.TRAFF_RAMPS_GENERIC;
		if ("ENTRY_RAMP".equalsIgnoreCase(ramps))
			return TraffRamps.TRAFF_RAMPS_ENTRY;
		if ("EXIT_RAMP".equalsIgnoreCase(ramps))
			return TraffRamps.TRAFF_RAMPS_EXIT;
		if ("NONE".equalsIgnoreCase(ramps))
			return TraffRamps.TRAFF_RAMPS_NONE;
		return null;
	}

	/**
	 * @brief Converts a {@link TraffRamps} instance to a string.
	 * 
	 * @param ramps
	 * 
	 * @return The corresponding string, {@code "NONE"} for unknown or null values
	 */
	private static String getStringFromRamps(TraffRamps ramps) {
		if (ramps == TraffRamps.TRAFF_RAMPS_ENTRY)
			return "ENTRY_RAMP";
		if (ramps == TraffRamps.TRAFF_RAMPS_EXIT)
			return "EXIT_RAMP";
		if ((ramps == TraffRamps.TRAFF_RAMPS_GENERIC) || (ramps == TraffRamps.TRAFF_RAMPS_ENTRY_EXIT))
			return "ALL_RAMPS";
		return "NONE";
	}

	/**
	 * @brief Merges two {@code TraffRamps} instances.
	 * 
	 * @param r1
	 * @param r2
	 * 
	 * @return Whichever value of {@code r1} and {@code r2} is more specific. If both arguments hold two different
	 * values of {@code TRAFF_RAMPS_ENTRY, TRAFF_RAMPS_EXIT, TRAFF_RAMPS_ENTRY_EXIT}, the result is
	 * {@code TRAFF_RAMPS_ENTRY_EXIT}. null is returned iff both values are null, which stands for an unspecified
	 * value.
	 */
	private static TraffRamps mergeRamps(TraffRamps r1, TraffRamps r2) {
		if (r1 == null)
			return r2;
		if (r2 == null)
			return r1;
		if (r1 == TraffRamps.TRAFF_RAMPS_NONE)
			return r2;
		if (r2 == TraffRamps.TRAFF_RAMPS_NONE)
			return r1;
		if (r1 == TraffRamps.TRAFF_RAMPS_GENERIC)
			return r2;
		if (r2 == TraffRamps.TRAFF_RAMPS_GENERIC)
			return r1;
		if (r1 != r2)
			return TraffRamps.TRAFF_RAMPS_ENTRY_EXIT;
		else
			return r1;
	}

	/**
	 * A mapping which specifies how a TMC event maps to one or multiple TraFF events.
	 * 
	 * If a single TMC event is represented by multiple TraFF events, one {@code TraffEventMapping} instance exists for each
	 * TraFF event.
	 */
	private static class TraffEventMapping {
		static Pattern pipePattern = Pattern.compile("\\|");

		/** The TMC event code */
		public final int tmcId;

		/** The mnemonic for the TraFF event class */
		public final String traffClass;

		/** The TraFF event mnemonic */
		public final String traffEvent;

		/** The speed in km/h, null if not specified */
		public final String speed;

		/** The length of the route affected, in m, -1 if not specified */
		public final int length;

		/** The quantifier type (without the q_ prefix), null for no quantifier */
		public final String qType;

		/** The quantifier, null for no quantifier.
		 *  Values may contain the $Q placeholder, to be replaced with an explicit quantifier. */
		public final String quantifier;

		/** Whether the event is a forecast */
		public final boolean isForecast;

		/** Supplementary information class, or null if none is specified */
		public final String siClass;

		/** Supplementary information, or null if none is specified */
		public final List<String> si;

		/** Whether the location affects ramps */
		public final TraffRamps ramps;

		TraffEventMapping(String line) throws IllegalArgumentException {
			String[] comp = colonPattern.split(line);
			if (comp.length < 3)
				throw new IllegalArgumentException("too few data fields in line");
			this.tmcId = Integer.parseInt(comp[0]);
			this.traffClass = comp[1].trim();
			this.traffEvent = comp[2].trim();
			if ((comp.length < 4) || (comp[3].isEmpty()))
				this.speed = null;
			else
				this.speed = comp[3].trim();
			if ((comp.length < 5) || (comp[4].isEmpty()))
				this.length = -1;
			else
				this.length = Integer.parseInt(comp[4]);
			if ((comp.length < 6) || (comp[5].isEmpty()))
				this.qType = null;
			else
				this.qType = comp[5];
			if ((comp.length < 7) || (comp[6].isEmpty()))
				this.quantifier = null;
			else
				this.quantifier = comp[6];
			if ((comp.length < 8) || (comp[7].isEmpty()))
				this.isForecast = false;
			else
				this.isForecast = Boolean.parseBoolean(comp[7]);

			/*
			 * Note: while we do allow multiple SIs per TraFF event generated from a single TMC event, all these SIs
			 * must be of the same class. There is only one TMC event (1062) which results in a TraFF event with
			 * multiple SIs, and these happen to be all of the same class.
			 */
			if ((comp.length < 9) || (comp[8].isEmpty()))
				this.siClass = null;
			else
				this.siClass = comp[8];
			if ((comp.length < 10) || (comp[9].isEmpty()))
				this.si = null;
			else {
				String[] siArray = pipePattern.split(comp[9]);
				this.si = Arrays.asList(siArray);
			}
			if ((comp.length < 11) || (comp[10].isEmpty()))
				ramps = null;
			else
				ramps = getRampsFromString(comp[10]);
		}
	}

	/**
	 * A mapping which specifies how a TMC supplementary information item maps to TraFF SI items and events.
	 */
	private static class TraffSiMapping {
		/** The TMC supplementary information code */
		public final int tmcId;

		/** Supplementary information class, or null if none is specified */
		public final String siClass;

		/** Supplementary information, or null if none is specified */
		public final String siType;

		/** The mnemonic for the TraFF event class */
		public final String traffClass;

		/** The TraFF event mnemonic */
		public final String traffEvent;

		/** The quantifier type (without the q_ prefix), null for no quantifier */
		public final String qType;

		/** The quantifier, null for no quantifier */
		public final String quantifier;

		TraffSiMapping(String line) throws IllegalArgumentException {
			String[] comp = colonPattern.split(line);
			if (comp.length < 3)
				throw new IllegalArgumentException("too few data fields in line");
			this.tmcId = Integer.parseInt(comp[0]);
			this.siClass = comp[1].trim();
			this.siType = comp[2].trim();
			if ((comp.length < 4) || (comp[3].isEmpty()))
				this.qType = null;
			else
				this.qType = comp[3];
			if ((comp.length < 5) || (comp[4].isEmpty()))
				this.quantifier = null;
			else
				this.quantifier = comp[4];
			if ((comp.length < 6) || (comp[5].isEmpty()))
				this.traffClass = null;
			else
				this.traffClass = comp[5].trim();
			if ((comp.length < 7) || (comp[6].isEmpty()))
				this.traffEvent = null;
			else
				this.traffEvent = comp[6].trim();
		}
	}

	private static enum TraffRamps {
		/** Ramps (no specific references to entry or exit ramps) */
		TRAFF_RAMPS_GENERIC,
		/** Entry ramps only */
		TRAFF_RAMPS_ENTRY,
		/** Exit ramps only */
		TRAFF_RAMPS_EXIT,
		/** Entry and exit ramps (both are implied in different parts) */
		TRAFF_RAMPS_ENTRY_EXIT,
		/** No ramps (main carriageway) */
		TRAFF_RAMPS_NONE
	}
}
