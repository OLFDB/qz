/*
 * Copyright © 2017–2018 Michael von Glasow.
 * 
 * This file is part of Qz.
 *
 * Qz is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Qz is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Qz.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.vonglasow.michael.qz.core;

import eu.jacquet80.rds.app.oda.AlertC.MessageVisitor;

public interface WrappedMessageVisitor extends MessageVisitor {
	public void visit(MessageWrapper wrapper);
}
