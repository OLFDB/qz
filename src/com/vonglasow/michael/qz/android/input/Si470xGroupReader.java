/*
 Copyright (c) 2017 Michael von Glasow
 Portions Copyright (c) 2009-2012 Christophe Jacquet (RDS Surveyor)
 Portions Copyright (c) 2009 Tobias Lorenz (original Linux driver)

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Lesser Public License for more details.

 You should have received a copy of the GNU Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */


package com.vonglasow.michael.qz.android.input;

import java.io.Closeable;
import java.io.IOException;

import com.vonglasow.michael.andhid.HidDevice;
import com.vonglasow.michael.qz.android.util.Const;

import android.content.Context;
import android.content.SharedPreferences;
import android.hardware.usb.UsbDevice;
import android.preference.PreferenceManager;
import android.util.Log;
import eu.jacquet80.rds.input.TunerGroupReader;
import eu.jacquet80.rds.input.UnavailableInputMethod;
import eu.jacquet80.rds.input.group.FrequencyChangeEvent;
import eu.jacquet80.rds.input.group.GroupEvent;
import eu.jacquet80.rds.input.group.GroupReaderEvent;
import eu.jacquet80.rds.log.RealTime;

/**
 * Android interface for Si470x-based USB FM tuners
 */
public class Si470xGroupReader extends TunerGroupReader implements Closeable {
	private static final String TAG = "Si470xGroupReader";

	private Context context;
	private UsbDevice usbDevice;
	private Si470xDevice si470xDevice;
	private boolean newGroups;
	private TunerData tunerData = new TunerData();
	private boolean closed = false;

	public Si470xGroupReader(Context context, UsbDevice device) throws UnavailableInputMethod {
		synchronized(this) {
			this.context = context;
			this.usbDevice = device;
			if(open()) {
				System.out.println(
						getDeviceName() + ": device found, using it!");
				setFrequency(87500);
				tunerData.frequency = 87500;
			} else {
				throw new UnavailableInputMethod("no device found");
			}
		}
	}

	@Override
	public synchronized void close() {
		this.closed = true;
		this.si470xDevice.close();
	}

	@Override
	public String getDeviceName() {
		return "Si470x";
	}

	@Override
	public int getFrequency() {
		return tunerData.frequency;
	}

	@Override
	public synchronized GroupReaderEvent getGroup() throws IOException, EndOfStream {
		if (closed)
			throw new EndOfStream();

		int oldFreq = tunerData.frequency;

		readTuner();

		if(tunerData.frequency != oldFreq) {
			// if frequency has just been changed, must report an event
			return new FrequencyChangeEvent(new RealTime(), tunerData.frequency);
		}

		if(!tunerData.groupReady) return null;

		int[] res = new int[4];
		for(int i=0; i<4; i++) {
			if(tunerData.err[i] > 0) res[i] = -1;
			else res[i] = tunerData.block[i] & 0xFFFF;
		}

		newGroups = true;
		return new GroupEvent(new RealTime(), res, false);
	}

	@Override
	public synchronized int getSignalStrength() {
		if (closed)
			return 0;

		return tunerData.rssi;
	}

	@Override
	public boolean isAudioCapable() {
		// No audio support
		return false;
	}

	@Override
	public boolean isPlayingAudio() {
		// No audio support
		return false;
	}

	@Override
	public boolean isStereo() {
		// No audio support
		return false;
	}

	@Override
	public synchronized boolean isSynchronized() {
		if (closed)
			return false;
		return tunerData.rdsSynchronized;
	}

	@Override
	public int mute() {
		// No audio support
		return 0;
	}

	@Override
	public synchronized boolean newGroups() {
		if (closed)
			return false;

		boolean ng = newGroups;
		newGroups = false;
		return ng;
	}

	@Override
	public synchronized boolean seek(boolean up) {
		if (closed)
			return false;

		si470xDevice.startSeek(true, up);
		return false;
	}

	@Override
	public synchronized int setFrequency(int frequency) {
		if (closed)
			return -1;

		int res = si470xDevice.setFreq(frequency);
		if (res >= 0)
			return si470xDevice.getFreq();
		return res;
	}

	@Override
	public synchronized void tune(boolean up) {
		if (closed)
			return;

		int freq = tunerData.frequency + (up ? 100 : -100);

		if(freq > 108000) freq = 87500;
		if(freq < 87500) freq = 108000;

		tunerData.frequency = setFrequency(freq);
	}

	@Override
	public int unmute() {
		// No audio support
		return 0;
	}

	private int readTuner() {
		if (closed)
			return 0;
		tunerData = si470xDevice.readRds(tunerData);
		return 0;
	}

	private boolean open() {
		try {
			this.si470xDevice = new Si470xDevice(context, usbDevice);
		} catch (Exception e) {
			return false;
		}

		if (si470xDevice.start(Si470xDevice.CHANNEL_SPACING_100_KHZ, Si470xDevice.BAND_87_108, Si470xDevice.DE_WORLD) != 0)
			return false;

		return true;
	}


	/**
	 * Encapsulates an Si470x USB tuner device.
	 */
	private class Si470xDevice {
		/** 50 kHz spacing (Italy and cable networks) */
		public static final byte CHANNEL_SPACING_50_KHZ = 2;
		/** 100 kHz spacing (Europe, Japan) */
		public static final byte CHANNEL_SPACING_100_KHZ = 1;
		/** 200 kHz spacing (USA, Australia) */
		public static final byte CHANNEL_SPACING_200_KHZ = 0;

		/** 87.5–108 MHz band (USA, Europe) */
		public static final byte BAND_87_108 = 0;
		/** 76–108 MHz band (Japan wide band) */
		public static final byte BAND_76_108 = 1;
		/** 76–90 MHz band (Japan) */
		public static final byte BAND_76_90 = 2;

		/** 75 µs de-emphasis (USA) */
		public static final byte DE_USA = 0;
		/** 50 µs de-emphasis (Europe, Australia, Japan) */
		public static final byte DE_WORLD = 1;

		private static final int RADIO_REGISTER_SIZE = 2;	/* 16 register bit width */
		private static final int RADIO_REGISTER_NUM = 16; /* DEVICEID   ... RDSD */
		private static final int RDS_REGISTER_NUM = 6;	/* STATUSRSSI ... RDSD */

		private static final int EP0_REPORT_SIZE = 17;

		/* Reports 1-16 give direct read/write access to the 16 Si470x registers */
		/* with the (REPORT_ID - 1) corresponding to the register address across USB */
		/* endpoint 0 using GET_REPORT and SET_REPORT */
		private static final int REGISTER_REPORT_SIZE = (RADIO_REGISTER_SIZE + 1);
		//#define REGISTER_REPORT(reg)	((reg) + 1)

		/* Report 17 gives direct read/write access to the entire Si470x register */
		/* map across endpoint 0 using GET_REPORT and SET_REPORT */
		private static final int ENTIRE_REPORT_SIZE = (RADIO_REGISTER_NUM * RADIO_REGISTER_SIZE + 1);
		private static final int ENTIRE_REPORT = 17;

		/* Report 18 is used to send the lowest 6 Si470x registers up the HID */
		/* interrupt endpoint 1 to Windows every 20 milliseconds for status */
		private static final int RDS_REPORT_SIZE = (RDS_REGISTER_NUM * RADIO_REGISTER_SIZE + 1);
		private static final int RDS_REPORT = 18;

		/* Report 19: LED state */
		private static final int LED_REPORT_SIZE = 3;
		private static final int LED_REPORT = 19;

		/* Report 19: stream */
		private static final int STREAM_REPORT_SIZE = 3;
		private static final int STREAM_REPORT = 19;

		/* Report 20: scratch */
		private static final int SCRATCH_PAGE_SIZE = 63;
		private static final int SCRATCH_REPORT_SIZE = (SCRATCH_PAGE_SIZE + 1);
		private static final int SCRATCH_REPORT = 20;

		private static final int DEVICEID = 0;	/* Device ID */
		private static final int DEVICEID_PN = 0xf000;	/* bits 15..12: Part Number */
		private static final int DEVICEID_MFGID = 0x0fff;	/* bits 11..00: Manufacturer ID */

		private static final int CHIPID = 1;	/* Chip ID. Note: this field changed with firmware r16 */
		private static final int CHIPID_REV = 0xfc00;	/* bits 15..10: Silicon revision */
		private static final int CHIPID_DEV = 0x03C0;	/* bits 09..06: Device */
		private static final int CHIPID_FIRMWARE = 0x003f;	/* bits 05..00: Firmware Version */

		private static final int POWERCFG = 2;	/* Power Configuration */
		private static final int POWERCFG_DSMUTE = 0x8000;	/* bits 15..15: Softmute Disable */
		private static final int POWERCFG_DMUTE = 0x4000;	/* bits 14..14: Mute Disable */
		private static final int POWERCFG_MONO = 0x2000;	/* bits 13..13: Mono Select */
		private static final int POWERCFG_RDSM = 0x0800;	/* bits 11..11: RDS Mode (Si4701 only) */
		private static final int POWERCFG_SKMODE = 0x0400;	/* bits 10..10: Seek Mode */
		private static final int POWERCFG_SEEKUP = 0x0200;	/* bits 09..09: Seek Direction */
		private static final int POWERCFG_SEEK = 0x0100;	/* bits 08..08: Seek */
		private static final int POWERCFG_DISABLE = 0x0040;	/* bits 06..06: Powerup Disable */
		private static final int POWERCFG_ENABLE = 0x0001;	/* bits 00..00: Powerup Enable */

		private static final int CHANNEL = 3;	/* Channel */
		private static final int CHANNEL_TUNE = 0x8000;	/* bits 15..15: Tune */
		private static final int CHANNEL_CHAN = 0x03ff;	/* bits 09..00: Channel Select */

		private static final int SYSCONFIG1 = 4;	/* System Configuration 1 */
		private static final int SYSCONFIG1_RDSIEN = 0x8000;	/* bits 15..15: RDS Interrupt Enable (Si4701 only) */
		private static final int SYSCONFIG1_STCIEN = 0x4000;	/* bits 14..14: Seek/Tune Complete Interrupt Enable */
		private static final int SYSCONFIG1_RDS = 0x1000;	/* bits 12..12: RDS Enable (Si4701 only) */
		private static final int SYSCONFIG1_DE = 0x0800;	/* bits 11..11: De-emphasis (0=75us 1=50us) */
		private static final int SYSCONFIG1_AGCD = 0x0400;	/* bits 10..10: AGC Disable */
		private static final int SYSCONFIG1_BLNDADJ = 0x00c0;	/* bits 07..06: Stereo/Mono Blend Level Adjustment */
		private static final int SYSCONFIG1_GPIO3 = 0x0030;	/* bits 05..04: General Purpose I/O 3 */
		private static final int SYSCONFIG1_GPIO2 = 0x000c;	/* bits 03..02: General Purpose I/O 2 */
		private static final int SYSCONFIG1_GPIO1 = 0x0003;	/* bits 01..00: General Purpose I/O 1 */

		private static final int SYSCONFIG2 = 5;	/* System Configuration 2 */
		private static final int SYSCONFIG2_SEEKTH = 0xff00;	/* bits 15..08: RSSI Seek Threshold */
		private static final int SYSCONFIG2_BAND = 0x00C0;	/* bits 07..06: Band Select */
		private static final int SYSCONFIG2_SPACE = 0x0030;	/* bits 05..04: Channel Spacing */
		private static final int SYSCONFIG2_VOLUME = 0x000f;	/* bits 03..00: Volume */

		private static final int SYSCONFIG3 = 6;	/* System Configuration 3 */
		private static final int SYSCONFIG3_SMUTER = 0xc000;	/* bits 15..14: Softmute Attack/Recover Rate */
		private static final int SYSCONFIG3_SMUTEA = 0x3000;	/* bits 13..12: Softmute Attenuation */
		private static final int SYSCONFIG3_SKSNR = 0x00f0;	/* bits 07..04: Seek SNR Threshold */
		private static final int SYSCONFIG3_SKCNT = 0x000f;	/* bits 03..00: Seek FM Impulse Detection Threshold */

		private static final int TEST1 = 7;	/* Test 1 */
		private static final int TEST1_AHIZEN = 0x4000;	/* bits 14..14: Audio High-Z Enable */

		private static final int TEST2 = 8;	/* Test 2 */
		/* TEST2 only contains reserved bits */

		private static final int BOOTCONFIG = 9;	/* Boot Configuration */
		/* BOOTCONFIG only contains reserved bits */

		private static final int STATUSRSSI = 10;	/* Status RSSI */
		private static final int STATUSRSSI_RDSR = 0x8000;	/* bits 15..15: RDS Ready (Si4701 only) */
		private static final int STATUSRSSI_STC = 0x4000;	/* bits 14..14: Seek/Tune Complete */
		private static final int STATUSRSSI_SF = 0x2000;	/* bits 13..13: Seek Fail/Band Limit */
		private static final int STATUSRSSI_AFCRL = 0x1000;	/* bits 12..12: AFC Rail */
		private static final int STATUSRSSI_RDSE = 0x0E00;  /* bits 11..09: RDS Errors (firmware rev <= 12 or "standard mode") */
		/* The next two fields are supported only in "verbose mode", available with firmware revs >12 */
		private static final int STATUSRSSI_RDSS = 0x0800;	/* bits 11..11: RDS Synchronized (Si4701 only) */
		private static final int STATUSRSSI_BLERA = 0x0600;	/* bits 10..09: RDS Block A Errors (Si4701 only) */
		private static final int STATUSRSSI_ST = 0x0100;	/* bits 08..08: Stereo Indicator */
		private static final int STATUSRSSI_RSSI = 0x00ff;	/* bits 07..00: RSSI (Received Signal Strength Indicator) */

		private static final int READCHAN = 11;	/* Read Channel */
		private static final int READCHAN_BLERB = 0xc000;	/* bits 15..14: RDS Block D Errors (Si4701 only) */
		private static final int READCHAN_BLERC = 0x3000;	/* bits 13..12: RDS Block C Errors (Si4701 only) */
		private static final int READCHAN_BLERD = 0x0c00;	/* bits 11..10: RDS Block B Errors (Si4701 only) */
		private static final int READCHAN_READCHAN = 0x03ff;	/* bits 09..00: Read Channel */

		private static final int RDSA = 12;	/* RDSA */
		private static final int RDSA_RDSA = 0xffff;	/* bits 15..00: RDS Block A Data (Si4701 only) */

		private static final int RDSB = 13;	/* RDSB */
		private static final int RDSB_RDSB = 0xffff;	/* bits 15..00: RDS Block B Data (Si4701 only) */

		private static final int RDSC = 14;	/* RDSC */
		private static final int RDSC_RDSC = 0xffff;	/* bits 15..00: RDS Block C Data (Si4701 only) */

		private static final int RDSD = 15;	/* RDSD */
		private static final int RDSD_RDSD = 0xffff;	/* bits 15..00: RDS Block D Data (Si4701 only) */

		private static final int MIN_CHIP_FIRMWARE_REV_FOR_VERBOSE_MODE = 13;

		/* Silabs internal registers (0..15) */
		int[] registers = new int[RADIO_REGISTER_NUM];

		/* scratch page */
		byte softwareVersion;
		byte hardwareVersion;

		/* radio configuration (in kHz) */
		int channelSpacing;
		int bandBottom;
		int bandTop;

		/* description */
		String productName;
		String vendorName;
		short pid, vid;

		int chipFirmwareRev;

		private Context context;
		private UsbDevice usbDevice;
		/* The HID device (equivalent to si470x_dev.devh) */
		private HidDevice hidDevice;
		/* Used by readRds() */
		private int cleared = 0;
		private int oldRds[] = {0, 0, 0, 0};

		private boolean closed = false;
		private boolean debugVerbose;

		/**
		 * @brief Opens an Si470x-based USB device
		 * 
		 * The device is typically obtained in one of the following ways:
		 * <ul>
		 * <li>By receiving a {@code USB_DEVICE_ATTACHED} intent, indicating that a USB device has
		 * been attached and the user wishes your application to handle it</li>
		 * <li>By receiving a {@code ACTION_USB_PERMISSION} intent, indicating that the user has
		 * granted you the permission to interact with the device which you previously requested</li>
		 * <li>By calling {@link android.hardware.usb.UsbManager#getDeviceList()} to enumerate all
		 * USB devices currently present (note you may still need to request access permissions for
		 * a device obtained in this manner)</li>
		 * </ul>
		 * 
		 * Prior to calling this constructor, you must ensure that you have permission to open the
		 * device, else an exception will be thrown.
		 * 
		 * @param device The device
		 * @throws IOException 
		 * 
		 * @throws IllegalArgumentException if the device supplied is not a Si470x device
		 * @throws IOException if opening the device fails
		 */
		// roughly equivalent to si470x_open
		public Si470xDevice(Context context, UsbDevice device) throws IllegalArgumentException, IOException {
			this.context = context;
			this.usbDevice = device;
			this.hidDevice = new HidDevice(context, device);
			// TODO
			/* Native code to identify known supported devices:
			(...)
			    si470x_dongle_t *sidev = find_known_device(cur_dev->vendor_id, cur_dev->product_id);
			    if(sidev) {
					// we have a known device
				}
			 */
			SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
			debugVerbose = preferences.getBoolean(Const.PREF_DEBUG_VERBOSE, false);
		}

		/**
		 * @brief Closes the Si470x device.
		 * 
		 * Closing a device releases all associated resources. After that the device cannot be used
		 * again, unless a new instance is obtained.
		 */
		public void close() {
			this.closed = true;
			this.hidDevice.close();
		}

		/**
		 * @brief Gets the current tuner frequency
		 * 
		 * @return The current tuner frequency in kHz
		 */
		// TODO throw an exception if something goes wrong
		public int getFreq() {
			if (closed)
				throw new IllegalStateException("this method cannot be called on a closed device");
			getRegister(READCHAN);
			int chan = registers[READCHAN] & READCHAN_READCHAN;
			return getFrequencyFromChannel(chan);
		}

		/**
		 * @brief Reads RDS data and other real-time RX information
		 * 
		 * @return A {@link TunerData} instance with the requested information
		 */
		public TunerData readRds(TunerData inTunerData) {
			if (closed)
				throw new IllegalStateException("this method cannot be called on a closed device");

			TunerData tunerData = (inTunerData == null ? new TunerData() : inTunerData);
			int retval;

			/*
			 * If we exit with an error, groupReady is false. Setting it to false here allows us to
			 * return tunerData at any point if an error is encountered. Prior to successful exit,
			 * we set it to true.
			 */
			tunerData.groupReady = false;

			retval = getRdsRegisters();

			if (retval < 0)
				return tunerData; // retval

			/* RDS data */
			tunerData.block[0] = (short) registers[RDSA];
			tunerData.block[1] = (short) registers[RDSB];
			tunerData.block[2] = (short) registers[RDSC];
			tunerData.block[3] = (short) registers[RDSD];

			/* Error info */
			if (chipFirmwareRev >= MIN_CHIP_FIRMWARE_REV_FOR_VERBOSE_MODE) {
				/* Verbose mode */
				tunerData.err[0] = (short) ((registers[STATUSRSSI] & STATUSRSSI_BLERA) >> 9);
				tunerData.err[1] = (short) ((registers[READCHAN] & READCHAN_BLERB) >> 14);
				tunerData.err[2] = (short) ((registers[READCHAN] & READCHAN_BLERC) >> 12);
				tunerData.err[3] = (short) ((registers[READCHAN] & READCHAN_BLERD) >> 10);
			} else {
				/* Standard mode */
				/* If one or more errors, mark the BLER of any block as "errorful" (3) */
				int bler = ((registers[STATUSRSSI] & STATUSRSSI_RDSE) >> 9) > 0 ? 3 : 0;

				tunerData.err[0] =
						tunerData.err[1] =
						tunerData.err[2] =
						tunerData.err[3] = (short) bler;
			}

			tunerData.rdsSynchronized = (registers[STATUSRSSI] & STATUSRSSI_RDSS) != 0;
			tunerData.stereo = (registers[STATUSRSSI] & STATUSRSSI_ST) != 0;
			tunerData.rssi = (registers[STATUSRSSI] & STATUSRSSI_RSSI) * 873;
			tunerData.frequency = getFrequencyFromChannel(registers[READCHAN] & READCHAN_READCHAN);

			/* If STC (seek/tune complete) or SF (seek fail) then stop tuning or seeking */
			if ((registers[STATUSRSSI] & (STATUSRSSI_STC | STATUSRSSI_SF)) != 0) {
				registers[POWERCFG] &= ~POWERCFG_SEEK;
				setRegister(POWERCFG);
				registers[CHANNEL] &= ~CHANNEL_TUNE;
				setRegister(CHANNEL);
			}

			if ((registers[STATUSRSSI] & STATUSRSSI_RDSR) == 0) {
				cleared = 1;
				return tunerData; //-3; // TODO FIXME Not synced
			}

			cleared = 0;

			for (int i=0; i<4; i++) {
				if(oldRds[i] != tunerData.block[i]) {
					cleared = 1;
					oldRds[i] = tunerData.block[i];
				}
			}

			tunerData.groupReady = (cleared != 0);

			return tunerData; // -5 on error (cleared == 0)
		}

		/**
		 * @brief Sets a new tuner frequency
		 * 
		 * @param freq The new tuner frequency in kHz
		 */
		// FIXME Do not set the TUNE or SEEK bits until the Si470x clears the STC bit.
		public int setFreq(int freq) {
			if (closed)
				throw new IllegalStateException("this method cannot be called on a closed device");

			int chan = getChannelFromFrequency(freq);
			registers[CHANNEL] &= ~CHANNEL_CHAN;
			registers[CHANNEL] |= CHANNEL_TUNE | chan;
			int retval = setRegister(CHANNEL);
			if (retval < 0)
				return retval;

			do {
				retval = getRegister(STATUSRSSI);
				if (retval < 0) {
					registers[CHANNEL] &= ~CHANNEL_TUNE;
					retval = setRegister(CHANNEL);
					return retval;
				}
			} while ((registers[STATUSRSSI] & STATUSRSSI_STC) == 0);

			return retval;
		}

		/**
		 * @brief Configures the device and starts operating
		 * 
		 * @param space Channel spacing, must be one of {@link #CHANNEL_SPACING_50_KHZ},
		 * {@link #CHANNEL_SPACING_100_KHZ} or {@link #CHANNEL_SPACING_200_KHZ}
		 * @param band FM frequency band to operate in, must be one of {@link #BAND_87_108},
		 * {@link #BAND_76_108} or {@link #BAND_76_90}. The OIRT band (65.8–74 MHz) is unsupported.
		 * 
		 * @return 0 on success, -1 if reading registers failed, -3 if SYSCONFIG1 register write
		 * failed, -4 if SYSCONFIG2 register write failed, -5 if SYSCONFIG3 register write failed
		 */
		public int start(byte space, byte band, byte de) {
			if (closed)
				throw new IllegalStateException("this method cannot be called on a closed device");

			/* Number of tries remaining */
			int tries = 2;

			do {
				/* powerup */
				registers[POWERCFG] = POWERCFG_DMUTE | POWERCFG_ENABLE;
				if(setRegister(POWERCFG) < 0)
					return -8;

				try {
					Thread.sleep(220);
				} catch (InterruptedException e) {
					return -7;
				}

				if (getAllRegisters() < 0)
					return -1;

				/* Store chip/dongle version info */
				chipFirmwareRev = registers[CHIPID] & CHIPID_FIRMWARE;

				/* powercfg */
				registers[POWERCFG] = POWERCFG_DMUTE | POWERCFG_ENABLE | POWERCFG_RDSM;
				if(setRegister(POWERCFG) < 0)
					return -2;

				/* channel */
				registers[CHANNEL] = 0;
				if (setRegister(CHANNEL) < 0)
					return -6;

				/* sysconfig 1 */
				registers[SYSCONFIG1] = SYSCONFIG1_RDS;		/* RDS on */
				if(de != 0)
					registers[SYSCONFIG1] |= SYSCONFIG1_DE;
				if(setRegister(SYSCONFIG1) < 0)
					return -3;

				/* sysconfig 2 */
				registers[SYSCONFIG2] =
						(16  << 8) |				/* SEEKTH */
						((band  << 6) & SYSCONFIG2_BAND)  |	/* BAND */
						((space << 4) & SYSCONFIG2_SPACE) |	/* SPACE */
						15;					/* VOLUME (max) */
				if(setRegister(SYSCONFIG2) < 0)
					return -4;

				/* sysconfig 3 */
				registers[SYSCONFIG3] = 
						6 << 4 |   /* bits 07..04: Seek SNR Threshold */
						10;        /* bits 03..00: Seek FM Impulse Detection Threshold */
				if(setRegister(SYSCONFIG3) < 0)
					return -5;

				tries--;

				if ((registers[READCHAN] & 0xFFFF) == 0xFFFF) {
					Log.w(TAG, "Cannot read registers, resetting and retrying");
					registers[POWERCFG] = POWERCFG_ENABLE | POWERCFG_DISABLE;
					if(setRegister(POWERCFG) < 0)
						return -9;
					registers[POWERCFG] = POWERCFG_DISABLE;
					if(setRegister(POWERCFG) < 0)
						return -10;
				} else {
					Log.i(TAG, "Si470x startup successful");
					tries = 0;
					break;
				}
			} while (tries > 0);

			/* frequency settings */
			switch(space) {
			case CHANNEL_SPACING_50_KHZ:
				channelSpacing = 50;
				break;
			case CHANNEL_SPACING_200_KHZ:
				channelSpacing = 200;
				break;
			case CHANNEL_SPACING_100_KHZ:
			default:
				channelSpacing = 100;
				break;
			}

			switch(band) {
			case BAND_76_108:
				bandBottom = 76000;
				bandTop = 108000;
				break;
			case BAND_76_90:
				bandBottom = 76000;
				bandTop = 90000;
				break;
			case BAND_87_108:
			default:
				bandBottom = 87500;
				bandTop = 108000;
			}

			getScratchPageVersions();

			if (debugVerbose) {
				Log.d(TAG, String.format("DEVICEID [0]:    0x%04X\n", registers[DEVICEID]));

				// Part number
				int pn = (registers[DEVICEID] & DEVICEID_PN)>>12;
					switch(pn) {
					case 0x1: Log.d(TAG, String.format("Part number:     0x%01X (Si4700/01/02/03)\n", pn)); break;
					default: Log.d(TAG, String.format("Part number:     0x%01X (Unknown)\n", pn)); break;
					}

					// Manufacturer
					int mfg = registers[DEVICEID] & DEVICEID_MFGID;
					switch(mfg) {
					case 0x242: Log.d(TAG, String.format("Manufacturer ID: 0x%03X (Silicon Labs)\n", mfg)); break;
					default: Log.d(TAG, String.format("Manufacturer ID: 0x%03X (Unknown)\n", mfg)); break;
					}

					Log.d(TAG, String.format("CHIPID   [1]:    0x%04X\n", registers[CHIPID]));

					// Chip revisions
					Log.d(TAG, String.format("Chip revision:   %c\n", 64 + ((registers[CHIPID] & CHIPID_REV)>>10)));

					// Device type
					int dev_type = (registers[CHIPID] & CHIPID_DEV)>>6;
				switch(dev_type) {
				case 0x0: Log.d(TAG, String.format("Device type:     0x%01X (Si4700)\n", dev_type)); break;
				case 0x1: Log.d(TAG, String.format("Device type:     0x%01X (Si4702)\n", dev_type)); break;
				case 0x8: Log.d(TAG, String.format("Device type:     0x%01X (Si4701)\n", dev_type)); break;
				case 0x9: Log.d(TAG, String.format("Device type:     0x%01X (Si4703)\n", dev_type)); break;
				}
				Log.d(TAG, String.format("Si470x firmware: %d\n", registers[CHIPID] & CHIPID_FIRMWARE));

				Log.d(TAG, String.format("FM band: %.2f to %.2f, %d kHz steps",
						(float)bandBottom / 1000.0,
						(float)bandTop / 1000.0,
						channelSpacing));
			}

			return 0;
		}

		/*
		 * start seek
		 */
		// TODO exceptions rather than return values
		// FIXME Do not set the TUNE or SEEK bits until the Si470x clears the STC bit.
		public int startSeek(boolean wrap_around, boolean seek_upward) {
			if (closed)
				throw new IllegalStateException("this method cannot be called on a closed device");

			registers[POWERCFG] |= POWERCFG_SEEK;

			if (wrap_around)
				registers[POWERCFG] &= ~POWERCFG_SKMODE;
			else
				registers[POWERCFG] |= POWERCFG_SKMODE;

			if (seek_upward)
				registers[POWERCFG] |= POWERCFG_SEEKUP;
			else
				registers[POWERCFG] &= ~POWERCFG_SEEKUP;

			return setRegister(POWERCFG);
		}


		/**
		 * @brief Reads data from all registers
		 * 
		 * The data is stored in {@code registers}.
		 * 
		 * @return 0 on success, -1 on failure
		 */
		// TODO get rid of workaround, revisit exceptions
		// 2012-06-18 - for now use this dummy version, as I cannot
		// figure out why it doesn't work on Windows. Maybe the
		// report size is not correct?
		private int getAllRegisters() {
			if (closed)
				throw new IllegalStateException("this method cannot be called on a closed device");
			for (int i = 0; i < RADIO_REGISTER_NUM; i++)
				if (getRegister(i) != 0)
					return -22; // TODO -EINVAL
			return 0;
		}

		/**
		 * @brief Converts a frequency to a channel as used by the device
		 * 
		 * @param frequency The frequency in kHz
		 * 
		 * @return The channel, as determined by the FM band, channel spacing and frequency
		 */
		private int getChannelFromFrequency(int frequency) {
			if (closed)
				throw new IllegalStateException("this method cannot be called on a closed device");
			return (int) ( (frequency - bandBottom) / channelSpacing);
		}

		/**
		 * @brief Converts a channel used by the device for a frequency
		 * 
		 * @param channel The channel
		 * 
		 * @return The frequency in kHz, as determined by the FM band, channel spacing and channel
		 */
		private int getFrequencyFromChannel(int channel) {
			if (closed)
				throw new IllegalStateException("this method cannot be called on a closed device");
			return bandBottom + channelSpacing * channel;
		}

		/*
		 * read RDS registers
		 */
		private int getRdsRegisters() {
			byte[] data = new byte[RDS_REPORT_SIZE];
			int retval;
			int regnr;

			if (closed)
				throw new IllegalStateException("this method cannot be called on a closed device");

			data[0] = RDS_REPORT;

			retval = hidDevice.read(data, data.length);

			if (retval >= 0) {
				if (retval != 1 + RDS_REGISTER_NUM * RADIO_REGISTER_SIZE)
					return -90 - retval;

				for (regnr = 0; regnr < RDS_REGISTER_NUM; regnr++)
					registers[RADIO_REGISTER_NUM - RDS_REGISTER_NUM + regnr] =
					(data[regnr * RADIO_REGISTER_SIZE + 1] & 0xFF) << 8 | (data[regnr * RADIO_REGISTER_SIZE + 2] & 0xFF);
			}

			return (retval < 0) ? -22 : 0;
		}

		/**
		 * @brief Reads data from a register
		 * 
		 * The data is stored in {@code registers}.
		 * 
		 * @param regnr The register number to retrieve
		 * 
		 * @return 0 on success
		 */
		// TODO revisit exceptions
		private int getRegister(int regnr) {
			if (closed)
				throw new IllegalStateException("this method cannot be called on a closed device");

			byte[] data = new byte[EP0_REPORT_SIZE];

			/* report number is register number plus 1 */
			if (getReport(regnr + 1, data, data.length) < 0)
				throw new IllegalArgumentException();
			registers[regnr] = (data[1] & 0xFF) << 8 | (data[2] & 0xFF);
			return 0;
		}

		/*
		 * receive a HID report
		 */
		private int getReport(int reportId, byte[] data, int length) {
			if (closed)
				throw new IllegalStateException("this method cannot be called on a closed device");

			return hidDevice.getFeatureReport(reportId, data, length);
			/* TODO do we need the extra validation stuff?
			if (retval < 0)
				dev_warn(
					"si470x_get_report: hid_get_feature_report returned %d\n",
					retval);
			return retval;
			 */
		}

		/*
		 * gets the scratch page and version infos
		 */
		private int getScratchPageVersions() {
			if (closed)
				throw new IllegalStateException("this method cannot be called on a closed device");

			byte[] data = new byte[SCRATCH_REPORT_SIZE];

			if (getReport(SCRATCH_REPORT, data, data.length) < 0)
				throw new IllegalArgumentException();

			softwareVersion = data[1];
			hardwareVersion = data[2];
			if (debugVerbose)
				Log.d(TAG, String.format("Versions: sw=%d, hw=%d\n", data[1], data[2]));

			return 0;
		}

		/**
		 * @brief Writes data to a register
		 * 
		 * The data is taken from {@code registers}.
		 * 
		 * @param regnr The register number to write to
		 * 
		 * @return The number of bytes written, or a negative value on error
		 */
		// TODO exception model
		private int setRegister(int regnr) {
			if (closed)
				throw new IllegalStateException("this method cannot be called on a closed device");

			byte[] data = new byte[EP0_REPORT_SIZE];

			/* report number is register number plus 1 */
			data[0] = (byte) (regnr + 1);
			data[1] = (byte) ((registers[regnr] >> 8) & 0xFF);
			data[2] = (byte) (registers[regnr] & 0xFF);
			return setReport(data[0], data, data.length);
		}

		/*
		 * send a HID report
		 */
		private int setReport(int reportId, byte[] data, int length) {
			if (closed)
				throw new IllegalStateException("this method cannot be called on a closed device");

			return hidDevice.sendFeatureReport(reportId, data, length);
			/* TODO do we need the extra validation stuff?
			if (retval < 0)
				dev_warn(
					"si470x_set_report: hid_send_feature_report returned %d\n",
					retval);
			return retval;
			 */
		}
	}
}

class TunerData {
	public short[] block = {-1, -1, -1, -1};
	public short[] err = {9, 9, 9, 9};
	public boolean groupReady;
	public boolean rdsSynchronized;
	public boolean stereo;

	/** Received Signal Strength Indicator, 0..65535 */
	public int rssi;

	/** Frequency in kHz */
	public int frequency;
}
