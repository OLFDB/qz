/*
 * Copyright © 2017–2018 Michael von Glasow.
 * 
 * This file is part of Qz.
 *
 * Qz is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Qz is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Qz.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.vonglasow.michael.qz.android.core;

import java.sql.SQLException;

import com.vonglasow.michael.qz.android.util.Const;
import com.vonglasow.michael.qz.android.util.LtHelper;
import com.vonglasow.michael.qz.core.MessageCache;
import com.vonglasow.michael.qz.core.MessageWrapper;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;

/**
 * A receiver for poll requests from other applications.
 */
public class TraffRequestReceiver extends BroadcastReceiver {

	/* (non-Javadoc)
	 * @see android.content.BroadcastReceiver#onReceive(android.content.Context, android.content.Intent)
	 */
	@Override
	public void onReceive(Context context, Intent intent) {
		ReceiveTask receiveTask = new ReceiveTask();
		receiveTask.execute(context);
	}

	private class ReceiveTask extends AsyncTask<Context, Void, Void> {
		private Context context;
		private StringBuilder builder;

		@Override
		protected Void doInBackground(Context... params) {
			context = params[0];

			MessageCache cache;

			/* Initialize location table and message cache */
			LtHelper.getInstance(context);

			MessageCache.setDatabaseUrl(Const.getCacheDbUrl(context));
			try {
				cache = MessageCache.getInstance();
			} catch (ClassNotFoundException | SQLException e) {
				e.printStackTrace();
				throw new IllegalStateException(e);
			}

			/* Build feed from cached messages */
			builder = new StringBuilder();
			builder.append("<feed>\n");
			for (MessageWrapper wrapper : cache.getMessages()) {
				String msgString = wrapper.toXml();
				if (msgString != null)
					builder.append(msgString);
			}
			builder.append("</feed>\n");

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			/* Broadcast the feed */
			Intent outIntent = new Intent(Const.ACTION_TRAFF_FEED);
			outIntent.putExtra(Const.EXTRA_FEED, builder.toString());
			context.sendBroadcast(outIntent, Manifest.permission.ACCESS_COARSE_LOCATION);
		}
	}
}
