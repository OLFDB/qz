/*
 * Copyright © 2019 Michael von Glasow.
 * 
 * This file is part of Qz.
 *
 * Qz is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Qz is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Qz.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.vonglasow.michael.qz.android.core;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.vonglasow.michael.qz.android.util.Const;

import android.app.Application;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.util.Log;

public class QzApplication extends Application implements OnSharedPreferenceChangeListener {
	private static final String TAG = "QzApplication";
	private static final String FILE_PREFIX = "logcat";

	private Process logcatProcess = null;
	private File log = null;

	@Override
	public void onCreate() {
		super.onCreate();
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
		preferences.registerOnSharedPreferenceChangeListener(this);
		boolean debugMirror = preferences.getBoolean(Const.PREF_DEBUG_MIRROR, false);
		if (debugMirror)
			startLog();
	}

	@Override
	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
		if (key.equals(Const.PREF_DEBUG_MIRROR)) {
			boolean debugMirror = sharedPreferences.getBoolean(Const.PREF_DEBUG_MIRROR, false);
			if (debugMirror)
				startLog();
			else
				stopLog();
		}
	}

	/**
	 * @brief Runs the media scanner on the log file.
	 * 
	 * If the internal reference to the log file is null, this is a no-op. This may happen when no log was ever
	 * started, or a last media scan was already requested after logging was stopped or the logcat process has exited.
	 * 
	 * If the logcat process has exited, one more scan is triggered and the internal reference to the log file is set
	 * to null.
	 */
	public void runMediaScanner() {
		if (log == null)
			return;
		Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
		Uri contentUri = Uri.fromFile(log);
		mediaScanIntent.setData(contentUri);
		sendBroadcast(mediaScanIntent);

		/* if the logcat process has ended, set log file to null to prevent further scans */
		if (logcatProcess != null)
			try {
				logcatProcess.exitValue();
				log = null;
			} catch (IllegalThreadStateException e) {
				// NOP
			}
		else
			log = null;
	}

	/**
	 * @brief Starts mirroring log data.
	 * 
	 * If log data is already being mirrored, this method logs a warning and returns.
	 */
	public void startLog() {
		if (logcatProcess != null) {
			try {
				logcatProcess.exitValue();
			} catch (IllegalThreadStateException e) {
				Log.w(TAG, "log mirroring is already active, ignoring");
				return;
			}
		}
		Date now = new Date();
		DateFormat format = new SimpleDateFormat("YYYY-MM-dd-HH-mm-ss");
		String filename = String.format("%s-%s.txt", FILE_PREFIX, format.format(now));
		log = new File(getExternalFilesDir(null), filename);
		Log.i(TAG, "Mirroring log to " + log.getAbsolutePath());
		try {
			logcatProcess = Runtime.getRuntime().exec("logcat -f " + log.getAbsolutePath());
			runMediaScanner();
		} catch (IOException e) {
			Log.w(TAG, "cannot create log file");
			e.printStackTrace();
		}
	}

	/**
	 * @brief Stops mirroring log data.
	 * 
	 * If log data is not being mirrored, this is a no-op.
	 */
	public void stopLog() {
		if (logcatProcess != null) {
			Log.i(TAG, "Stopping log");
			logcatProcess.destroy();
			logcatProcess = null;
			runMediaScanner();
		}
	}
}
