/*
 * Copyright © 2017 Michael von Glasow.
 * 
 * This file is part of Qz.
 *
 * Qz is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Qz is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Qz.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.vonglasow.michael.qz.android.core;

import java.io.Closeable;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;

import com.vonglasow.michael.qz.android.input.Si470xGroupReader;
import com.vonglasow.michael.qz.android.ui.MainActivity;
import com.vonglasow.michael.qz.android.util.BluetoothManager;
import com.vonglasow.michael.qz.android.util.Const;
import com.vonglasow.michael.qz.android.util.LtHelper;
import com.vonglasow.michael.qz.android.util.PermissionHelper;
import com.vonglasow.michael.qz.core.MessageCache;
import com.vonglasow.michael.qz.core.MessageWrapper;
import com.vonglasow.michael.qz.tuning.AfiNetwork;
import com.vonglasow.michael.qz.tuning.OtherNetwork;
import com.vonglasow.michael.qz.tuning.TmcService;
import com.vonglasow.michael.qz.util.MessageListener;
import com.vonglasow.michael.qz.R;

import eu.jacquet80.rds.app.Application;
import eu.jacquet80.rds.app.ChangeListener;
import eu.jacquet80.rds.app.oda.AlertC;
import eu.jacquet80.rds.app.oda.AlertC.Message;
import eu.jacquet80.rds.core.DecoderShell;
import eu.jacquet80.rds.core.Station;
import eu.jacquet80.rds.core.TMCOtherNetwork;
import eu.jacquet80.rds.input.FileFormatGuesser;
import eu.jacquet80.rds.input.GnsGroupReader;
import eu.jacquet80.rds.input.GroupReader;
import eu.jacquet80.rds.input.TeeGroupReader;
import eu.jacquet80.rds.input.TunerGroupReader;
import eu.jacquet80.rds.input.UnavailableInputMethod;
import eu.jacquet80.rds.log.ApplicationChanged;
import eu.jacquet80.rds.log.DefaultLogMessageVisitor;
import eu.jacquet80.rds.log.EndOfStream;
import eu.jacquet80.rds.log.GroupReceived;
import eu.jacquet80.rds.log.LogMessageVisitor;
import eu.jacquet80.rds.log.StationTuned;
import android.Manifest;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.IBinder;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat.OnRequestPermissionsResultCallback;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.Toast;

public class TmcReceiver extends Service implements OnRequestPermissionsResultCallback {
	public static final String TAG = "TmcReceiver";

	/** The lowest frequency the receiver will tune to. */
	private static final int FREQ_MIN = 87500;
	/** The channel raster, or tuning resolution, of the receiver */
	private static final int FREQ_RASTER = 100;
	/** The upper end of the frequency band supported. */
	private static final int FREQ_MAX = 107900;

	private static final int ONGOING_NOTIFICATION_ID = 0x27;

	// TODO export this from AlertC
	/** Substring for unknown TMC provider (first or last 4 characters) */
	private static final String TMC_PROVIDER_UNKNOWN = "????";

	/** The Alternate Frequency Information (AFI) bit for the current station */
	private Boolean afi = null;

	/** The AlertC application instance for the current station */
	private AlertC alertCApp = null;

	/** The Bluetooth tuner device */
	private BluetoothDevice btDevice;

	/** The Bluetooth socket for connection with the tuner device */
	private BluetoothSocket btSocket;

	/** The TMC message cache */
	private MessageCache cache = null;

	/** The cached display name for the current TMC provider */
	private String cachedProviderName = null;

	/** The base name for debug output files */
	private String debugSessionName = "null";

	/** Whether to log detailed information */
	private boolean debugVerbose;

	/** 
	 * The current frequency in kHz.
	 * Synchronized to the class instance.
	 */
	private int freq = 0;

	/**
	 * Whether we got any new messages (other than updates) from the current station since we last checked.
	 * Synchronized to {@link #messages}.
	 */
	private boolean hasNewMessages = false;

	/** 
	 * Whether we have a valid LTN and SID for the current station.
	 * Synchronized to the class instance.
	 */
	private boolean hasProviderInfo = false;

	/**
	 * Whether the current station carries TMC information (i.e. the TMC ODA has been announced on
	 * it since we tuned into it, or a station with the same PI carried TMC and had the AFI bit set
	 * the last time it was picked up).
	 * 
	 * Synchronized to the class instance.
	 */
	private boolean hasTmc = false;

	/** The Location Table Number (LTN) of the current TMC service */
	private int ltn = -1;

	/**
	 * The intent which started the service.
	 */
	private Intent intent;

	/** 
	 * Whether the service is running as a foreground service.
	 * 
	 * Any write operations on this member, or conditional code depending on it, must be
	 * synchronized to the class instance.
	 */
	private boolean isForeground = false;

	/** Whether the source supplies live input */
	private boolean isLive = false;

	/** TMC messages received from the current station */
	private List<Message> messages = Collections.synchronizedList(new LinkedList<Message>());

	/** The number of messages currently in the cache */
	private int messageCount = 0;

	/** A {@link MessageListener} which updates the message count every time messages are added or deleted */
	private MessageListener messageListener;

	/** The notification builder for the notification shown while this service is running. */
	private Notification.Builder notificationBuilder;

	/** The Program Identifier (PI) code of the current station */
	private int pi = 0;

	/** 
	 * The previous frequency (before the last seek operation) in kHz.
	 * Synchronized to the class instance.
	 */
	private int prevFreq = 0;

	/** The display name for the current TMC provider */
	private String providerName = null;

	/** The Service Identifier (SID) if the current TMC service */
	private int sid = -1;

	/** The GroupReader which delivers incoming RDS groups */
	private GroupReader reader = null;

	/** 
	 * If input is being logged to a file, this is a wrapper around {@code reader} which duplicates input to a file.
	 * Else it is identical to {@code reader}.
	 */
	private GroupReader teeReader = null;

	/** Signal strength of current station, from 0 to 65535 */
	private int rssi = Const.RSSI_NOT_CONNECTED;

	/** Whether the receiver is currently running */
	private boolean running = false;

	/** Cache of stations and TMC services encountered during the last scan cycle */
	private StationInfo[] scanHistory = new StationInfo [(FREQ_MAX - FREQ_MIN) / FREQ_RASTER + 1];

	/** 
	 * Whether the receiver is requesting a new seek operation.
	 * Synchronized to the class instance.
	 */
	private boolean seekRequested = false;

	/** An intent filter for device detach events */
	private IntentFilter deviceDetachedFilter = new IntentFilter();

	/** The tuner dongle */
	private UsbDevice usbDevice;

	/** A broadcast receiver to process device detach events */
	private BroadcastReceiver deviceDetachedReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			if (intent != null) {
				if (intent.getAction().equals(BluetoothDevice.ACTION_ACL_DISCONNECTED)) {
					BluetoothDevice detachedDevice = (BluetoothDevice) intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);

					SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(TmcReceiver.this);
					boolean autoConnect = preferences.getBoolean(Const.PREF_BLUETOOTH_AUTO, false);

					onDeviceDetached(detachedDevice, autoConnect);
				} else if (intent.getAction().equals(UsbManager.ACTION_USB_DEVICE_DETACHED)) {
					UsbDevice detachedDevice = (UsbDevice) intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);

					onDeviceDetached(detachedDevice);
				} else if (intent.getAction().equals(Const.ACTION_BT_DISCONNECT_REQUESTED)) {
					if (btDevice != null)
						onDeviceDetached(btDevice, false);
				} else if (intent.getAction().equals(Const.ACTION_BT_STATUS_REQUEST)) {
					sendStatusBroadcast();
				}
			}
			((QzApplication)(context.getApplicationContext())).runMediaScanner();
		}

	};

	/**
	 * The nullConsole just does nothing. It silently discards any message.
	 */
	public static PrintStream nullConsole = new PrintStream(new OutputStream() {
		@Override
		public void write(int b) throws IOException {
		}
	});


	/** The ChangeListener which is notified of TMC-related events. */
	private ChangeListener alertCChangeListener = new ChangeListener() {
		public void notifyChange() {
			/* skip encrypted services */
			if (isLive && Boolean.TRUE.equals(alertCApp.isEncrypted())) {
				if (debugVerbose)
					Log.d(TAG, "    Service is encrypted, requesting retune...\n");
				requestSeek();
				return;
			}

			/* check for new messages */
			List<Message> newMessages = null;
			synchronized(alertCApp.getMessages()) {
				if (alertCApp.getMessages().size() > 0) {
					newMessages = new ArrayList<Message>(alertCApp.getMessages());
					alertCApp.getMessages().clear();
				}
			}
			if (newMessages != null)
				processMessages(newMessages);

			/* check for AFI bit */
			if (!Boolean.valueOf(alertCApp.getAFI() != 0).equals(afi))
				processAfi(pi, (alertCApp.getAFI() != 0));

			/* check for provider ID and name */
			if ((alertCApp.getLTN() != getLtn())
					|| (alertCApp.getSID() != getSid())
					|| !alertCApp.getProviderName().equals(providerName)) {
				processProvider(pi, alertCApp.getLTN(), alertCApp.getSID(), alertCApp.getProviderName());
				if (isLive) {
					TunerGroupReader tgr = (TunerGroupReader) reader;
					StationInfo info = new StationInfo(getFreq(), tgr.getSignalStrength(), pi, getLtn(), getSid());

					/* clear scan history entries between prevFreq and freq and add the current one */
					synchronized(TmcReceiver.this) {
						if (prevFreq != 0) {
							long lastReceived = 0;
							if (freq > prevFreq) {
								for (int i = getIndexFromFrequency(prevFreq) + 1;
										(i <= getIndexFromFrequency(freq)) && (i < scanHistory.length); i++) {
									if (scanHistory[i] != null)
										lastReceived = scanHistory[i].lastReceived;
									scanHistory[i] = null;
								}
							} else {
								for (int i = getIndexFromFrequency(prevFreq) + 1; i < scanHistory.length; i++) {
									if (scanHistory[i] != null)
										lastReceived = scanHistory[i].lastReceived;
									scanHistory[i] = null;
								}
								for (int i = 0; (i <= getIndexFromFrequency(freq)) && (i < scanHistory.length); i++) {
									if (scanHistory[i] != null)
										lastReceived = scanHistory[i].lastReceived;
									scanHistory[i] = null;
								}
							}
							try {
								if (scanHistory[getIndexFromFrequency(freq)] != null)
									lastReceived = scanHistory[getIndexFromFrequency(freq)].lastReceived;
							} catch (ArrayIndexOutOfBoundsException e) {
								// NOP
							}

							if (lastReceived > 0) {
								long cycleDuration = info.lastReceived - lastReceived;
								cache.setScanCycleDuration((int) Math.ceil(cycleDuration / 1000));
							}
						}

						try {
							scanHistory[getIndexFromFrequency(freq)] = info;
						} catch (ArrayIndexOutOfBoundsException e) {
							// NOP
						}
					}

					/* if we already picked up this service in this scan cycle in better quality, skip */
					for (int i = 0; (i < scanHistory.length) && !isSeekRequested(); i++)
						if (scanHistory[i] != null
						&& (i != getIndexFromFrequency(getFreq()))
						&& !scanHistory[i].isSkipped()
						&& info.isSameService(scanHistory[i])
						&& info.rssi <= scanHistory[i].rssi) {
							if (debugVerbose)
								Log.d(TAG, "    Repetition of previous service, requesting retune...\n");
							info.markSkipped();
							requestSeek();
						}
				}
			}

			/* check for tuning information */
			Map<Integer, TMCOtherNetwork> otherNetworks = null;
			synchronized(alertCApp.getOtherNetworks()) {
				if (alertCApp.getOtherNetworks().size() > 0) {
					otherNetworks = new HashMap<Integer, TMCOtherNetwork>(alertCApp.getOtherNetworks());
					alertCApp.getOtherNetworks().clear();
				}
			}
			if (otherNetworks != null)
				processOtherNetworks(otherNetworks);
		}
	};


	/** The LogMessageVisitor which is notified of changes. */
	private LogMessageVisitor messageVisitor = new DefaultLogMessageVisitor() {
		@Override
		public void visit(GroupReceived groupReceived) {
			int freq = 0;
			TunerGroupReader tgr = null;

			int[] blocks = groupReceived.getBlocks();

			/* Identify version of the group: if it is B, we can extract the PI from block 2 */
			int version = -1;
			if(blocks[1] >= 0) {
				version = ((blocks[1]>>11) & 1);
			}

			int pi = -1;

			if(blocks[0] >= 0) {
				pi = blocks[0];
			} else if(version == 1 && (blocks[2] >= 0)) {
				pi = blocks[2];
			}

			if ((pi < 0) || (pi == TmcReceiver.this.getPi()))
				return;

			if (isLive) {
				tgr = (TunerGroupReader) reader;
				freq = tgr.getFrequency();
			}

			synchronized(TmcReceiver.this) {
				if ((freq != TmcReceiver.this.freq) || !isLive) {
					/* 
					 * do this only if the frequency has actually changed (otherwise the PI change
					 * was likely caused by noise) 
					 */
					synchronized(messages) {
						messages.clear();
						hasNewMessages = false;
					}
					hasProviderInfo = false;
					hasTmc = false;
					TmcReceiver.this.freq = freq;

					/* update service name in notification */
					AfiNetwork network = cache.getAfiNetwork(pi);
					if (network != null) {
						TmcService service = new TmcService(pi >> 12, network.ltn, network.sid);
						service = cache.getServiceName(service);
						cachedProviderName = (service == null) ? null : service.name;
					} else {
						setLtn(-1);
						setSid(-1);
						cachedProviderName = null;
					}
					updateNotification();
				}

				setPi(pi);
			}

			if (debugVerbose)
				Log.d(TAG, String.format("  At %3.1f, PI = %04X, RSSI = %d\n",
						freq / 1000f, pi, isLive ? tgr.getSignalStrength() : -1));
		}

		@Override
		public void visit(ApplicationChanged applicationChanged) {
			Application newApp = applicationChanged.getNewApplication();
			if (newApp instanceof AlertC) {
				TmcReceiver.this.alertCApp = (AlertC) newApp;
				((AlertC) newApp).storeCancellationMessages(true);
				newApp.addChangeListener(alertCChangeListener);
				synchronized(TmcReceiver.this) {
					hasTmc = true;
				}
				if (debugVerbose)
					Log.d(TAG, "  AlertC listener registered");
			}
		}

		@Override
		public void visit(StationTuned stationTuned) {
			// TODO should we handle PI changes here?
		}

		@Override
		public void visit(EndOfStream endOfStream) {
			// TODO do we need to handle this for live data?
			if (!isLive) {
				Log.d(TAG, "End of canned data reached, stopping service");

				/*
				 * Cleanup includes removing the message listener, which cannot be done from a listener method.
				 * As a workaround, we send ourselves an intent stating the end of canned data has been reached.
				 */
				Intent outIntent = new Intent(Const.ACTION_CANNED_DATA_EOF, null, TmcReceiver.this, TmcReceiver.class);
				startService(outIntent);
			}
		}
	};

	/**
	 * A thread which constantly scans the FM band for TMC services.
	 * 
	 * After tuning into a new station, the scanner listens to RDS/TMC data and waits for 12 s to
	 * obtain the LTN and SID for the service, either over the air or from its cache. When it has
	 * obtained the LTN and SID, it waits for TMC messages.
	 * 
	 * The scanner retunes if:
	 * <ul>
	 * <li>No RDS data is received for more than 2 s</li>
	 * <li>No TMC ODA is registered during the first 6 s, and no LTN and SID for the current
	 * station is in the cache</li>
	 * <li>The LTN and SID for the current station are not in the cache and are not received during
	 * the first 12 s</li>
	 * <li>No new TMC messages (which are not updates to another message received since the last
	 * retune) are received for 12 s</li>
	 * </ul>
	 * 
	 * This behavior deliberately deviates from the RDS TMC spec: according to the latter, a TMC
	 * receiver should only scan until it has obtained a service, then stay on that frequency until
	 * it moves out of range, use EON/ON information to retune and scan only as a last resort.
	 * However, this leads to a lock-in situation if competing TMC services do not announce each
	 * other, as the receiver would stay in the first network it has picked up and never attempt
	 * to move to another, causing messages to be missed when the vehicle is near the border of
	 * the coverage area. By mostly ignoring this information and scanning, we ensure that we pick
	 * up all TMC services in range.
	 */
	private Thread scanner = new Thread() {
		public void run() {
			while(running) {
				if (!(reader instanceof TunerGroupReader)) {
					running=false;
					return;
				}
				TunerGroupReader tgr = (TunerGroupReader) reader;
				boolean rdsReceived = false;
				boolean hasNewMessages = false;
				do {
					try {
						/*
						 * If we don't have provider info (i.e. on startup), wait up to 12 s to
						 * obtain it, but abort if no RDS is received in 2 s or no TMC is received
						 * in the first 6 s.
						 */
						for (int i = 0; (!hasProviderInfo() && !isSeekRequested() && i < 6); i++) {
							sleep(2000);
							TmcReceiver.this.rssi = tgr.getSignalStrength();
							sendStatusBroadcast();
							rdsReceived = tgr.newGroups();
							if (!rdsReceived)
								break;
							if (i >= 2 && !hasTmc())
								break;
						}

						/* 
						 * If we have provider information, wait for new messages.
						 * Maximum TMC window duration is 8 s; wait 12 s to ensure we receive a message.
						 */
						if (hasProviderInfo())
							for (int i = 0; !isSeekRequested() && i < 6; i++) {
								sleep(2000);
								TmcReceiver.this.rssi = tgr.getSignalStrength();
								sendStatusBroadcast();
								rdsReceived = tgr.newGroups();
								if (!rdsReceived)
									break;
							}
					} catch (InterruptedException e) {
						break;
					}
					synchronized(TmcReceiver.this.messages) {
						hasNewMessages = TmcReceiver.this.hasNewMessages;
						TmcReceiver.this.hasNewMessages = false;
					}
				} while (rdsReceived && hasProviderInfo() && hasNewMessages && !isSeekRequested());
				if (debugVerbose) {
					if (isSeekRequested())
						Log.d(TAG, String.format("    At %3.1f, retune requested.\n", tgr.getFrequency() / 1000f));
					else if (!rdsReceived)
						Log.d(TAG, String.format("    At %3.1f, no RDS received in last 2 seconds.\n", tgr.getFrequency() / 1000f));
					else if (!hasTmc())
						Log.d(TAG, String.format("    At %3.1f, no TMC data in 6 seconds.\n", tgr.getFrequency() / 1000f));
					else if (!hasProviderInfo())
						Log.d(TAG, String.format("    At %3.1f, no provider info in 12 seconds.\n", tgr.getFrequency() / 1000f));
					else if (!hasNewMessages)
						Log.d(TAG, "    No new TMC messages in last 12 seconds.\n");
				}

				/* Check if we have lost the BT device */
				if ((btDevice != null) && !btSocket.isConnected()) {
					onDeviceDetached(btDevice, true);
					break;
				}

				if (debugVerbose)
					Log.d(TAG, "*** Tuning...\n");
				synchronized(TmcReceiver.this.messages) {
					tgr.seek(true);
					messages.clear();
					hasNewMessages = false;
				}
				synchronized(TmcReceiver.this) {
					hasProviderInfo = false;
					hasTmc = false;
					seekRequested = false;
					prevFreq = freq;
				}
			}
		}
	};

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@Override
	public void onCreate() {
		Log.d(TAG, "onCreate() " + this.toString());
		deviceDetachedFilter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECTED);
		deviceDetachedFilter.addAction(UsbManager.ACTION_USB_DEVICE_DETACHED);
		deviceDetachedFilter.addAction(Const.ACTION_BT_DISCONNECT_REQUESTED);
		deviceDetachedFilter.addAction(Const.ACTION_BT_STATUS_REQUEST);

		/* message listener */
		messageListener = new MessageListener() {
			@Override
			public void onUpdateReceived(final MessageWrapper added, final Collection<MessageWrapper> removed) {
				messageCount = cache.getMessages().size();
				updateNotification();

				// FIXME handle cancellation messages (and revisit message removal)
				if (added != null) {
					StringBuilder builder = new StringBuilder();
					builder.append("<feed>\n");
					String addedString = added.toXml();
					if (addedString != null) {
						builder.append(addedString);
						builder.append("</feed>\n");

						/* Broadcast the feed */
						Intent outIntent = new Intent(Const.ACTION_TRAFF_FEED);
						outIntent.putExtra(Const.EXTRA_FEED, builder.toString());
						sendBroadcast(outIntent, Manifest.permission.ACCESS_COARSE_LOCATION);
					}
				}
			}
		};
	}

	/**
	 * Called when a Bluetooth device is detached.
	 * 
	 * If the device being detached is the currently used tuner device, this method will release
	 * all resources associated with it.
	 * 
	 * If {@code reconnect} is false, this method will also stop the service; else it will attempt to
	 * reconnect to the device and stop the service if this fails.
	 * 
	 * @param detachedDevice The device
	 * @param reconnect Whether an attempt should be made to re-establish the connection
	 */
	public void onDeviceDetached(BluetoothDevice detachedDevice, boolean reconnect) {
		if ((btDevice != null) && (btDevice.equals(detachedDevice))) {
			if (reconnect)
				Log.d(TAG, "Tuner device detached, attempting to reconnect");
			else
				Log.d(TAG, "Tuner device detached, stopping service");

			closeBluetoothDevice();

			if (reconnect) {
				Intent outIntent = new Intent(Const.ACTION_BT_CONNECTION_REQUESTED, null, TmcReceiver.this, TmcReceiver.class);
				outIntent.putExtra(Const.EXTRA_DEVICE, detachedDevice.getAddress());
				outIntent.putExtra(Const.EXTRA_SILENT, true);
				outIntent.putExtra(Const.EXTRA_KEEP_EXISTING_CONNECTION, false);

				StartupTask startupTask = new StartupTask();
				startupTask.execute(outIntent);
			} else {
				stop();
			}
		}
	}

	/**
	 * Called when a USB device is detached.
	 * 
	 * If the device being detached is the currently used tuner device, this method will release
	 * all resources associated with it and stop the service.
	 */
	public void onDeviceDetached(UsbDevice detachedDevice) {
		if (usbDevice == null)
			Log.d(TAG, "Device reported as detached but nothing connected (usbDevice is null), ignoring");
		else if (!usbDevice.equals(detachedDevice))
			Log.d(TAG, String.format("Device reported detached (%s) does not match tuner device %s, ignoring",
					usbDevice.getDeviceName(), detachedDevice.getDeviceName()));
		else {
			Log.d(TAG, "Tuner device detached, stopping service");

			closeUsbDevice();

			stop();
		}
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
		if ((requestCode == Const.PERM_REQUEST_LOCATION_RECEIVER) && (grantResults.length > 0))
			if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
				start();
			} else {
				Log.w(TAG, "ACCESS_COARSE_LOCATION permission not granted, aborting.");
				stopForeground();
				stopSelf();
			}
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		this.intent = intent;
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(TmcReceiver.this);
		debugVerbose = preferences.getBoolean(Const.PREF_DEBUG_VERBOSE, false);

		if (intent.getAction() == Const.ACTION_CANNED_DATA_EOF) {
			// end of canned data, stop service
			if ((reader != null) && (reader instanceof Closeable))
				try {
					((Closeable) reader).close();
				} catch (IOException e) {
					if (debugVerbose)
						e.printStackTrace();
				}

			closeInput();
			stop();
		} else if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
						!= PackageManager.PERMISSION_GRANTED) {
			Log.i(TAG, "ACCESS_COARSE_LOCATION permission not granted, asking for it...");
			PermissionHelper.requestPermissions(this,
					new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
					Const.PERM_REQUEST_LOCATION_RECEIVER,
					getString(R.string.notif_perm_title),
					getString(R.string.notif_perm_text),
					R.drawable.ic_permissions);
			startForeground();
		} else {
			// the intent tells us to connect a new device and we have permission, start service
			start();
		}

		return START_REDELIVER_INTENT;
	}

	/**
	 * @brief Closes the attached Bluetooth device, if any, but does not stop the service.
	 */
	private void closeBluetoothDevice() {
		if (btDevice == null)
			return;

		Log.d(TAG, "Stopping Bluetooth device");

		if ((reader != null) && (reader instanceof Closeable))
			try {
				((Closeable) reader).close();
			} catch (IOException e) {
				if (debugVerbose)
					e.printStackTrace();
			}

		try {
			btSocket.close();
		} catch (Exception e) {
			if (debugVerbose)
				e.printStackTrace();
		}
		btDevice = null;
		btSocket = null;

		closeInput();

		sendStatusBroadcast();
	}

	/**
	 * @brief Performs cleanup tasks common to USB and Bluetooth shutdown
	 */
	private void closeInput() {
		DecoderShell.instance.getLog().removeNewMessageListener(messageVisitor);
		rssi = Const.RSSI_NOT_CONNECTED;
		
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(TmcReceiver.this);
		boolean debugInput = preferences.getBoolean(Const.PREF_DEBUG_INPUT, false);
		boolean debugOutput = preferences.getBoolean(Const.PREF_DEBUG_OUTPUT, false);

		File dumpDir = TmcReceiver.this.getExternalFilesDir(null);
		if (isLive && debugInput) {
			Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
			Uri contentUri = Uri.fromFile(new File(dumpDir, debugSessionName + Const.FILE_EXTENSION_RDS));
			mediaScanIntent.setData(contentUri);
			sendBroadcast(mediaScanIntent);
		}
		if (isLive && debugOutput) {
			File outputFile = new File(dumpDir, debugSessionName + Const.FILE_EXTENSION_TRAFF);
			try {
				PrintWriter writer = new PrintWriter(outputFile);
				writer.println("<feed>");
				for (MessageWrapper wrapper : cache.getMessages()) {
					String msgString = wrapper.toXml();
					if (msgString != null)
						writer.print(msgString);
				}
				writer.println("</feed>");
				writer.close();
				Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
				Uri contentUri = Uri.fromFile(outputFile);
				mediaScanIntent.setData(contentUri);
				sendBroadcast(mediaScanIntent);
			} catch (FileNotFoundException e) {
				if (debugVerbose)
					e.printStackTrace();
			}
		}
		((QzApplication)(this.getApplicationContext())).runMediaScanner();
	}

	/**
	 * @brief Closes the attached USB device, if any, but does not stop the service.
	 */
	private void closeUsbDevice() {
		if (usbDevice == null)
			return;

		Log.d(TAG, "Stopping USB device");

		if ((reader != null) && (reader instanceof Closeable))
			try {
				((Closeable) reader).close();
			} catch (IOException e) {
				if (debugVerbose)
					e.printStackTrace();
			}

		usbDevice = null;

		closeInput();
	}

	/**
	 * @brief Gets the Alternate Frequency Information (AFI) bit
	 */
	private synchronized Boolean getAfi() {
		return afi;
	}

	/**
	 * @brief Gets the current frequency in kHz.
	 */
	private synchronized int getFreq() {
		return freq;
	}

	/**
	 * @brief Converts a frequency to an index into the scan history.
	 * 
	 * While the indices returned by this function are largely similar to the frequency indices
	 * used in the TMC stream, this assumption is not guaranteed and may change in the future.
	 * Use this function only to get an index into the scan history, and use only this function
	 * for that purpose.
	 * 
	 * @param aFreq The frequency in kHz. Minimum is 87500, i.e. 87.5 MHz.
	 * 
	 * @return The index of the corresponding entry in the scan history.
	 */
	private int getIndexFromFrequency(int aFreq) {
		return (aFreq - FREQ_MIN) / FREQ_RASTER;
	}

	/**
	 * @brief Gets the Location Table Number (LTN) for the current TMC service.
	 */
	private synchronized int getLtn() {
		return ltn;
	}

	/**
	 * @brief Gets the Program Identification (PI) code of the current station.
	 */
	private synchronized int getPi() {
		return pi;
	}

	/**
	 * @brief Gets the Service Identifier (SID) if the current TMC service.
	 */
	private synchronized int getSid() {
		return sid;
	}

	/**
	 * @brief Whether we have a valid LTN and SID for the current station.
	 * 
	 * This method is synchronized to the class instance.
	 */
	private synchronized boolean hasProviderInfo() {
		return hasProviderInfo;
	}

	/**
	 * @brief Whether the TMC ODA has been announced on the current station.
	 * 
	 * This method is synchronized to the class instance.
	 */
	private synchronized boolean hasTmc() {
		return hasTmc;
	}

	/**
	 * @brief Whether the receiver is requesting a new seek operation.
	 * 
	 * This method is synchronized to the class instance.
	 */
	private synchronized boolean isSeekRequested() {
		return seekRequested;
	}

	/**
	 * @brief Processes a change in the AFI bit.
	 * 
	 * @param pi The Program Identification (PI) code for the station
	 * @param afi The value of the AFI bit
	 */
	private void processAfi(int pi, boolean afi) {
		setAfi(afi);

		if (!afi) {
			try {
				cache.removeAfiNetwork(pi);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		} else if ((pi != 0) && (getLtn() != -1) && getSid() != -1)
			try {
				cache.putAfiNetwork(new AfiNetwork(pi, getLtn(), getSid()));
			} catch (SQLException e) {
				e.printStackTrace();
			}

		if (debugVerbose)
			Log.d(TAG, String.format("    AFI for %04X is %b\n", pi, afi));
	}

	/**
	 * @brief Processes a list of newly received TMC messages.
	 * 
	 * @param newMessages The new messages
	 */
	private void processMessages(List<Message> newMessages) {
		for (Message message : newMessages) {
			/* inject LTN and SID if missing */
			if ((message.getLocationTableNumber() == -1) || (message.getSid() == -1))
				try {
					message.setService(getLtn(), getSid());
				} catch (IllegalArgumentException e) {
					/* The message has partial service information but it doesn’t match ours; skip */
				}

			try {
				cache.putMessage(message);
			} catch (SQLException e) {
				e.printStackTrace();
			}

			/* keep track of fully resolved messages received from this station */
			if (message.isFullyResolved())
				synchronized (messages) {
					/* find messages which are overridden by the new message */
					List<Message> messagesToRemove = new LinkedList<Message>();
					for (Message m : messages)
						if (message.overrides(m))
							messagesToRemove.add(m);
					for (Message m : messagesToRemove)
						messages.remove(m);

					/* we also add cancellation messages so we can detect updates to them */
					messages.add(message);

					hasNewMessages |= messagesToRemove.isEmpty();
				}
		}

		if (debugVerbose)
			Log.d(TAG, "    Messages received");
	}

	/**
	 * @brief Processes a list of TMC Other Network information
	 * 
	 * @param otherNetworks The new Other Network information
	 */
	private void processOtherNetworks(Map<Integer, TMCOtherNetwork> otherNetworks) {
		for (Integer pi : otherNetworks.keySet()) {
			if (pi == null)
				continue;
			TMCOtherNetwork on = otherNetworks.get(pi);
			if (on == null)
				continue;

			/* Variant 6: PI and two AFs */
			Set<Integer> afs = on.getPseudoMethodAAFs();
			for (Integer afc : afs) {
				if (afc == null)
					continue;
				float af = (float) Station.channelToFrequency(afc) / 10.0f;
				try {
					cache.putOtherNetwork(new OtherNetwork(this.getPi(), pi, af));
				} catch (SQLException e) {
					e.printStackTrace();
				}

				if (debugVerbose)
					Log.d(TAG, String.format("    Tuning Info 6 for %04X: PI %04X, AF: %.1f\n",
							this.getPi(), pi, af));
			}

			/* Variant 7: PI, AF and TN */
			Map<Integer, Set<Integer>> mappedAfs = on.getMappedAFs();
			for (Integer tnc : mappedAfs.keySet()) {
				if (tnc == null)
					continue;
				Set<Integer> mafs = mappedAfs.get(tnc);
				if (mafs == null)
					continue;
				float tn = (float) Station.channelToFrequency(tnc) / 10.0f;
				for (Integer afc : mafs) {
					if (afc == null)
						continue;
					float af = (float) Station.channelToFrequency(afc) / 10.0f;
					try {
						cache.putOtherNetwork(new OtherNetwork(this.getPi(), tn, pi, af));
					} catch (SQLException e) {
						e.printStackTrace();
					}

					if (debugVerbose)
						Log.d(TAG, String.format("    Tuning Info 7 for %04X: PI %04X, AF: %.1f, TN: %.1f\n",
								this.getPi(), pi, af, tn));
				}
			}

			/* Variant 8/9 */
			if (afs.isEmpty() && mappedAfs.isEmpty()) {
				if ((on.getLtn() != -1) && (on.getSid() != -1)) {
					/* Variant 9: PI with LTN, MGS and SID */
					try {
						cache.putAfiNetwork(new AfiNetwork(pi, on.getLtn(), on.getSid()));
					} catch (SQLException e) {
						e.printStackTrace();
					}
					if ((this.getPi() > 0) && (this.getLtn() >= 0) && (this.getSid() >= 0))
						/* different service, add mapping between the two */
						try {
							cache.putOnMappedService(new TmcService(this.getPi() >> 12, this.getLtn(), this.getSid()),
									new TmcService(pi >> 12, on.getLtn(), on.getSid()));
						} catch (SQLException e) {
							e.printStackTrace();
						}
				} else
					/* Variant 8: PI */
					/* Zero PI or own PI may be used as fillers, these are discarded here. */
					if ((pi > 0) && (pi != this.getPi()))
						try {
							cache.putAfiNetwork(new AfiNetwork(pi, on.getLtn(), on.getSid()));
						} catch (SQLException e) {
							e.printStackTrace();
						}

				if (debugVerbose)
					Log.d(TAG, String.format("    Tuning Info 8/9 for %04X: PI %04X, LTN %d, MGS %s, SID %d\n",
							this.getPi(), pi, on.getLtn(), AlertC.decodeMGS(on.getMgs()), on.getSid()));
			}
		}
	}

	/**
	 * @brief Processes new provider information
	 * 
	 * @param pi The Program Identification (PI) code for the station
	 * @param ltn The Location Table Number of the current TMC service
	 * @param sid The Service Identifier of the current TMC service
	 * @param provider The display name for the current TMC service
	 */
	private void processProvider(int pi, int ltn, int sid, String provider) {
		if ((ltn < 0) || (sid < 0))
			return;

		synchronized(this) {
			hasProviderInfo = true;
		}

		int cc = pi >> 12;

		if ((ltn != this.getLtn()) || (sid != this.getSid())) {
			setLtn(ltn);
			setSid(sid);

			if (Boolean.TRUE.equals(getAfi()))
				try {
					cache.putAfiNetwork(new AfiNetwork(pi, ltn, sid));
				} catch (SQLException e) {
					e.printStackTrace();
				}

			if (debugVerbose)
				Log.d(TAG, String.format("    PI %04X, service: CC %X, LTN %d, SID %d\n", pi, cc, ltn, sid));
		}

		if (provider.equals(this.providerName))
			return;

		if (provider.startsWith(TMC_PROVIDER_UNKNOWN)
				|| provider.endsWith(TMC_PROVIDER_UNKNOWN)) {
			/* no valid provider name, try to display a cached one and exit */
			TmcService service = new TmcService(cc, this.getLtn(), this.getSid());
			service = cache.getServiceName(service);
			cachedProviderName = (service == null) ? null : service.name;
			updateNotification();

			return;
		}

		/* we have a valid provider string which does not match the current one */
		this.providerName = provider;
		this.cachedProviderName = provider;
		updateNotification();

		try {
			cache.putServiceName(new TmcService(cc, ltn, sid, provider));
		} catch (SQLException e) {
			e.printStackTrace();
		}

		if (debugVerbose)
			Log.d(TAG, String.format("    PI %04X, provider: %s (CC %X, LTN %d, SID %d)\n", pi, provider, cc, ltn, sid));
	}

	/**
	 * @brief Requests the scanner to start a seek operation as soon as possible.
	 * 
	 * This is typically called when the receiver determines that the current station does not
	 * provide useful information, e.g. because the same service has been picked up earlier in the
	 * same scan cycle or because the service supplies only encrypted information.
	 */
	private synchronized void requestSeek() {
		seekRequested = true;
	}

	/**
	 * @brief Sends a broadcast reporting Bluetooth connection status and FM RSSI.
	 */
	private void sendStatusBroadcast() {
		Intent outIntent = new Intent((btDevice != null) ? Const.ACTION_BT_CONNECTED : Const.ACTION_BT_DISCONNECTED);
		if ((btDevice == null) && (usbDevice == null))
			outIntent.putExtra(Const.EXTRA_RSSI, Const.RSSI_NOT_CONNECTED);
		else {
			outIntent.putExtra(Const.EXTRA_RSSI, this.rssi);
			if (freq >= FREQ_MIN) {
				outIntent.putExtra(Const.EXTRA_FREQ, this.freq);
				synchronized(this) {
					if ((this.cachedProviderName != null)
							&& !this.cachedProviderName.startsWith(TMC_PROVIDER_UNKNOWN)
							&& !this.cachedProviderName.endsWith(TMC_PROVIDER_UNKNOWN))
						outIntent.putExtra(Const.EXTRA_PROVIDER_NAME, this.cachedProviderName);
					else if ((ltn > 0) && (sid > 0))
						outIntent.putExtra(Const.EXTRA_PROVIDER_NAME, String.format("%X/%d/%d", pi >> 12, ltn, sid));
				}
			}
		}
		sendBroadcast(outIntent);
	}

	/**
	 * @brief Sets the Alternate Frequency Information (AFI) bit
	 */
	private synchronized void setAfi(boolean afi) {
		this.afi = afi;
	}

	/**
	 * @brief Sets the Location Table Number (LTN) for the current TMC service.
	 */
	private synchronized void setLtn(int ltn) {
		this.ltn = ltn;
	}

	/**
	 * @brief Sets the Program Identification (PI) code of the current station.
	 */
	private synchronized void setPi(int pi) {
		if (this.pi == pi)
			return;
		this.pi = pi;
		AfiNetwork network = cache.getAfiNetwork(pi);
		if (network != null) {
			this.afi = true;
			this.ltn = network.ltn;
			this.sid = network.sid;
			this.hasProviderInfo = true;

			if (debugVerbose)
				Log.d(TAG, String.format("    PI %04X, cached service: CC %X, LTN %d, SID %d\n", pi, pi >> 12, ltn, sid));
		} else {
			this.afi = null;
			this.ltn = -1;
			this.sid = -1;
			this.hasTmc = true;
		}
	}

	/**
	 * @brief Sets the Service Identifier (SID) of the current TMC service.
	 */
	private synchronized void setSid(int sid) {
		this.sid = sid;
	}

	private void start() {
		Log.d(TAG, "Starting service " + this.toString());

		this.registerReceiver(deviceDetachedReceiver, deviceDetachedFilter);

		StartupTask startupTask = new StartupTask();
		startupTask.execute(this.intent);
	}

	/**
	 * @brief Makes this service a foreground service.
	 */
	private synchronized void startForeground() {
		Intent notificationIntent = new Intent(this, MainActivity.class);
		PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);

		notificationBuilder = new Notification.Builder(this);
		notificationBuilder.setContentTitle(String.format(getString(R.string.notification_title), messageCount));
		notificationBuilder.setContentText(getText(R.string.notification_text_nofreq));
		notificationBuilder.setSmallIcon(R.drawable.ic_stat_notify);
		notificationBuilder.setContentIntent(pendingIntent);

		startForeground(ONGOING_NOTIFICATION_ID, notificationBuilder.getNotification());

		isForeground = true;
	}

	/**
	 * @brief Stops the currently running service instance.
	 * 
	 * This unregisters the broadcast receiver and stops the service.
	 * 
	 * Callers must close devices and readers prior to this call.
	 */
	private void stop() {
		Log.d(TAG, "Stopping service " + this.toString());

		running = false;

		try {
			this.unregisterReceiver(deviceDetachedReceiver);
		} catch (Exception e) {
			/*
			 * Under some circumstances (e.g. unsuccessful connection attempt when the receiver was
			 * not already running), we may get here with no receiver being registered, in which
			 * case trying to unregister it will throw an exception. No action is needed, other
			 * than catching the exception.
			 */
		}
		cache.removeListener(messageListener);

		stopForeground();
		this.stopSelf();
	}

	/**
	 * @brief Removes this service from the foreground state and removes its notification.
	 */
	private synchronized void stopForeground() {
		isForeground = false;
		stopForeground(true);
	}

	/**
	 * @brief Updates the notification.
	 */
	private synchronized void updateNotification() {
		sendStatusBroadcast();
		if (!isForeground)
			return;

		notificationBuilder.setContentTitle(String.format(getString(R.string.notification_title), messageCount));
		if (freq >= FREQ_MIN)
			synchronized(this) {
				if ((cachedProviderName != null)
						&& !cachedProviderName.startsWith(TMC_PROVIDER_UNKNOWN)
						&& !cachedProviderName.endsWith(TMC_PROVIDER_UNKNOWN))
					notificationBuilder.setContentText(String.format(getString(R.string.notification_text_name),
							freq / 1000.0f, cachedProviderName.trim()));
				else if ((ltn > 0) && (sid > 0))
					notificationBuilder.setContentText(String.format(getString(R.string.notification_text_name),
							freq / 1000.0f, String.format("%X/%d/%d", pi >> 12, ltn, sid)));
				else
					notificationBuilder.setContentText(String.format(getString(R.string.notification_text_noname),
							freq / 1000.0f));
			}
		else
			notificationBuilder.setContentText(getString(R.string.notification_text_nofreq));

		NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		notificationManager.notify(ONGOING_NOTIFICATION_ID, notificationBuilder.getNotification());
	}


	/**
	 * Information about a station and its TMC service.
	 */
	private static class StationInfo {
		/** Frequency in kHz */
		public final int freq;
		/** Signal strength, from 0 to 65535 */
		public final int rssi;
		/** The PI code of the station */
		public final int pi;
		/** The LTN for the TMC service */
		public final int ltn;
		/** The SID for the TMC service */
		public final int sid;
		/** Whether the station was skipped in the last scan cycle */
		private boolean skipped = false;
		/**
		 * Time the station was last picked up, in milliseconds since boot.
		 * 
		 * This refers to the time scan stopped at the station, not when scanning resumed, thus data from the station
		 * was likely received beyond that time.
		 */
		public final long lastReceived;

		public StationInfo(int freq, int rssi, int pi, int ltn, int sid) {
			this.freq = freq;
			this.rssi = rssi;
			this.pi = pi;
			this.ltn = ltn;
			this.sid = sid;
			this.lastReceived = SystemClock.elapsedRealtime();
		}

		/**
		 * @brief Obtains the country code from the PI.
		 * 
		 * @return The single-digit country code
		 */
		public int getCountryCode() {
			return (pi >> 12) & 0xF;
		}

		/**
		 * @brief Whether two {@code StationInfo} instances refer to the same TMC service.
		 * 
		 * @return true if, and only if, the country code derived from the {@code pi} field, as
		 * well as the {@code ltn} and {@code sid} fields, match. All other fields are not compared.
		 */
		public boolean isSameService(StationInfo that) {
			return (this.getCountryCode() == that.getCountryCode()) && (this.ltn == that.ltn) && (this.sid == that.sid);
		}

		/** 
		 * @brief Whether the station was skipped in the last scan cycle
		 */
		public boolean isSkipped() {
			return skipped;
		}

		/**
		 * @brief Mark the station as skipped during the last scan cycle
		 */
		public void markSkipped() {
			skipped = true;
		}
	}

	private class StartupTask extends AsyncTask<Intent, Void, Void> {
		/** Whether the connection request was ignored. */
		boolean ignoreRequest = false;

		GroupReader newReader = null;

		/** Text for toast alert to display to the user (if null, no toast will be displayed) */
		String toastText = null;

		/** Duration for toast alert (meaningful only if {@code toastText} is not null) */
		int toastLength = Toast.LENGTH_SHORT;

		/** Whether to suppress error messages */
		boolean silent;

		@Override
		protected Void doInBackground(Intent... params) {
			Intent intent = params[0];

			LtHelper.getInstance(TmcReceiver.this);

			if (cache == null) {

				MessageCache.setDatabaseUrl(Const.getCacheDbUrl(TmcReceiver.this));
				try {
					cache = MessageCache.getInstance();
				} catch (ClassNotFoundException | SQLException e) {
					e.printStackTrace();
					throw new IllegalStateException(e);
				}

				messageCount = cache.getMessages().size();

				DecoderShell.instance.setConsole(nullConsole);
			}

			if (intent.getAction() == Const.ACTION_USB_DEVICE_ATTACHED) {
				/* Si470x USB device attached */
				closeBluetoothDevice();
				closeUsbDevice();

				usbDevice = (UsbDevice) intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);

				/* set up group reader and start receiving messages */
				Log.d(TAG, "Trying to connect to USB device");
				try {
					newReader = new Si470xGroupReader(TmcReceiver.this, usbDevice);
					isLive = true;
				} catch (UnavailableInputMethod e) {
					e.printStackTrace();
				}
			} else if (intent.getAction() == Const.ACTION_BT_CONNECTION_REQUESTED) {
				if (intent.getBooleanExtra(Const.EXTRA_KEEP_EXISTING_CONNECTION, false)
						&& ((btDevice != null) || (usbDevice != null))) {
					/* keep existing connection */
					Log.d(TAG, "Already connected, keeping existing connection");
					newReader = reader;
					return null;
				}
				/* user requested connection to a Bluetooth device */
				silent = intent.getBooleanExtra(Const.EXTRA_SILENT, false);
				BluetoothDevice newDevice = null;
				BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();
				Set<BluetoothDevice> devices = btAdapter.getBondedDevices();

				for (BluetoothDevice device : devices)
					if (device.getAddress().equalsIgnoreCase(intent.getStringExtra(Const.EXTRA_DEVICE)))
						newDevice = device;

				if (newDevice == null) {
					if (!silent)
						toastText = TmcReceiver.this.getString(R.string.err_no_device);
					else
						Log.w(TAG, "No device specified, aborting connection attempt");
				} else if (newDevice.equals(btDevice) && btSocket.isConnected()) {
					Log.w(TAG, "Ignoring attempt to connect to a device that is already connected");
					ignoreRequest = true;
					return null;
				} else {
					closeBluetoothDevice();
					closeUsbDevice();

					Log.d(TAG, "Trying to connect to Bluetooth device");
					try {
						btSocket = BluetoothManager.connect(newDevice, silent && !debugVerbose);
						btDevice = newDevice;
						InputStream in = btSocket.getInputStream();
						OutputStream out = btSocket.getOutputStream();
						newReader = new GnsGroupReader(in, out);
						isLive = true;
					} catch (IOException e) {
						if (debugVerbose || !silent)
							e.printStackTrace();
					} catch (UnavailableInputMethod e) {
						if (debugVerbose || !silent)
							e.printStackTrace();
					}

					if ((newReader == null) && !silent) {
						toastText = TmcReceiver.this.getString(R.string.err_device_unavailable);
						toastLength = Toast.LENGTH_LONG;
					}
				}
			} else if (intent.getAction() == Const.ACTION_CANNED_DATA_REQUESTED) {
				/* user requested injection of canned data */
				closeBluetoothDevice();
				closeUsbDevice();

				String cannedPath = intent.getStringExtra(Const.EXTRA_FILE);
				Log.d(TAG, "Processing canned data from " + cannedPath);
				File cannedFile = (cannedPath == null) ? null : new File(cannedPath);

				if ((cannedFile != null) && cannedFile.exists() && cannedFile.isFile()) {
					try {
						newReader = FileFormatGuesser.createReader(cannedFile);
					} catch (IOException e) {
						e.printStackTrace();
					}
				}

				if (newReader == null) {
					toastText = TmcReceiver.this.getString(R.string.err_invalid_file);
					toastLength = Toast.LENGTH_LONG;
				}

				isLive = false;
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			if (ignoreRequest) {
				/* 
				 * start request was ignored (e.g. because the requested device is already connected),
				 * resend broadcast and exit
				 */
				sendStatusBroadcast();
				return;
			}

			if ((newReader != null) && (newReader != reader)) {
				/* make service a foreground service so it won't get killed for memory */
				startForeground();

				SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(TmcReceiver.this);
				boolean debugInput = preferences.getBoolean(Const.PREF_DEBUG_INPUT, false);

				DateFormat fmt = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss", Locale.ROOT);
				fmt.setTimeZone(TimeZone.getTimeZone("UTC"));
				debugSessionName = fmt.format(new Date(System.currentTimeMillis()));
				File dumpDir = TmcReceiver.this.getExternalFilesDir(null);

				reader = newReader;
				if (isLive && debugInput) {
					try {
						teeReader = new TeeGroupReader(reader, new File(dumpDir, debugSessionName + Const.FILE_EXTENSION_RDS));
					} catch (IOException e) {
						e.printStackTrace();
						teeReader = reader;
					}
				} else
					teeReader = reader;
				DecoderShell.instance.process(teeReader, false);
				DecoderShell.instance.getLog().addNewMessageListener(messageVisitor);

				running = true;

				if ((reader instanceof TunerGroupReader) && (!scanner.isAlive()))
					scanner.start();
			}

			if (toastText != null)
				Toast.makeText(TmcReceiver.this, toastText, toastLength).show();

			if (newReader == null) {
				if (debugVerbose || !silent)
					Log.d(TAG, "Connection failed, stopping service");

				/* Something went wrong during initialization; stop the service */
				if (btSocket != null) {
					try {
						btSocket.close();
					} catch (Exception e) {
						if (debugVerbose || !silent)
							e.printStackTrace();
					}
					btSocket = null;
				}
				btDevice = null;

				stop();
			}

			cache.addListener(messageListener);

			sendStatusBroadcast();
		}
	}
}
