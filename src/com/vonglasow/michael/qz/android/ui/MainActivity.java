package com.vonglasow.michael.qz.android.ui;

import java.sql.SQLException;
import java.text.DateFormat;
import java.util.Collection;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

import com.vonglasow.michael.qz.android.core.QzApplication;
import com.vonglasow.michael.qz.android.core.TmcReceiver;
import com.vonglasow.michael.qz.android.util.Const;
import com.vonglasow.michael.qz.android.util.LtHelper;
import com.vonglasow.michael.qz.android.util.MessageHelper;
import com.vonglasow.michael.qz.android.util.MessageHelper.MessageClass;
import com.vonglasow.michael.qz.core.MessageCache;
import com.vonglasow.michael.qz.core.MessageWrapper;
import com.vonglasow.michael.qz.tuning.TmcService;
import com.vonglasow.michael.qz.util.MessageListener;
import com.vonglasow.michael.qz.R;

import eu.jacquet80.rds.app.oda.AlertC.Message;
import eu.jacquet80.rds.app.oda.tmc.LocationComparator;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.Manifest;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends ActionBarActivity {
	private static final String TAG = "MainActivity";

	/** The action bar for the activity */
	private ActionBar actionBar;

	/** An intent filter for Bluetooth status events */
	private IntentFilter btStatusFilter = new IntentFilter();

	/** The TMC message cache */
	private MessageCache cache = null;

	/** The menu item to establish a Bluetooth connection */
	private MenuItem connectBt = null;

	/** The menu item to close a Bluetooth connection */
	private MenuItem disconnectBt = null;

	/** The listener which fires when a list item is clicked */
	private OnItemClickListener itemClickListener;

	/** The adapter through which the list view retrieves messages */
	private MessageAdapter messageAdapter;

	/** The Comparator to sort messages in the list */
	private Comparator<Message> messageComparator = new DefaultComparator();

	/** A {@link MessageListener} which updates {@code messageAdapter} every time messages are added or deleted */
	private MessageListener messageListener;

	/** The UI element shown during startup */
	private ProgressBar startProgress;

	/** The UI element shown when there are no messages */
	private TextView emptyMessage;

	/** The UI element which displays the list of messages */
	private ListView messageView;

	/** A broadcast receiver to process device detach events */
	private BroadcastReceiver btStatusReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			if (intent != null) {
				if (intent.getAction().equals(Const.ACTION_BT_CONNECTED)) {
					setBtState(true);
				} else if (intent.getAction().equals(Const.ACTION_BT_DISCONNECTED)) {
					setBtState(false);
				}
				if (intent.hasExtra(Const.EXTRA_RSSI)) {
					int rssi = intent.getIntExtra(Const.EXTRA_RSSI, Const.RSSI_NOT_CONNECTED - 1);
					if (rssi < Const.RSSI_NOT_CONNECTED) {
						/* invalid (or not supplied), ignore and leave icon unchanged */
					} else if (rssi == Const.RSSI_NOT_CONNECTED) {
						actionBar.setIcon(R.drawable.ic_signal_radio_null);
						actionBar.setSubtitle(null);
						getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
					} else {
						if (rssi < 0x2000)
							actionBar.setIcon(R.drawable.ic_signal_radio_0_bar);
						else if (rssi < 0x6000)
							actionBar.setIcon(R.drawable.ic_signal_radio_1_bar);
						else if (rssi < 0xa000)
							actionBar.setIcon(R.drawable.ic_signal_radio_2_bar);
						else if (rssi < 0xe000)
							actionBar.setIcon(R.drawable.ic_signal_radio_3_bar);
						else
							actionBar.setIcon(R.drawable.ic_signal_radio_4_bar);
						if (intent.hasExtra(Const.EXTRA_FREQ)) {
							if (intent.hasExtra(Const.EXTRA_PROVIDER_NAME))
								actionBar.setSubtitle(String.format(getString(R.string.notification_text_name),
										intent.getIntExtra(Const.EXTRA_FREQ, 0) / 1000.0f,
										intent.getStringExtra(Const.EXTRA_PROVIDER_NAME).trim()));
							else
								actionBar.setSubtitle(String.format(getString(R.string.notification_text_noname),
										intent.getIntExtra(Const.EXTRA_FREQ, 0) / 1000.0f));
						} else
							actionBar.setSubtitle(null);
						getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
					}
				}
			}
		}

	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		startProgress = (ProgressBar) findViewById(R.id.startProgress);
		emptyMessage = (TextView) findViewById(R.id.emptyMessage);
		messageView = (ListView) this.findViewById(R.id.messageView);

		messageView.setVisibility(View.GONE);
		emptyMessage.setVisibility(View.GONE);
		startProgress.setVisibility(View.VISIBLE);

		actionBar = this.getSupportActionBar();

		StartupTask startupTask = new StartupTask();
		startupTask.execute();

		btStatusFilter.addAction(Const.ACTION_BT_CONNECTED);
		btStatusFilter.addAction(Const.ACTION_BT_DISCONNECTED);

		if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION)
				!= PackageManager.PERMISSION_GRANTED)
			ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
					Const.PERM_REQUEST_LOCATION_MAIN_RESUME);
	}

	@Override
	protected void onDestroy() {
		if (cache != null)
			cache.removeListener(messageListener);
		super.onDestroy();
	}

	@Override
	protected void onStop() {
		this.unregisterReceiver(btStatusReceiver);
		super.onPause();
	}

	@Override
	protected void onStart() {
		super.onResume();
		actionBar.setIcon(R.drawable.ic_signal_radio_null);
		actionBar.setSubtitle(null);
		actionBar.setDisplayShowHomeEnabled(true);

		this.registerReceiver(btStatusReceiver, btStatusFilter);
		Intent outIntent = new Intent(Const.ACTION_BT_STATUS_REQUEST);

		boolean isServiceRunning = false;

		ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
		for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE))
			if (TmcReceiver.class.getName().equals(service.service.getClassName()))
				isServiceRunning = true;

		if (isServiceRunning)
			sendBroadcast(outIntent);
		else {
			setBtState(false);

			if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION)
					== PackageManager.PERMISSION_GRANTED) {
				ResumeTask resumeTask = new ResumeTask();
				resumeTask.execute();
			}
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		connectBt = menu.findItem(R.id.action_connect_bt);
		disconnectBt = menu.findItem(R.id.action_disconnect_bt);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			startActivity(new Intent(this, SettingsActivity.class));
			return true;
		} else if (id == R.id.action_connect_bt) {
			if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
					== PackageManager.PERMISSION_GRANTED)
				connectBluetooth(true, false);
			else
				ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
						Const.PERM_REQUEST_LOCATION_MAIN_EXPLICIT);
			return true;
		} else if (id == R.id.action_disconnect_bt) {
			Intent outIntent = new Intent(Const.ACTION_BT_DISCONNECT_REQUESTED);

			// Send broadcast
			sendBroadcast(outIntent);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
		if (((requestCode == Const.PERM_REQUEST_LOCATION_MAIN_EXPLICIT)
				|| (requestCode == Const.PERM_REQUEST_LOCATION_MAIN_RESUME))
				&& (grantResults.length > 0))
			if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
				SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
				boolean autoConnect = preferences.getBoolean(Const.PREF_BLUETOOTH_AUTO, false);

				if (autoConnect || (requestCode == Const.PERM_REQUEST_LOCATION_MAIN_EXPLICIT))
					/* verbose if an explicit connection request was made, keep existing connection if resuming */
					connectBluetooth(requestCode == Const.PERM_REQUEST_LOCATION_MAIN_EXPLICIT,
					requestCode == Const.PERM_REQUEST_LOCATION_MAIN_RESUME);
				((QzApplication)(this.getApplicationContext())).runMediaScanner();
			} else {
				String message = getString(R.string.err_perm_location);
				Toast.makeText(this, message, Toast.LENGTH_LONG).show();
				Log.w(TAG, "ACCESS_COARSE_LOCATION permission not granted, aborting connection attempt.");
			}
	}

	/**
	 * @brief Attempts to connect to the configured Bluetooth device.
	 * 
	 * @param verbose Whether a toast notification should be displayed if no device is configured
	 * @param keepExisting Whether existing connections should be kept alive
	 */
	private void connectBluetooth(boolean verbose, boolean keepExisting) {
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
		String device = preferences.getString(Const.PREF_BLUETOOTH_DEV, null);

		if (device == null) {
			if (verbose)
				Toast.makeText(this, this.getString(R.string.err_no_device), Toast.LENGTH_SHORT).show();
			return;
		}

		// Create a new intent and put the BT device in as an extra
		Intent outIntent = new Intent(Const.ACTION_BT_CONNECTION_REQUESTED, null, this, TmcReceiver.class);
		outIntent.putExtra(Const.EXTRA_DEVICE, device);
		if (!verbose)
			outIntent.putExtra(Const.EXTRA_SILENT, true);
		if (keepExisting)
			outIntent.putExtra(Const.EXTRA_KEEP_EXISTING_CONNECTION, true);

		// Send intent to service
		startService(outIntent);
	}

	/**
	 * @brief Sets visibility of Bluetooth menu items to reflect the Bluetooth connection state.
	 * 
	 * @param connected Whether we are currently connected to a Bluetooth tuner device
	 */
	private void setBtState(boolean connected) {
		if (connectBt != null)
			connectBt.setVisible(!connected);
		if (disconnectBt != null)
			disconnectBt.setVisible(connected);
	}

	/**
	 * @brief The default comparator for sorting TMC messages.
	 * 
	 * This comparator compares events, using the following items of information in the order
	 * shown, until a difference is found:
	 * <ol>
	 * <li>Road class</li>
	 * <li>Road numbers</li>
	 * <li>if road numbers are present and equal, sequence of locations on the road
	 * <li>Area names</li>
	 * <li>Location IDs</li>
	 * </ol>
	 * 
	 * Road numbers and area names are sorted lexicographically. Null values are placed at the end,
	 * two null values are considered equal (causing the next items in the above list to be
	 * examined). Location IDs and extents are sorted numerically. 
	 */
	public static class DefaultComparator implements Comparator<Message> {
		private NumberStringComparator nsc = new NumberStringComparator();

		/** Location comparator for negative direction */
		private LocationComparator nlc = new LocationComparator(true);

		/** Location comparator for positive direction */
		private LocationComparator plc = new LocationComparator(false);

		/**
		 * @return -1 if {@code lhs} appears first as per the sort order, 1 if {@code rhs} appears
		 * first, 0 if no order can be determined 
		 */
		@Override
		public int compare(Message lhs, Message rhs) {
			int res = 0;
			/* compare by road class */
			res = MessageWrapper.getRoadClass(lhs).compareTo(MessageWrapper.getRoadClass(rhs));
			if (res != 0)
				return res;

			/* directionality and direction */
			int ldir = lhs.isBidirectional ? 0 : 1 - 2 * lhs.direction;
			int rdir = rhs.isBidirectional ? 0 : 1 - 2 * rhs.direction;

			/* direction, used to sort two candidates affecting the same road in the same direction */
			int dir = (ldir > 0) ? -1 : 1;

			/* compare by road numbers (if only one location has a road number, it is first) */
			String lrn = lhs.getRoadNumber();
			String rrn = rhs.getRoadNumber();
			if ((lrn != null) && (rrn != null)) {
				res = nsc.compare(lrn, rrn);
				if (res != 0)
					return res;

				/* same road number, first compare by directionality and direction */
				res = ldir - rdir;
				if (res != 0)
					return res;

				/* compare upstream locations (secondary if present, primary otherwise) */
				res = ((dir < 0) ? nlc : plc).compare(lhs.getSecondaryLocation(), rhs.getSecondaryLocation());
				if (res != 0)
					return res;

				/* compare downstream (primary) locations) */
				res = ((dir < 0) ? nlc : plc).compare(lhs.location, rhs.location);
				if (res != 0)
					return res;

				/* no result (which may happen for non-contiguous roads), try junction numbers */
				int jndir = 0;
				if ((lhs.getPrimaryJunctionNumber() != null) && (lhs.getSecondaryJunctionNumber() != null))
					jndir = nsc.compare(lhs.getPrimaryJunctionNumber(), lhs.getSecondaryJunctionNumber());
				if ((rhs.getPrimaryJunctionNumber() != null) && (rhs.getSecondaryJunctionNumber() != null)) {
					int rjndir = nsc.compare(rhs.getPrimaryJunctionNumber(), rhs.getSecondaryJunctionNumber());
					if (rjndir != 0)
						if ((jndir != 0) && (rjndir != jndir))
							jndir = 0;
						else
							jndir = rjndir;
				}
				if (jndir != 0) {
					String ljn = (lhs.getSecondaryJunctionNumber() != null) ? lhs.getSecondaryJunctionNumber() : lhs.getPrimaryJunctionNumber();
					String rjn = (rhs.getSecondaryJunctionNumber() != null) ? rhs.getSecondaryJunctionNumber() : rhs.getPrimaryJunctionNumber();
					if ((ljn != null) && (rjn != null)) {
						res = nsc.compare(ljn, rjn) * jndir;
						if (res != 0)
							return res;
						ljn = (lhs.getPrimaryJunctionNumber() != null) ? lhs.getPrimaryJunctionNumber() : lhs.getSecondaryJunctionNumber();
						rjn = (rhs.getPrimaryJunctionNumber() != null) ? rhs.getPrimaryJunctionNumber() : rhs.getSecondaryJunctionNumber();
						res = nsc.compare(ljn, rjn) * jndir;
						if (res != 0)
							return res;
					}
				}
			} else if (lrn != null)
				return -1;
			else if (rrn != null)
				return 1;

			/* compare by area names (if only one location has an area name, it is first) */
			String lan = lhs.getAreaName();
			String ran = rhs.getAreaName();
			if ((lan != null) && (ran != null)) {
				res = lan.compareTo(ran);
				if (res != 0)
					return res;
			} else if (lan != null)
				return -1;
			else if (ran != null)
				return 1;

			/* compare by directionality and direction */
			res = ldir - rdir;
			if (res != 0)
				return res;

			/* compare by primary location codes */
			return lhs.lcid - rhs.lcid;
		}
	}

	/**
	 * @brief A comparator for strings containing numbers.
	 *
	 * To compare two strings, each string is broken down into numeric and non-numeric parts.
	 * Then both string are compared part by part. The following rules apply:
	 * <ul>
	 * <li>Whitespace bordering on a number is discarded</li>
	 * <li>Whitespace surrounded by string characters is treated as one string with the surrounding
	 * characters</li>
	 * <li>If one string has more parts than the other, the shorter string is padded with null
	 * parts.</li>
	 * <li>null is less than non-null.</li>
	 * <li>null equals null.</li>
	 * <li>A numeric part is less than a string part.</li>
	 * <li>Numeric parts are compared as integers.</li>
	 * <li>String parts are compared as strings.</li>
	 * </ul>
	 * 
	 * If one of the strings supplied is null or empty (but not the other), it is considered to be
	 * greater than the other, causing it to be inserted at the end.
	 */
	public static class NumberStringComparator implements Comparator<String> {
		private enum State { WHITESPACE, NUMERIC, ALPHA };

		/**
		 * @return -1 if {@code lhs} appears first as per the sort order, 1 if {@code rhs} appears
		 * first, 0 if no order can be determined 
		 */
		@Override
		public int compare(String lhs, String rhs) {
			int res = 0;
			if ((lhs == null) || lhs.isEmpty()) {
				if ((rhs == null) || rhs.isEmpty())
					return 0;
				else
					return 1;
			} else if ((rhs == null) || rhs.isEmpty())
				return -1;

			int i;
			List<Object> l = parse(lhs);
			List<Object> r = parse(rhs);
			for (i = 0; i < Math.min(l.size(), r.size()); i++) {
				if (l.get(i) instanceof Integer) {
					if (r.get(i) instanceof Integer)
						res = (Integer) l.get(i) - (Integer) r.get(i);
					else if (r.get(i) instanceof String)
						return -1;
				} else if (l.get(i) instanceof String) {
					if (r.get(i) instanceof Integer)
						return 1;
					else if (r.get(i) instanceof String)
						res = ((String) l.get(i)).compareTo((String) r.get(i));
				}
				if (res != 0)
					return res;
			}

			if (i < l.size())
				return 1;
			else if (i < r.size())
				return -1;
			else
				return 0;
		}

		/**
		 * @brief Parses a string into numeric and non-numeric components.
		 * @param string The string to parse
		 * @return A list of {@link String} and {@link Integer} objects.
		 */
		private List<Object> parse(String string) {
			List<Object> res = new LinkedList<Object>();
			State state = State.WHITESPACE;
			int i = 0;

			while (i < string.length()) {
				char c = string.charAt(i);
				if (c <= 0x20) {
					/* whitespace */
					if (state == State.NUMERIC) {
						res.add(Integer.valueOf(string.substring(0, i)));
						string = string.substring(i);
						i = 1;
						state = State.WHITESPACE;
					} else
						i++;
				} else if ((c >= '0') && (c <= '9')) {
					/* numeric */
					if (state == State.ALPHA) {
						res.add(string.substring(0, i).trim());
						string = string.substring(i);
						i = 1;
					} else
						i++;
					state = State.NUMERIC;
				} else {
					/* alpha */
					if (state == State.NUMERIC) {
						res.add(Integer.valueOf(string.substring(0, i)));
						string = string.substring(i);
						i = 1;
					} else
						i++;
					state = State.ALPHA;
				}
			}

			if (string.length() > 0) {
				if (state == State.NUMERIC)
					res.add(Integer.valueOf(string));
				else if (state == State.ALPHA)
					res.add(string.trim());
			}

			return res;
		}
	}

	/**
	 * @brief An adapter to display TMC messages in a ListView. 
	 */
	private class MessageAdapter extends ArrayAdapter<Message> {
		private Message selection = null;
		
		private MessageAdapter(Context context, int resource,
				int textViewResourceId) {
			super(context, resource, textViewResourceId);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View view = super.getView(position, convertView, parent);
			TextView roadRef = (TextView) view.findViewById(R.id.roadRef);
			ImageView icon = (ImageView) view.findViewById(R.id.icon);
			TextView roadName = (TextView) view.findViewById(R.id.roadName);
			TextView locationName = (TextView) view.findViewById(R.id.locationName);
			TextView eventDescription = (TextView) view.findViewById(R.id.eventDescription);
			TextView source = (TextView) view.findViewById(R.id.source);
			TextView updated = (TextView) view.findViewById(R.id.updated);
			TextView updates = (TextView) view.findViewById(R.id.updates);

			Message message = getItem(position);

			if (message == selection)
				view.setBackgroundColor(MainActivity.this.getResources().getColor(R.color.ripple_material_dark));
			else
				view.setBackgroundColor(MainActivity.this.getResources().getColor(R.color.background_material_dark));

			/* roadRef: Road number */
			String roadRefText = message.getRoadNumber();
			if (roadRefText != null) {
				roadRef.setVisibility(View.VISIBLE);
				roadRef.setText(roadRefText);
			} else
				roadRef.setVisibility(View.INVISIBLE);
			MessageWrapper.RoadClass rclass = MessageWrapper.getRoadClass(message);
			switch (rclass) {
			case MOTORWAY:
				roadRef.setBackgroundColor(getContext().getResources().getColor(R.color.motorway));
				roadRef.setTextColor(getContext().getResources().getColor(android.R.color.primary_text_dark));
				break;
			case TRUNK:
				roadRef.setBackgroundColor(getContext().getResources().getColor(R.color.trunk));
				roadRef.setTextColor(getContext().getResources().getColor(android.R.color.primary_text_dark));
				break;
			case PRIMARY:
				roadRef.setBackgroundColor(getContext().getResources().getColor(R.color.primary));
				roadRef.setTextColor(getContext().getResources().getColor(android.R.color.primary_text_dark));
				break;
			case SECONDARY:
				roadRef.setBackgroundColor(getContext().getResources().getColor(R.color.secondary));
				roadRef.setTextColor(getContext().getResources().getColor(android.R.color.primary_text_light));
				break;
			case TERTIARY:
				roadRef.setBackgroundColor(getContext().getResources().getColor(R.color.tertiary));
				roadRef.setTextColor(getContext().getResources().getColor(android.R.color.primary_text_light));
				break;
			default:
				/* use default color */
				roadRef.setBackgroundColor(getContext().getResources().getColor(R.color.unclassified));
				roadRef.setTextColor(getContext().getResources().getColor(android.R.color.primary_text_light));
			}

			/* icon: Icon */
			MessageClass mclass = MessageHelper.getMessageClass(message);
			switch (mclass) {
			case SECURITY:
				icon.setImageResource(R.drawable.ic_security);
				break;
			case WARNING_ACCIDENT:
				icon.setImageResource(R.drawable.ic_warning_accident);
				break;
			case WARNING_SLIPPERY:
				icon.setImageResource(R.drawable.ic_warning_slippery);
				break;
			case WARNING_WIND:
				icon.setImageResource(R.drawable.ic_warning_wind);
				break;
			case WARNING_ROADWORKS:
				icon.setImageResource(R.drawable.ic_warning_roadworks);
				break;
			case WARNING_DANGER:
				icon.setImageResource(R.drawable.ic_warning_danger);
				break;
			case WARNING_DELAY:
				icon.setImageResource(R.drawable.ic_warning_delay);
				break;
			case WARNING_QUEUE:
				icon.setImageResource(R.drawable.ic_warning_queue);
				break;
			case RESTRICTION_CLOSED:
				icon.setImageResource(R.drawable.ic_restriction_closed);
				break;
			case RESTRICTION_SIZE:
				icon.setImageResource(R.drawable.ic_restriction_size);
				break;
			case INFO_CANCELLATION:
				icon.setImageResource(R.drawable.ic_info_cancellation);
				break;
			case INFO_TEMPERATURE:
				icon.setImageResource(R.drawable.ic_info_temperature);
				break;
			case INFO_VISIBILITY:
				icon.setImageResource(R.drawable.ic_info_visibility);
				break;
			case INFO_EVENT:
				icon.setImageResource(R.drawable.ic_info_event);
				break;
			case INFO_TIME:
				icon.setImageResource(R.drawable.ic_info_time);
				break;
			case INFO_CARPOOL:
				icon.setImageResource(R.drawable.ic_info_carpool);
				break;
			case INFO_PARKING:
				icon.setImageResource(R.drawable.ic_info_parking);
				break;
			default:
				icon.setImageResource(R.drawable.ic_info_info);
			}

			/* roadName: Road or location name */
			roadName.setText(message.getDisplayName());

			/* locationName: Detailed location ("at place", "between place1 and place2") */
			String locationNameText = MessageHelper.getDetailedLocationName(message);
			if (locationNameText != null) {
				locationName.setVisibility(View.VISIBLE);
				locationName.setText(locationNameText);
			} else {
				locationName.setVisibility(View.GONE);
			}

			/* eventDescription */
			eventDescription.setText(MessageHelper.getEventText(getContext(), message));

			/* source */
			TmcService service = new TmcService(message.cc, message.getLocationTableNumber(), message.getSid());
			service = cache.getServiceName(service);
			String serviceName = null;
			if (service != null && service.name != null)
				serviceName = service.name;
			else
				serviceName = String.format("[%01X/%d/%d]", message.cc, message.getLocationTableNumber(), message.getSid());
			source.setText(serviceName);

			/* updated */
			DateFormat format = DateFormat.getTimeInstance(DateFormat.SHORT);
			updated.setText(format.format(message.getTimestamp()));

			/* updates */
			updates.setText(getResources().getQuantityString(R.plurals.updates,
					message.getUpdateCount(), message.getUpdateCount()));

			return view;
		}

		/**
		 * @brief Returns the selected message
		 * 
		 * @return The selected message, or null if none is selected
		 */
		protected Message getSelection() {
			return selection;
		}

		/**
		 * @brief Sets the selected message, unselecting any previously selected message.
		 * 
		 * Callers need to call {@link ArrayAdapter#notifyDataSetChanged()} separately after this
		 * method in order to ensure the update is displayed correctly in the associated list view.
		 * 
		 * @param message The message to select, or null to clear the selection
		 */
		protected void setSelection(Message message) {
			selection = message;
		}
	}

	private class ResumeTask extends AsyncTask<Void, Void, Void> {
		@Override
		protected Void doInBackground(Void... params) {
			/* NOP */
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
			boolean autoConnect = preferences.getBoolean(Const.PREF_BLUETOOTH_AUTO, false);

			if (autoConnect)
				connectBluetooth(false, true);
			((QzApplication)(MainActivity.this.getApplicationContext())).runMediaScanner();
		}
	}

	private class StartupTask extends AsyncTask<Void, Void, Void> {
		@Override
		protected Void doInBackground(Void... params) {
			LtHelper.getInstance(MainActivity.this);

			MessageCache.setDatabaseUrl(Const.getCacheDbUrl(MainActivity.this));
			try {
				cache = MessageCache.getInstance();
			} catch (ClassNotFoundException | SQLException e) {
				e.printStackTrace();
				throw new IllegalStateException(e);
			}

			/* message adapter */
			messageAdapter = new MessageAdapter(MainActivity.this, R.layout.view_message_item, R.id.roadName);

			/* message listener */
			messageListener = new MessageListener() {
				@Override
				public void onUpdateReceived(final MessageWrapper added, final Collection<MessageWrapper> removed) {
					runOnUiThread(new Runnable() {
						@Override
						public void run() {
							Message selection = messageAdapter.getSelection();

							/* begin "transaction", i.e. inhibit updates */
							messageAdapter.setNotifyOnChange(false);

							/*
							 * Important: In order to preserve the selection, we need to add the
							 * new message before we remove any old ones.
							 */
							if ((added != null) && (!added.message.isCancellation())
									&& (messageAdapter.getPosition(added.message) < 0)) {
								messageAdapter.add(added.message);
								if ((selection != null) && (added.message.overrides(selection))) {
									messageAdapter.setSelection(added.message);
									selection = added.message;
								}
							}
							for (MessageWrapper wrapper: removed) {
								if (wrapper.message == selection) {
									messageAdapter.setSelection(null);
									selection = null;
								}
								messageAdapter.remove(wrapper.message);
							}

							if (messageAdapter.getCount() >= 1) {
								emptyMessage.setVisibility(View.GONE);
								messageView.setVisibility(View.VISIBLE);
								MainActivity.this.setTitle(String.format(getString(R.string.app_name_n),
										messageAdapter.getCount()));
								if (messageAdapter.getCount() > 1)
									messageAdapter.sort(messageComparator);
							} else {
								emptyMessage.setVisibility(View.VISIBLE);
								messageView.setVisibility(View.GONE);
								MainActivity.this.setTitle(getString(R.string.app_name));
							}

							/* "commit transaction", triggering an update */
							messageAdapter.notifyDataSetChanged();

							/* If a message is selected, keep it in view */
							if (selection != null) {
								int pos = messageAdapter.getPosition(selection);
								messageView.smoothScrollToPosition(pos);
							}
						}
					});
				}
			};

			/* on click listener */
			itemClickListener = new OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {
					/* Select or unselect a message */
					Message message = messageAdapter.getItem(position);
					if (message == messageAdapter.getSelection())
						messageAdapter.setSelection(null);
					else
						messageAdapter.setSelection(message);
					messageAdapter.notifyDataSetChanged();
				}
			};

			cache.addListener(messageListener);

			for (MessageWrapper wrapper : cache.getMessages())
				if (!wrapper.message.isCancellation())
					messageAdapter.add(wrapper.message);
			if (messageAdapter.getCount() > 1)
				messageAdapter.sort(messageComparator);

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			messageView.setAdapter(messageAdapter);
			messageView.setOnItemClickListener(itemClickListener);

			startProgress.setVisibility(View.GONE);
			if (messageAdapter.getCount() >= 1) {
				emptyMessage.setVisibility(View.GONE);
				messageView.setVisibility(View.VISIBLE);
				MainActivity.this.setTitle(String.format(getString(R.string.app_name_n),
						messageAdapter.getCount()));
			} else {
				emptyMessage.setVisibility(View.VISIBLE);
				messageView.setVisibility(View.GONE);
			}
		}
	}
}
