/*
 * Copyright © 2017 Michael von Glasow.
 * 
 * This file is part of Qz.
 *
 * Qz is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Qz is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Qz.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.vonglasow.michael.qz.android.ui;

import com.vonglasow.michael.qz.R;
import com.vonglasow.michael.qz.android.core.TmcReceiver;
import com.vonglasow.michael.qz.android.util.Const;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.preference.Preference;
import android.support.v7.preference.Preference.OnPreferenceClickListener;
import android.support.v7.preference.PreferenceCategory;
import android.support.v7.preference.PreferenceFragmentCompat;
import android.support.v7.preference.PreferenceManager;
import android.view.MenuItem;
import ua.com.vassiliev.androidfilebrowser.FileBrowserActivity;

public class SettingsActivity extends AppCompatActivity implements OnPreferenceClickListener {
	public static final String TAG = SettingsActivity.class.getSimpleName();

	public static final int REQUEST_CODE_PICK_RDS = 1;

	PreferenceCategory prefCatDebug;
	Preference prefDebugCanned;
	Preference prefDebugInput;
	Preference prefDebugMirror;
	Preference prefDebugOutput;
	Preference prefDebugShow;
	Preference prefDebugVerbose;

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == REQUEST_CODE_PICK_RDS) {
			if (resultCode == RESULT_OK) {
				// Create a new intent and put the file path in as an extra
				Intent outIntent = new Intent(Const.ACTION_CANNED_DATA_REQUESTED, null, this, TmcReceiver.class);
				outIntent.putExtra(Const.EXTRA_FILE, data.getStringExtra(FileBrowserActivity.returnFileParameter));

				// Broadcast this event so we can receive it
				startService(outIntent);
			}
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			onBackPressed();
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	@Override
	protected
	void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_settings);

		// Show the Up button in the action bar.
		ActionBar actionBar = getSupportActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);

		// Display the fragment as the main content.
		getSupportFragmentManager().beginTransaction()
		.replace(android.R.id.content, new SettingsFragment())
		.commit();

		PreferenceManager.getDefaultSharedPreferences(this);
	}

	@Override
	public boolean onPreferenceClick(Preference preference) {
		if (preference == prefDebugCanned) {
			Intent intent = new Intent(FileBrowserActivity.INTENT_ACTION_SELECT_FILE, null, this, FileBrowserActivity.class);
			intent.putExtra(FileBrowserActivity.startDirectoryParameter, getExternalFilesDir(null).getAbsolutePath());
			startActivityForResult(intent, REQUEST_CODE_PICK_RDS);
			return true;
		} else if (preference == prefDebugShow) {
			Editor editor = PreferenceManager.getDefaultSharedPreferences(this).edit();
			editor.putBoolean(Const.PREF_DEBUG_SHOW, true);
			editor.apply();
			refreshDebugCategory();
			return true;
		} else
			return false;
	}

	@Override
	protected void onStart() {
		super.onStart();

		SettingsFragment sf = (SettingsFragment) getSupportFragmentManager().findFragmentById(android.R.id.content);
		prefCatDebug = (PreferenceCategory) sf.findPreference(Const.PREF_CAT_DEBUG);
		prefDebugCanned = sf.findPreference(Const.PREF_DEBUG_CANNED);
		prefDebugCanned.setOnPreferenceClickListener(this);
		prefDebugInput = sf.findPreference(Const.PREF_DEBUG_INPUT);
		prefDebugMirror = sf.findPreference(Const.PREF_DEBUG_MIRROR);
		prefDebugOutput = sf.findPreference(Const.PREF_DEBUG_OUTPUT);
		prefDebugShow = sf.findPreference(Const.PREF_DEBUG_SHOW);
		prefDebugShow.setOnPreferenceClickListener(this);
		prefDebugVerbose = sf.findPreference(Const.PREF_DEBUG_VERBOSE);

		refreshDebugCategory();
	}

	/**
	 * @brief Shows or hides debug options depending on preference.
	 */
	private void refreshDebugCategory() {
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
		boolean debugShow = preferences.getBoolean(Const.PREF_DEBUG_SHOW, false);
		/*
		 * Due to an apparent bug in preference-v7, only set visibility if it has actually changed.
		 * Setting visibility to true on a visible item will cause it to show up twice.
		 * Setting visibility to false on a hidden item will result in an exception.
		 */
		if (debugShow) {
			if (prefDebugShow.isVisible())
				prefDebugShow.setVisible(false);
			if (!prefDebugInput.isVisible())
				prefDebugInput.setVisible(true);
			if (!prefDebugOutput.isVisible())
				prefDebugOutput.setVisible(true);
			if (!prefDebugVerbose.isVisible())
				prefDebugVerbose.setVisible(true);
			if (!prefDebugMirror.isVisible())
				prefDebugMirror.setVisible(true);
			if (!prefDebugCanned.isVisible())
				prefDebugCanned.setVisible(true);
		} else {
			if (!prefDebugShow.isVisible())
				prefDebugShow.setVisible(true);
			if (prefDebugInput.isVisible())
				prefDebugInput.setVisible(false);
			if (prefDebugOutput.isVisible())
				prefDebugOutput.setVisible(false);
			if (prefDebugVerbose.isVisible())
				prefDebugVerbose.setVisible(false);
			if (prefDebugMirror.isVisible())
				prefDebugMirror.setVisible(false);
			if (prefDebugCanned.isVisible())
				prefDebugCanned.setVisible(false);
		}
	}

	public static class SettingsFragment extends PreferenceFragmentCompat {
		@Override
		public void onCreatePreferences(Bundle savedInstanceState, String arg1) {
			//super.onCreatePreferences(savedInstanceState, arg1);

			// Load the preferences from an XML resource
			addPreferencesFromResource(R.xml.preferences);
		}

	}
}
