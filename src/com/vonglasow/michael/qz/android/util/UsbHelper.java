/*
 * Copyright © 2017–2018 Michael von Glasow.
 * 
 * This file is part of Qz.
 *
 * Qz is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Qz is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Qz.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.vonglasow.michael.qz.android.util;

import com.vonglasow.michael.qz.R;
import com.vonglasow.michael.qz.android.core.TmcReceiver;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.usb.UsbManager;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

/**
 * Helper class to interact with USB tuner devices.
 * 
 * Because USB attach intents are delivered only to activities but not to services or broadcast
 * receivers, this class provides a transparent activity whose only purpose it is to listen to USB
 * attach intents and launch the TMC receiver service upon receiving one.
 */
public class UsbHelper extends AppCompatActivity {
	private static final String TAG = "UsbHelper";

	private Intent intent;

	@Override
	public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
		if ((requestCode == Const.PERM_REQUEST_LOCATION_USB) && (grantResults.length > 0))
			if (grantResults[0] == PackageManager.PERMISSION_GRANTED)
				connectUsb();
			else {
				String message = getString(R.string.err_perm_location);
				Toast.makeText(this, message, Toast.LENGTH_LONG).show();
				Log.w(TAG, "ACCESS_COARSE_LOCATION permission not granted, aborting.");
				finish();
			}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	protected void onResume() {
		super.onResume();

		intent = getIntent();
		if (intent != null) {
			if (intent.getAction().equals(UsbManager.ACTION_USB_DEVICE_ATTACHED)) {
				Log.d(TAG, "onResume() for UsbManager.ACTION_USB_DEVICE_ATTACHED");

				// Make sure we have location permission (to answer requests)
				if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
						== PackageManager.PERMISSION_GRANTED)
					connectUsb();
				else
					ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
							Const.PERM_REQUEST_LOCATION_USB);
			}
		}
	}

	private void connectUsb() {
		Parcelable usbDevice = intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);

		// Create a new intent and put the usb device in as an extra
		Intent outIntent = new Intent(Const.ACTION_USB_DEVICE_ATTACHED, null, this, TmcReceiver.class);
		outIntent.putExtra(UsbManager.EXTRA_DEVICE, usbDevice);

		// Broadcast this event so we can receive it
		startService(outIntent);

		// Close the activity
		finish();
	}
}
