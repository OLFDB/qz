/*
 * Copyright © 2017 Michael von Glasow.
 * 
 * This file is part of Qz.
 *
 * Qz is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Qz is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Qz.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.vonglasow.michael.qz.android.util;

import java.io.File;

import eu.jacquet80.rds.app.oda.tmc.TMC;
import android.content.Context;
import android.content.ContextWrapper;

/**
 * Constants used throughout the application
 */
public class Const {

	/**
	 * Internal intent to indicate a Bluetooth device is connected.
	 * 
	 * This intent is sent when the connection is established but can be resent at any time. 
	 */
	public static final String ACTION_BT_CONNECTED = "com.vonglasow.michael.qz.ACTION_BT_CONNECTED";

	/**
	 * Internal intent to indicate that the user has requested a connection to a Bluetooth device.
	 */
	public static final String ACTION_BT_CONNECTION_REQUESTED = "com.vonglasow.michael.qz.ACTION_BT_CONNECTION_REQUESTED";

	/**
	 * Internal intent to indicate that the user has requested to disconnect a Bluetooth device.
	 */
	public static final String ACTION_BT_DISCONNECT_REQUESTED = "com.vonglasow.michael.qz.ACTION_BT_DISCONNECT_REQUESTED";

	/**
	 * Internal intent to indicate a Bluetooth device was disconnected, or no Bluetooth device is currently connected.
	 * 
	 * This intent is sent when the connection is closed but can be resent at any time. 
	 */
	public static final String ACTION_BT_DISCONNECTED = "com.vonglasow.michael.qz.ACTION_BT_DISCONNECTED";

	/**
	 * Internal intent to indicate that the sender would like the Bluetooth connection status to be resent.
	 */
	public static final String ACTION_BT_STATUS_REQUEST = "com.vonglasow.michael.qz.ACTION_BT_STATUS_REQUEST";

	/**
	 * Internal intent to indicate that the end of canned RDS data has been reached.
	 */
	public static final String ACTION_CANNED_DATA_EOF = "com.vonglasow.michael.qz.ACTION_CANNED_DATA_EOF";

	/**
	 * Internal intent to indicate that the user has requested that a RDS file be processed.
	 */
	public static final String ACTION_CANNED_DATA_REQUESTED = "com.vonglasow.michael.qz.ACTION_CANNED_DATA_REQUESTED";

	/**
	 * Intent to notify other applications of new traffic messages
	 * 
	 * This intent will be sent as a broadcast. The feed is included as an extra ({@link #EXTRA_FEED}).
	 */
	public static final String ACTION_TRAFF_FEED = "org.traffxml.traff.FEED";

	/**
	 * Intent to request a feed of all currently active traffic messages
	 * 
	 * This intent is sent by other applications as a broadcast.
	 */
	public static final String ACTION_TRAFF_POLL = "org.traffxml.traff.POLL";

	/**
	 * Internal intent to indicate that a USB device has been attached.
	 */
	public static final String ACTION_USB_DEVICE_ATTACHED = "com.vonglasow.michael.qz.ACTION_USB_DEVICE_ATTACHED";

	/**
	 * Extra for {@link #ACTION_BT_CONNECTION_REQUESTED} specifying the device to connect to.
	 * 
	 * This is a String extra.
	 */
	public static final String EXTRA_DEVICE = "device";

	/** Extra for FM signal strength.
	 * 
	 * This is an integer extra, with values representing frequency in kHz.
	 */
	public static final String EXTRA_FREQ = "freq";

	/** Extra for TMC provider name.
	 * 
	 * This is a String extra.
	 */
	public static final String EXTRA_PROVIDER_NAME = "provider_name";

	/** Extra for FM signal strength.
	 * 
	 * This is an integer extra, with values ranging from 0 to 65535, or
	 * {@link #RSSI_NOT_CONNECTED} if no tuner is connected.
	 */
	public static final String EXTRA_RSSI = "rssi";

	/**
	 * Extra for the TraFF feed.
	 * 
	 * This is a String extra.
	 */
	public static final String EXTRA_FEED = "feed";

	/**
	 * Extra for {@link #ACTION_CANNED_DATA_REQUESTED} specifying the file to process.
	 * 
	 * This is a String extra.
	 */
	public static final String EXTRA_FILE = "file";

	/**
	 * Extra for {@link #ACTION_BT_CONNECTION_REQUESTED} specifying that existing connections, if any, should be kept
	 * alive rather than disconnecting them and trying to connect to a new device. This is used for some cases in which
	 * connections are initiated automatically, not through the GUI.
	 * 
	 * This is a boolean extra; default is false.
	 */
	public static final String EXTRA_KEEP_EXISTING_CONNECTION = "keep_existing_connection";

	/**
	 * Extra for {@link #ACTION_BT_CONNECTION_REQUESTED} specifying that no feedback should be
	 * given to the user if the connection succeeded or failed. This is used for connections which
	 * were initiated automatically, not through the GUI.
	 * 
	 * This is a boolean extra.
	 */
	public static final String EXTRA_SILENT = "silent";

	/**
	 * File extension for RDS dump files
	 */
	public static final String FILE_EXTENSION_RDS = ".rds";

	/**
	 * File extension for TraFF dump files
	 */
	public static final String FILE_EXTENSION_TRAFF = ".xml";

	/**
	 * Permission request for location access when a connection is explicitly made in MainActivity
	 */
	public static final int PERM_REQUEST_LOCATION_MAIN_EXPLICIT = 1;

	/**
	 * Permission request for location access when MainActivity is resumed
	 */
	public static final int PERM_REQUEST_LOCATION_MAIN_RESUME = 2;

	/**
	 * Permission request for location access by TmcReceiver
	 */
	public static final int PERM_REQUEST_LOCATION_RECEIVER = 3;

	/**
	 * Permission request for location access by UsbHelper
	 */
	public static final int PERM_REQUEST_LOCATION_USB = 4;

	/**
	 * Preference key for Bluetooth autoconnect setting.
	 */
	public static final String PREF_BLUETOOTH_AUTO = "pref_bluetooth_auto";

	/**
	 * Preference key for the Bluetooth device to connect to.
	 */
	public static final String PREF_BLUETOOTH_DEV = "pref_bluetooth_dev";

	/**
	 * Preference key for the debug category.
	 */
	public static final String PREF_CAT_DEBUG = "pref_cat_debug";

	/**
	 * Preference key to inject canned RDS data.
	 */
	public static final String PREF_DEBUG_CANNED = "pref_debug_canned";

	/**
	 * Preference key for input debug setting.
	 */
	public static final String PREF_DEBUG_INPUT = "pref_debug_input";

	/**
	 * Preference key for output debug setting.
	 */
	public static final String PREF_DEBUG_OUTPUT = "pref_debug_output";

	/**
	 * Preference key to display debug settings.
	 */
	public static final String PREF_DEBUG_SHOW = "pref_debug_show";

	/**
	 * Preference key for verbose logging.
	 */
	public static final String PREF_DEBUG_VERBOSE = "pref_debug_verbose";

	/**
	 * Preference key for log mirroring.
	 */
	public static final String PREF_DEBUG_MIRROR = "pref_debug_mirror";

	/**
	 * RSSI value indicating that no tuner device is currently connected.
	 */
	public static final int RSSI_NOT_CONNECTED = -1;

	/**
	 * @brief Returns the URL for the cache database.
	 * 
	 * @param context The calling context
	 * 
	 * @return A URL which can be passed directly to {@link MessageCache#setDatabaseUrl(String)}.
	 */
	public static String getCacheDbUrl(Context context) {
		ContextWrapper wrapper = new ContextWrapper(context);
		return "jdbc:hsqldb:file:" + (new File(wrapper.getFilesDir(), "messagecache")).getPath();
	}

	/**
	 * @brief Returns the URL for the location table database.
	 * 
	 * @param context The calling context
	 * 
	 * @return A URL which can be passed directly to {@link TMC#setDbUrl(String)}.
	 */
	public static String getLtDbUrl(Context context) {
		ContextWrapper wrapper = new ContextWrapper(context);
		return "jdbc:hsqldb:file:" + (new File(wrapper.getFilesDir(), "ltdb")).getPath();
	}

}
