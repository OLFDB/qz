/*
 Copyright (c) 2017 Michael von Glasow

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Lesser Public License for more details.

 You should have received a copy of the GNU Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

 */

package com.vonglasow.michael.andhid;

import java.io.IOException;

import android.content.Context;
import android.hardware.usb.UsbConstants;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbEndpoint;
import android.hardware.usb.UsbInterface;
import android.hardware.usb.UsbManager;

/**
 * Encapsulates a USB HID device with which applications can interact.
 * 
 * Methods are not guaranteed to be thread-safe (and in fact perform operations which are suspected
 * of not being thread-safe), therefore the caller must provide for proper synchronization where an
 * instance of this class is accessed by multiple threads.
 */
/*
 * TODO This is an ad-hoc implementation created out of a need to have an HID API for Android.
 * It lacks some primitives which were not needed by the application for which it was created.
 */
public class HidDevice {
	/** GET_REPORT request code */
	public static final int REQUEST_GET_REPORT = 0x01;
	/** SET_REPORT request code */
	public static final int REQUEST_SET_REPORT = 0x09;
	/** INPUT report type */
	public static final int REPORT_TYPE_INPUT = 0x0100;
	/** OUTPUT report type */
	public static final int REPORT_TYPE_OUTPUT = 0x0200;
	/** FEATURE report type */
	public static final int REPORT_TYPE_FEATURE = 0x0300;

	private Context context;
	private UsbManager manager;
	private UsbDevice device;
	private UsbInterface ifHid = null;
	private UsbEndpoint epIn = null;
	private UsbDeviceConnection connection;
	private boolean closed = false;

	/**
	 * @brief Opens a USB HID device.
	 * 
	 * This is a rough equivalent to hidapi's {@code hid_open()}.
	 * 
	 * The device is typically obtained in one of the following ways:
	 * <ul>
	 * <li>By receiving a {@code USB_DEVICE_ATTACHED} intent, indicating that a USB device has
	 * been attached and the user wishes your application to handle it</li>
	 * <li>By receiving a {@code ACTION_USB_PERMISSION} intent, indicating that the user has
	 * granted you the permission to interact with the device which you previously requested</li>
	 * <li>By calling {@link android.hardware.usb.UsbManager#getDeviceList()} to enumerate all
	 * USB devices currently present (note you may still need to request access permissions for
	 * a device obtained in this manner)</li>
	 * </ul>
	 * 
	 * Prior to calling this constructor, you must ensure that you have permission to open the
	 * device, else an exception will be thrown.
	 * 
	 * @param device The device
	 * 
	 * @throws IllegalArgumentException if the device is not a HID device or does not have an INTERRUPT IN endpoint
	 * @throws IOException if claiming the HID interface of the device fails 
	 */
	public HidDevice(Context context, UsbDevice device) throws IOException {
		this.context = context;
		this.device = device;

		for (int i = 0; (this.ifHid == null) && (i < device.getInterfaceCount()); i++)
			if (device.getInterface(i).getInterfaceClass() == UsbConstants.USB_CLASS_HID) {
				this.ifHid = device.getInterface(i);
				for (int j = 0; j < ifHid.getEndpointCount(); j++) {
					UsbEndpoint ep = ifHid.getEndpoint(j);
					if ((ep.getDirection() == UsbConstants.USB_DIR_IN) && (ep.getType() == UsbConstants.USB_ENDPOINT_XFER_INT))
						epIn = ep;
				}
			}
		if (this.ifHid == null)
			throw new IllegalArgumentException("Device has no HID interface");
		else if (this.epIn == null)
			throw new IllegalArgumentException("Device has no INTERRUPT IN endpoint (type USB_ENDPOINT_XFER_INT, direction USB_DIR_IN");

		this.manager = (UsbManager) context.getSystemService(Context.USB_SERVICE);
		this.connection = manager.openDevice(device);
		if (!connection.claimInterface(ifHid, true))
			throw new IOException("Failed to claim HID interface");
	}

	/**
	 * @brief Closes the USB HID device.
	 * 
	 * Closing a device releases all associated resources. After that the device cannot be used
	 * again, unless a new instance is obtained.
	 */
	public void close() {
		this.closed = true;
		this.connection.close();
	}

	/** 
	 * @brief Gets a feature report from a HID device.
	 * 
	 * Note that, unlike for other HID implementations, the report ID is supplied as a separate
	 * argument and it is not necessary to set it in {@code data}. However, when this method
	 * returns, the first element of {@code data} will contain the report ID.
	 * 
	 * @param reportId The report ID of the report to be read
	 * @param data A buffer to which report data will be written
	 * @param length The length of the buffer (can be longer than the actual report)
	 * 
	 * @return The number of bytes read, or -1 if an error occurred.
	 */
	// TODO use HIDAPI-style logic to supply report ID, drop length arg
	public int getFeatureReport(int reportId, byte[] data, int length) {
		if (closed)
			throw new IllegalStateException("this method cannot be called on a closed device");
		if ((reportId & 0xFF) != reportId)
			throw new IllegalArgumentException("reportId may only set the lowest 8 bits");
		return connection.controlTransfer(
				UsbConstants.USB_DIR_IN | UsbConstants.USB_TYPE_CLASS | UsbConstants.USB_INTERFACE_SUBCLASS_BOOT,
				REQUEST_GET_REPORT,
				reportId | REPORT_TYPE_OUTPUT,
				ifHid.getId(), data, length, 0);
	}

	/** @brief Read an Input report from a HID device.

	Input reports are returned
    to the host through the INTERRUPT IN endpoint. The first byte will
	contain the Report number if the device uses numbered reports.

	@ingroup API
	@param device A device handle returned from hid_open().
	@param data A buffer to put the read data into.
	@param length The number of bytes to read. For devices with
		multiple reports, make sure to read an extra byte for
		the report number.

	@returns
		This function returns the actual number of bytes read and
		-1 on error.
	 */
	public int read(byte[] data, int length) {
		if (closed)
			throw new IllegalStateException("this method cannot be called on a closed device");
		/* TODO timeout 0 is infinite (blocking),
		 * allow finite timeouts (or non-blocking calls by specifying timeout 1)
		 */
		return connection.bulkTransfer(epIn, data, length, 0);
	}

	/** @brief Send a Feature report to the device.

	Feature reports are sent over the Control endpoint as a
	Set_Report transfer.  The first byte of @p data[] must
	contain the Report ID. For devices which only support a
	single report, this must be set to 0x0. The remaining bytes
	contain the report data. Since the Report ID is mandatory,
	calls to hid_send_feature_report() will always contain one
	more byte than the report contains. For example, if a hid
	report is 16 bytes long, 17 bytes must be passed to
	hid_send_feature_report(): the Report ID (or 0x0, for
	devices which do not use numbered reports), followed by the
	report data (16 bytes). In this example, the length passed
	in would be 17.

	@ingroup API
	@param device A device handle returned from hid_open().
	@param data The data to send, including the report number as
		the first byte.
	@param length The length in bytes of the data to send, including
		the report number.

	@returns
		This function returns the actual number of bytes written and
		-1 on error.
	 */
	// TODO use HIDAPI-style logic to supply report ID, drop length arg
	public int sendFeatureReport(int reportId, byte[] data, int length) {
		if (closed)
			throw new IllegalStateException("this method cannot be called on a closed device");
		if ((reportId & 0xFF) != reportId)
			throw new IllegalArgumentException("reportId may only set the lowest 8 bits");
		return connection.controlTransfer(
				UsbConstants.USB_DIR_OUT | UsbConstants.USB_TYPE_CLASS | UsbConstants.USB_INTERFACE_SUBCLASS_BOOT,
				REQUEST_SET_REPORT,
				reportId | REPORT_TYPE_INPUT,
				ifHid.getId(), data, length, 0);
	}
}