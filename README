To build Qz with Eclipse:

* Create a new Eclipse workspace.

* In the menu, choose File > New > Other…, then choose Android > Android Project from Existing Code.
  Choose the Qz source directory (in which this README resides) and complete the wizard.
  
* Add the required Android support libraries, which can be found in your Android SDK directory, in
  extras/android/support/vX/LIBNAME/ (insert version and library name as appropriate). Step-by-step
  instructions can be found at https://stackoverflow.com/a/26878804/2703209. You will need the
  following libraries:
  
  * appcompat-v7
  * preference-v7 (depends on appcompat-v7)
  * preference-v14 (depends on appcompat-v7 and preference-v7)

* Make sure you have cloned all subrepositories. If you haven’t done so yet, do:
  git submodule update --init --recursive
  
* Add a project for RDS Surveyor: In the menu, choose File > Import…, then choose
  General > Existing Projects into Workspace. Choose contrib/rdssurveyor/RDSSurveyor (in the Qz source
  directory) as the path and complete the wizard.
  
* Add RDS Surveyor to the Qz project: In the Qz project properties (accessible by right-clicking the
  project), go to Java Build Path, change to the Projects tab and add the RDSSurveyor project.
  
That’s it! You now have two ways to build and install Qz—either from Eclipse, or by issuing the
following commands on the command line:

    gradle build
    gradle installDebug

If you really want to use Android Studio, feel free to do so, but:

* use `build.gradle` to import the project
* keep paths intact
* keep the toolchain intact (i.e. Android build tools and gradle versions)
* before committing, make sure you are committing code changes *only*